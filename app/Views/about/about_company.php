

  <!-- Content Wrapper. Contains page content -->
  <div class="overlay-wrapper test">
                      <div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">Loading...</div></div>
                     </div>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>General Form</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">حول التطبيق</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
            <div class="col-md-12">
               
                 <div class="card card-info">
                      <div class="card-header button_color">
                        <h3 class="card-title">حول التطبيق</h3>
                      </div>
         <form role="form" method="post" id="editform" action="#">
              <div class="card-body">

                <?php if(isset($data) && !empty($data)){ ?>
                  <?php

?>
                <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <label for="inputSpentBudget">حول التطبيق</label>
                    <div class="input-group mb-3">
                    <textarea  name="about"   class="form-control" ><?php if(isset($data['about']) && strlen($data['about'])!=0) echo $data['about']; ?></textarea>
                    </div>
                    <!-- /input-group -->
                  </div>
</div>

                  <div class="col-md-12">
                  <div class="form-group">
                  <label for="inputSpentBudget">الايميل</label>
                    <div class="input-group mb-3">
                      <input type="email" name="email" value="<?php if(isset($data['email']) && strlen($data['email'])!=0) echo $data['email']; ?>"  class="form-control" placeholder="Email">
                      <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fas fa-envelope"></span>
                        </div>
                      </div>
                    </div>
</div>
                    <!-- /input-group -->
                  </div>
                

                  <!-- /.col-lg-6 -->
                    
                  <div class="col-md-12">
                  <div class="form-group">
                  <label for="inputSpentBudget">الهاتف</label>
                   <div class="input-group mb-3">
                      <input type="text" name="phone"  value="<?php if(isset($data['phone']) && strlen($data['phone'])!=0) echo $data['phone']; ?>"  class="form-control" placeholder="Phone Number">
                      <div class="input-group-append">
                        <div class="input-group-text">
                          <span class="fa fa-phone"></span>
                        </div>
                      </div>
                    </div>
                    <!-- /input-group -->
                  </div>
</div>
                <div class="col-md-12">
                <div class="form-group">
                <label for="inputSpentBudget">الفيس بوك</label>
                    <div class="input-group mb-3">
                          <input type="text" name="facebook"  value="<?php if(isset($data['facebook']) && strlen($data['facebook'])!=0) echo $data['facebook']; ?>"  class="form-control" placeholder="Facebbok">
                          <div class="input-group-append">
                          </div>
                        </div>
</div>
                    <!-- /input-group -->
                  </div>
                  <div class="col-md-12">
                <div class="form-group">
                <label for="inputSpentBudget">انستغرام</label>
                    <div class="input-group mb-3">
                          <input type="text" name="intagram"  value="<?php if(isset($data['instagram']) && strlen($data['instagram'])!=0) echo $data['instagram']; ?>"  class="form-control" placeholder="Instagram">
                          <div class="input-group-append">
                          </div>
                        </div>
</div>
                    <!-- /input-group -->
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                    <label for="inputSpentBudget">يوتيوب</label>
                    <div class="input-group mb-3">
                    <input type="text" name="youtube"  value="<?php if(isset($data['youtube']) && strlen($data['youtube'])!=0) echo $data['youtube']; ?>"  class="form-control" placeholder="youtube">
                    </div>
                    <!-- /input-group -->
                  </div>
</div>
                  <div class="col-md-12">
                <div class="form-group">
                <label for="inputSpentBudget">تويتر</label>
                    <div class="input-group mb-3">
                          <input type="text" name="twiter"  value="<?php if(isset($data['twiter']) && strlen($data['twiter'])!=0) echo $data['twiter']; ?>"  class="form-control" placeholder="Twiter">
                          <div class="input-group-append">
                          </div>
                        </div>
</div>
                    <!-- /input-group -->
                  </div>
                  <!-- /.col-lg-6 -->
                    <div class="card-footer" style="display: flex;">
                    <button type="submit" name="update"  class="btn button_color">حقظ</button>
                   
                </div>
                </div>
                <?php } else{ ?>
<h3 class="alert alert-danger">Error received please try later</h3>
                <?php } ?>
            <!-- /.card -->
            </div>
         </form>
          </div>
        </div>
        </div>
      </section>
</div>
  <script>
  $(".test").hide();
          <?php $id= $this->uri->segment(3)?>
          $('#editform').submit(function () {
          var form_data = new FormData($("#editform")[0]);

          form_data.append('action','add')
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'About/update_comp/'?><?=$id?>',
          data: form_data,
          type: 'post',
          //  cache:false,
          processData: false,
          contentType: false,
          beforeSend: function(){
            $(".test").show();
          },
          success: function(output) {
            $(".test").hide();
          var response=JSON.stringify(output)
          if(output==true){
                      window.location="<?=site_url()?>About";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
          }
          });


          });
         
</script>

            