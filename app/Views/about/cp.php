
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <h1>الاحصائيات</h1>
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">الاحصائيات</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-lg-4 col-6">
            <!-- small box -->
           
            <div class="small-box bg-info">
              <div class="inner">
              <h3><?php if(!empty($count1)) echo $count1['count']; ?></h3>
                <p  style="font-family: 'Droid Arabic Kufi', serif;font-size:100%;"> المستخدمين السوريين</p>
                
              </div>
              <div class="icon">
              <i class="fas fa-users"></i>
              </div>
              <a href="<?=site_url('User/users')?>" class="small-box-footer">مزيد من المعلومات<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          
          </div>
          <div class="col-lg-4 col-6">
            <!-- small box -->
           
            <div class="small-box bg-success">
              <div class="inner">
              <h3><?php if(!empty($count2)) echo $count2['count']; ?></h3>
                <p  style="font-family: 'Droid Arabic Kufi', serif;font-size:100%;"> المستخدمين الكويتين</p>
              </div>
              <div class="icon">
              <i class="fas fa-users"></i>
              </div>
              <a href="<?=site_url('User/users')?>" class="small-box-footer">مزيد من المعلومات<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          
          </div>
          <div class="col-lg-4 col-6">
            <!-- small box -->
           
            <div class="small-box bg-warning">
              <div class="inner" >
              <h3><?php if(!empty($count3)) echo $count3['count']; ?></h3>
                <p  style="font-family: 'Droid Arabic Kufi', serif;font-size:100%;">التصنيفات الرئيسية</p>
              </div>
              <div class="icon">
              <i class="fas fa-sort"></i>
              </div>
              <a href="<?=site_url('Category')?>" class="small-box-footer">مزيد من المعلومات<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          
          </div>
          <div class="col-lg-6 col-6">
            <!-- small box -->
           
            <div class="small-box bg-danger">
              <div class="inner" >
              <h3><?php if(!empty($count4)) echo $count4['count']; ?></h3>
                <p  style="font-family: 'Droid Arabic Kufi', serif;font-size:100%;">التصنيفات الفرعية</p>
              </div>
              <div class="icon">
              <i class="fas fa-sort"></i>
              </div>
              <a href="<?=site_url('category/sub_category')?>" class="small-box-footer">مزيد من المعلومات<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          
          </div>
          <div class="col-lg-6 col-4">
            <!-- small box -->
           
            <div class="small-box bg-success">
              <div class="inner">
              <h3><?php if(!empty($count5)) echo $count5['count']; ?></h3>
                <p style="font-family: 'Droid Arabic Kufi', serif;font-size:100%;">الاعلانات</p>
              </div>
              <div class="icon">
              <i class="fas fa-sort"></i>
              </div>
              <a href="<?=site_url('Ads/ads')?>" class="small-box-footer">مزيد من المعلومات <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          
          </div>
        
   

    
    
    <!-- /.content -->
  </div>