

  <!-- Content Wrapper. Contains page content -->
  <div class="overlay-wrapper test">
                      <div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">Loading...</div></div>
                     </div>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>General Form</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">الاعدادات</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
            <div class="col-md-12">
               
                 <div class="card card-info">
                      <div class="card-header button_color">
                        <h3 class="card-title">الاعدادات</h3>
                      </div>
         <form role="form" method="post" id="editform" action="#">
              <div class="card-body">

                <?php if(isset($data) && !empty($data)){ ?>

                <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <label for="inputSpentBudget">عدد الاعلانات</label>
                    <div class="input-group mb-3">
                    <input type="number"  name="post_count"   class="form-control" value="<?php if(isset($data['post_count']) && strlen($data['post_count'])!=0) echo $data['post_count']; ?>">
                    </div>
                    <!-- /input-group -->
                  </div>
</div>
<div class="col-md-12">
                    <div class="form-group">
                    <label for="inputSpentBudget">عدد الفيديو المسموح بها</label>
                    <div class="input-group mb-3">
                    <input type="number"  name="video_count"   class="form-control" value="<?php if(isset($data['video_count']) && strlen($data['video_count'])!=0) echo $data['video_count']; ?>">
                    </div>
                    <!-- /input-group -->
                  </div>
</div>
                 
                    <!-- /input-group -->
                

                  <!-- /.col-lg-6 -->
                    
           
                     
                    
                  <!-- /.col-lg-6 -->
                    <div class="card-footer" style="display: flex;">
                    <button type="submit" name="update"  class="btn button_color">حفظ</button>
                   
                </div>
                </div>
                <?php } else{ ?>
<h3 class="alert alert-danger">Error received please try later</h3>
                <?php } ?>
            <!-- /.card -->
            </div>
         </form>
          </div>
        </div>
        </div>
      </section>
</div>
  <script>
  $(".test").hide();
          
          $('#editform').submit(function () {
          var form_data = new FormData($("#editform")[0]);

          form_data.append('action','add')
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'About/update_settings'?>',
          data: form_data,
          type: 'post',
          //  cache:false,
          processData: false,
          contentType: false,
          beforeSend: function(){
            $(".test").show();
          },
          success: function(output) {
            $(".test").hide();
          var response=JSON.stringify(output)
          if(output==true){
                      window.location="<?=site_url()?>About/settings";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
          }
          });


          });
         
</script>

            