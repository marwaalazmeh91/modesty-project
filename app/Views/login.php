<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Modesty| Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
     <style>
        /* .login-page{
            background: url(http://localhost/modesty/assets/images/pages_logo.png) !important;
            background-size: cover !important;
            background-repeat: no-repeat !important;
            background-position: center !important;
        }
         .login-logo a{
             color: white !important;
             font-weight: 500
         } */
    </style>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">WELCOME</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <center>
      <img src="https://modesty-fation.com/assets/images/pages_logo.png" style="height:100px;width:100px">
        </center>
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="<?=site_url('users/login') ?>" method="post">
        <div class="input-group mb-3">
          <input type="text" name="username" class="form-control" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span >@</span>
            </div>
          </div>
        </div>
        

        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
            
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-4">
            
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn  btn-block button_color" style="background-color: black;color: white;">Sign In</button>
          </div>
      
          <!-- /.col -->
        </div>
      </form>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?=site_url('assets/')?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=site_url('assets/')?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=site_url('assets/')?>dist/js/adminlte.min.js"></script>

</body>
</html>
