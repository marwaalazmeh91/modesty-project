<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title> حشمتي  | Dashboard</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>dist/css/adminlte.min.css">
  <link rel="stylesheet" href="<?=site_url('assets/')?>dist/css/rtl.css">
  <link rel="stylesheet" href="<?=site_url('assets/')?>dist/css/style.css">

    <!-- summernote -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- daterange picker -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/daterangepicker/daterangepicker.css">
          <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />  

    <!-- Select2 -->
    
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/fileinput/css/fileinput.min.css">
    <script src="<?=site_url('assets/')?>plugins/jquery/jquery.min.js"></script>
     <!-- <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
      <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>             -->
</head>
   <style>
 .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active {
    background-color: #ffffff !important;
    color: #583794;
}
        
.bg-purple, .bg-purple>a {
    color: #fff!important;
}
[class*=sidebar-light-] .sidebar a {
    color: #ffffff;
}
[class*=sidebar-light-] .nav-header {
    background: inherit;
    color: #dee2e6;
}

        
.nav-pills .nav-link:not(.active):hover {
    color: #ececec !important;
    background: #6610f254;
}        
        
thead, tbody{
  text-align: center
    }
.navbar-expand .navbar-nav .nav-link {
    padding-right: 1rem;
    padding-left: 1rem;
    color: white;
}
        
    </style>
<body class="hold-transition sidebar-mini" id="body">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
<!--
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?=site_url()?>" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?=site_url('user/logout')?>" class="nav-link">Logout</a>
      </li>
-->
    </ul>

    <!-- SEARCH FORM -->
<!--
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
-->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      
      <!-- Notifications Dropdown Menu -->
      <!-- <li class="nav-item ">
        <a class="nav-link" href="<?=site_url('user/logout')?>">
         Logout
        </a>
        </li> -->
    </ul>
  </nav>
  <!-- /.navbar -->
    <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=site_url('about/cp')?>" class="brand-link">
      <img src="<?=site_url('assets/')?>images/pages_logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: 0.8">
      <span class="brand-text font-weight-light">حشمتي</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
<!--      <div class="user-panel mt-3 pb-3 mb-3 d-flex">-->
<!--        <div class="image">-->
<!--            -->
<!--          <img src="--><?//=site_url('assets/dist/img/')?><!----><?//=$user['image_url'] ?><!-- " class="img-circle elevation-2" alt="User Image">-->
<!--        </div>-->
<!--        <div class="info">   -->
<!--          <a href="#" class="d-block">--><?//= $user['first_name'].$user['last_name']?><!--</a>-->
<!--        </div>-->
<!--      </div>-->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
<!--            <a href="#" class="nav-link active">-->
<!--              <i class="nav-icon fas fa-tachometer-alt"></i>-->
<!--              <p>-->
<!--                    Dashboard-->
<!--<!--                  <i class="right fas fa-angle-left"></i>-->
<!--              </p>-->
<!--            </a>-->
            </li>
            <!-- <li class="nav-header">Pages</li> -->
         
         <!-- <li class="nav-item">
            <a href="<?=site_url('About')?>" class="nav-link">
              <i class="nav-icon fa fa-building "></i>
              <p>
حول التطبيق               </p>
            </a>
          </li>
          <li class="nav-item" style="width:100%">
            <a href="<?=site_url('About/privacy_policy')?>" class="nav-link">
              <i class="nav-icon fa fa-building "></i>
              <p>
سياسة الخصوصصية              </p>
            </a>
      </li> -->
         

          <li class="nav-item" style="width:100%">
            <a href="<?=site_url('users/users/users')?>" class="nav-link">
              <i class="fas fa-users"></i>
              <p>
المستخدمين             </p>
            </a>
          </li>
          <li class="nav-item" style="width:100%">
            <a href="<?=site_url('Admin/Ads')?>" class="nav-link">
            <i class="fas fa-sort"></i>
              <p>
الاعلانات             </p>
            </a>
          </li>
          <li class="nav-item" style="width:100%">
            <a href="<?=site_url('Admin/Sliders')?>" class="nav-link">
            <i class="fas fa-sort"></i>
              <p>
السلايدر             </p>
            </a>
          </li>
          
          <li class="nav-item" style="width:100%">
            <a href="<?=site_url('Admin/Products')?>" class="nav-link">
            <i class="fas fa-sort"></i>
              <p>
المنتجات             </p>
            </a>
          </li>

       
        
 
        
          <br/>
          <li class="nav-item" style="width:100%">
            <a href="<?=site_url('Admin/Offers')?>" class="nav-link">
            <i class="fas fa-sort"></i>
              <p>
العروض             </p>
            </a>
          </li>

       
        
 
        
          <br/>
      
          <li class="nav-item"  style="width:100%">
          <a href="#" class="nav-link">
          <i class="fas fa-sort"></i>
              <p>
              الطلبات
              </p>
          </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url('Admin/orders/new_orders')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">الطلبات الجديدة</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('Admin/orders/active_orders')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">الطلبات النشطة </p>
                </a>
              </li>
          
              <li class="nav-item">
                <a href="<?=site_url('Admin/orders/ended_orders')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">الطلبات المنتهية </p>
                </a>
              </li>
            </ul>

          </li>
          <br/>
          <br/>
          <li class="nav-item">
          <a href="#" class="nav-link">
          <i class="fas fa-bell"></i>
              <p>
              الاشعارات
              </p>
          </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <a href="<?=site_url('admin/notifications/send_some_user')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-family: 'Droid Arabic Kufi', serif;font-size:100%;">إرسال لبعض المستخدمين</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="<?=site_url('admin/Notifications/send_all_user')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-family: 'Droid Arabic Kufi', serif;font-size:100%;">  إرسال لكل المستخدمين </p>
                </a>
              </li>
             
            </ul>
         
          </li>
        
          <li class="nav-item" style="width:100%">
            <a href="<?=site_url('Admin/settings')?>" class="nav-link">
            <i class="fas fa-sliders-h"></i>
              <p>
الاعدادات            </p>
            </a>
          </li>
          <br/>
        
          <br/>
          <!-- <li class="nav-item" style="width:100%">
            <a href="<?=site_url('Admin/City')?>" class="nav-link">
            <i class="fas fa-sliders-h"></i>
              <p>
المدن            </p>
            </a>
          </li>
          <br/> -->
          <li class="nav-item"  style="width;100%">
          <a href="#" class="nav-link">
          <i class="fas fa-sort"></i>
              <p>
              الأصناف
              </p>
          </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url('Admin/Category')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">الأصناف الرئيسية</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('Admin/category/sub_category')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">الأصناف الفرعية </p>
                </a>
              </li>
          
              
            </ul>

          </li>
          <br/>
        
        <br/>
        <li class="nav-item"  style="width:100%">
          <a href="#" class="nav-link">
          <i class="fas fa-sort"></i>
              <p>
              الكوبونات
              </p>
          </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url('Admin/Coubons')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">الكوبونات</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('Admin/Coubons/add_coubon')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">إضافة لكل المستخدمين</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('Admin/Coubons/add_coubon_for_some_user')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">إضافة لبعض المستخدمين</p>
                </a>
              </li>
              
            </ul>

          </li>
          <br/>
        
        <!-- <br/>
          <li class="nav-item"  style="width:100%">
          <a href="#" class="nav-link">
          <i class="fas fa-sort"></i>
              <p>
              المرتجعات
              </p>
          </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url('Admin/Product_returns/reasons_return')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">أسباب المرتجعات</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('Admin/Product_returns/products_return')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">المرتجعات</p>
                </a>
              </li>
          
              
            </ul>
         
          </li> -->
            
          <li class="nav-item"  style="width;100%">
          <a href="#" class="nav-link">
          <i class="fas fa-sort"></i>
              <p>
              المحتوى
              </p>
          </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url('Admin/about/edit_about/1')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">حول التطبيق</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('Admin/about/edit_terms/1')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">شروط الاستخدام </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('Admin/about/edit_privacy_policy/1')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">سياسة الخصوصصية </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?=site_url('Admin/about/edit_terms_return_orders/1')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p style="font-size:100%;">شروط الارتجاع </p>
                </a>
              </li>
              
            </ul>

          </li>
         
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <?php
        if (isset($_view) && $_view)
        echo view($_view);
        
        ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-light">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
   
    
 
   <p class="text-center"> <strong>Copyright &copy; <?php echo date('Y'); ?> </strong> </p>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->

<!-- Bootstrap 4 -->

<!-- Select2 -->
<script src="<?=site_url('assets/')?>plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="<?=site_url('assets/')?>plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="<?=site_url('assets/')?>plugins/moment/moment.min.js"></script>
<script src="<?=site_url('assets/')?>plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- date-range-picker -->
<script src="<?=site_url('assets/')?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?=site_url('assets/')?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=site_url('assets/')?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="<?=site_url('assets/')?>plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=site_url('assets/')?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=site_url('assets/')?>dist/js/demo.js"></script>
<!-- Summernote -->
<script src="<?=site_url('assets/')?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- bs-custom-file-input -->
<script src="<?=site_url('assets/')?>plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>


<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  });
    
 $(document).ready(function () {
  bsCustomFileInput.init();
});
    
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    });
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })

</script>
</body>
</html>
