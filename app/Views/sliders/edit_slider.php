

  <!-- Content Wrapper. Contains page content -->
  <?php 
  $uri = service('uri');
  
  $id=$uri->getSegment(4);
  $translation_id=$uri->getSegment(5);
 $langeages=array();
  ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>Add Category</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">تعديل سلايدر</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
<!--                <h3 class="card-title">Add Category</h3>-->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" id="addform">
                <div class="card-body">
                 
  

                <div class="form-group">
                    <label for="exampleInputEmail1">تاريخ البداية</label>
                    <input  class="form-control" name="start_date"  type="date" required value="<?php if(isset($data_update->start_date) && !empty($data_update->start_date)) echo $data_update->start_date; ?>">
                   
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">تاريخ النهاية</label>
                    <input  class="form-control" name="end_date"  type="date" required value="<?php if(isset($data_update->end_date) && !empty($data_update->end_date)) echo $data_update->end_date; ?>">
                   
                  </div>
                  <div class="form-group">
                    <label for="image">الصورة</label>
                    <input type="file" name="image" class="form-control" id="image" data-name="<?php if(isset($data_update->image_url) && !empty($data_update->image_url)) echo $data_update->image_url; ?>">
                  </div>
             
                </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer" style="display: flex;">
                    <a>
                    <button type="submit" name="save" class="btn button_color">حفظ</button></a>
                    &ensp;
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
</div>
<script src="<?=site_url('assets/')?>plugins/fileinput/js/fileinput.min.js"></script>
<script>
  // $("#image").fileinput();
  var image=$('#image').data('name');
var url="<?php echo image_url?>";
    $("#image").fileinput({initialPreview: [
  "<img src='"+url+image+"' class='file-preview-image' style='hegiht:100px;width:100px'>",
  
],});
$('#addform').submit(function () {
          var form_data = new FormData($("#addform")[0]);
//          var files = $('#files')[0].files;
          form_data.append('action','add')
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'Admin/sliders/update_slider/'.$id ?>',
              data: form_data,
              type: 'post',
              //  cache:false,
              processData: false,
              contentType: false,
              beforeSend: function(){
             
              },
              success: function(output) {
                  var response=JSON.stringify(output)

                  if(output==true){
                      window.location="<?=site_url()?>Admin/sliders";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
              }
          });
      });
      



</script>
            