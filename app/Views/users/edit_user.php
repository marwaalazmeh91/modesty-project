

  <!-- Content Wrapper. Contains page content -->
  <?php $id=$this->uri->segment(3);
  
 
  ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>Add Category</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصصفة الرئيسية</a></li>
              <li class="breadcrumb-item active">تعديل مستخدم</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
<!--                <h3 class="card-title">Add Category</h3>-->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" id="addform">
                <div class="card-body">
                <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="exampleInputEmail1">اسم المستخدم</label>
                     <input class="form-control" name="username" type="text" placeholder="Username"  value="<?php if(isset($data['username']) && strlen($data['username'])!=0) echo $data['username']; ?>">
                  </div>
                  </div>
                  <!-- <div class="col-md-6">
                 <div class="form-group">
                    <label for="image">الصورة</label>
                    <input type="file" name="image" class="form-control" id="image"  data-name="<?php if(isset($data['image_url']) && !empty($data['image_url'])) echo $data['image_url']; ?>">
                  </div>
                  </div>-->
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الايميل</label>
                     <input class="form-control" name="email" type="text" placeholder="email"  value="<?php if(isset($data['email']) && strlen($data['email'])!=0) echo $data['email']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الهاف</label>
                     <input class="form-control" name="phone" type="text" placeholder="Phone"  value="<?php if(isset($data['phone']) && strlen($data['phone'])!=0) echo $data['phone']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">نبذة</label>
                     <input class="form-control" name="resume" type="text" placeholder="resume"  value="<?php if(isset($data['resume']) && strlen($data['resume'])!=0) echo $data['resume']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">البلد</label>
                     <input class="form-control" name="country" type="text" placeholder="country"  value="<?php if(isset($data['country']) && strlen($data['country'])!=0) echo $data['country']; ?>">
                  </div>
               
                  <div class="form-group">
                    <label for="image">الصورة</label>
                    <input type="file" name="image" class="form-control" id="image"  data-name="<?php if(isset($data['image_url']) && !empty($data['image_url'])) echo $data['image_url']; ?>">
                  </div>
                </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer" style="display: flex;">
                    <a>
                    <button type="submit" name="save" class="btn button_color">حفظ</button></a>
                    &ensp;
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
</div>
<script src="<?=site_url('assets/')?>plugins/fileinput/js/fileinput.min.js"></script>
<script>
   var image=$('#image').data('name');

$("#image").fileinput({initialPreview: [
  "<img src='"+image+"' class='file-preview-image' style='hegiht:100px;width:100px'>",
  
],});
  $("#image").fileinput();
$('#addform').submit(function () {
          var form_data = new FormData($("#addform")[0]);
//          var files = $('#files')[0].files;
          form_data.append('action','add')
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'user/update/'.$id ?>',
              data: form_data,
              type: 'post',
              //  cache:false,
              processData: false,
              contentType: false,
              beforeSend: function(){
                  alert(form_data);
              },
              success: function(output) {
                  var response=JSON.stringify(output)

                  if(output==true){
                      window.location="<?=site_url()?>User/users";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
              }
          });
      });
      



</script>
            