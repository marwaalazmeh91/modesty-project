

  <!-- Content Wrapper. Contains page content -->
  <?php 
  $uri = service('uri');
  
  $id=$uri->getSegment(4);
  $translation_id=$uri->getSegment(5);
 $langeages=array();
  ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>Add Category</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">تعديل صنف</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
<!--                <h3 class="card-title">Add Category</h3>-->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" id="addform">
                <div class="card-body">
                  <?php foreach($data_update as $data){?>
                    <?php if($data->language=='ar'){
                      
                      array_push($langeages,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">العنوان بالعربي</label>
                     <input class="form-control" name="title_ar" type="text" placeholder="Title" required value="<?php if(($data->language=='ar') && isset($data->name) && strlen($data->name)!=0) echo $data->name; ?>">
                  </div>
           <?php } ?>
           <?php if($data->language=='en'){
              array_push($langeages,$data->language);
             ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">العنوان بالانكليزي</label>
                     <input class="form-control" name="title_en" type="text" placeholder="Title" required value="<?php if(($data->language=='en') && isset($data->name) && strlen($data->name)!=0) echo $data->name; ?>">
                  </div>
                  <?php } ?>
                  <?php if($data->language=='de'){
                     array_push($langeages,$data->language);
                    ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">العنوان بالألماني</label>
                     <input class="form-control" name="title_de" type="text" placeholder="Title" required value="<?php if(($data->language=='de') && isset($data->name) && strlen($data->name)!=0) echo $data->name; ?>">
                  </div>
                  <?php } ?>
                  <?php if($data->language=='tr'){
                     array_push($langeages,$data->language);
                    ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">العنوان بالتركي</label>
                     <input class="form-control" name="title_tr" type="text" placeholder="Title" required value="<?php if(($data->language=='tr') && isset($data->name) && strlen($data->name)!=0) echo $data->name; ?>">
                  </div>
                  <?php  } ?>
                  <?php  } ?>

                  <?php 
                    if(!in_array('ar',$langeages)){

                    ?>
                      <div class="form-group">
                    <label for="exampleInputEmail1">العنوان بالعربي</label>
                     <input class="form-control" name="title_new_ar" type="text" placeholder="Title" required>
                  </div>
                  <?php } ?>
                  <?php 
                    if(!in_array('en',$langeages)){

                    ?>
                      <div class="form-group">
                    <label for="exampleInputEmail1">العنوان بالانكليزي</label>
                     <input class="form-control" name="title_new_en" type="text" placeholder="Title" required >
                  </div>
                  <?php } ?>
                  <?php 
                    if(!in_array('de',$langeages)){

                    ?>
                     <div class="form-group">
                    <label for="exampleInputEmail1">العنوان بالألماني</label>
                     <input class="form-control" name="title_new_de" type="text" placeholder="Title" required>
                  </div>
                  <?php } ?>
                  <?php 
                    if(!in_array('de',$langeages)){

                    ?>
                     <div class="form-group">
                    <label for="exampleInputEmail1">العنوان بالتركي</label>
                     <input class="form-control" name="title_new_tr" type="text" placeholder="Title" >
                  </div>
                  <?php } ?>
                  <div class="form-group">
                    <label for="image">الصورة</label>
                    <input type="file" name="image" class="form-control" id="image" data-name="<?php if(isset($data->image_url) && !empty($data->image_url)) echo $data->image_url; ?>">
                  </div>
             
                </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer" style="display: flex;">
                    <a>
                    <button type="submit" name="save" class="btn button_color">حفظ</button></a>
                    &ensp;
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
</div>
<script src="<?=site_url('assets/')?>plugins/fileinput/js/fileinput.min.js"></script>
<script>
  // $("#image").fileinput();
  var image=$('#image').data('name');

$("#image").fileinput({initialPreview: [
  "<img src='"+image+"' class='file-preview-image' style='hegiht:100px;width:100px'>",
  
],});
$('#addform').submit(function () {
          var form_data = new FormData($("#addform")[0]);
//          var files = $('#files')[0].files;
          form_data.append('action','add')
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'Admin/Category/update_category/'.$id ?>',
              data: form_data,
              type: 'post',
              //  cache:false,
              processData: false,
              contentType: false,
              beforeSend: function(){
             
              },
              success: function(output) {
                  var response=JSON.stringify(output)

                  if(output==true){
                      window.location="<?=site_url()?>Admin/Category";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
              }
          });
      });
      



</script>
            