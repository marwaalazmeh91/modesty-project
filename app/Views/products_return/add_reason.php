

            

  <!-- Content Wrapper. Contains page content -->
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>Add Category</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">إضافة سبب</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
<!--                <h3 class="card-title">Add Category</h3>-->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" id="addform">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">السبب بالعربي</label>
                     <input class="form-control" name="title_ar" type="text" placeholder="Title" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">السبب بالانكليزي</label>
                     <input class="form-control" name="title_en" type="text" placeholder="Title" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">السبب بالألماني</label>
                     <input class="form-control" name="title_de" type="text" placeholder="Title" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">السبب بالتركي</label>
                     <input class="form-control" name="title_tr" type="text" placeholder="Title" required>
                  </div>
            
                
                
                <!-- /.card-body -->

                <div class="card-footer" style="display: flex;">
                    <a>
                    <button type="submit" name="save" class="btn button_color">حفظ</button></a>
                    &ensp;
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
</div>
<script src="<?=site_url('assets/')?>plugins/fileinput/js/fileinput.min.js"></script>
<script>
  $("#image").fileinput();
$('#addform').submit(function () {
          var form_data = new FormData($("#addform")[0]);
//          var files = $('#files')[0].files;
          form_data.append('action','add')
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'admin/Product_returns/save_reason'?>',
              data: form_data,
              type: 'post',
              //  cache:false,
              processData: false,
              contentType: false,
              beforeSend: function(){
                 
              },
              success: function(output) {
                  var response=JSON.stringify(output)

                  if(output==true){
                      window.location="<?=site_url()?>Admin/Product_returns/reasons_return";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
              }
          });
      });
      



</script>
            