

  <!-- Content Wrapper. Contains page content -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>Add Category</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">إضافة منتج</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
<!--                <h3 class="card-title">Add Category</h3>-->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" id="addform">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالعربي</label>
                     <input class="form-control" name="title_ar" type="text" placeholder="Title" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالانكليزي</label>
                     <input class="form-control" name="title_en" type="text" placeholder="Title" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالألماني</label>
                     <input class="form-control" name="title_de" type="text" placeholder="Title" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالتركي</label>
                     <input class="form-control" name="title_tr" type="text" placeholder="Title" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الوصف بالعربي</label>
                     <input class="form-control" name="description_ar" type="text" placeholder="Description" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الوصف بالانكليزي</label>
                     <input class="form-control" name="description_en" type="text" placeholder="Description" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الوصف بالألماني</label>
                     <input class="form-control" name="description_de" type="text" placeholder="Description" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الوصف بالتركي</label>
                     <input class="form-control" name="description_tr" type="text" placeholder="Description" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الصنف الفرعي</label>
                    <select class="form-control" name="category"  required>
                    <option value="">Please Choose</option>
                    <?php if(isset($category) && !empty($category)){
                        foreach($category as $data){
                        ?>
                    <option value="<?php echo $data->id;?>"><?php echo $data->name; ?></option>
                    <?php }} ?>
                    </select>
                  </div>
                         <div class="form-group">
                    <label for="exampleInputEmail1">رمز المنتج </label>
                     <input class="form-control" name="bar_code" type="text" placeholder="barcode" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">السعر</label>
                     <input type="number" class="form-control" name="price" type="text" placeholder="price" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الحسم</label>
                     <input  type="number" class="form-control" name="dicount" type="text" placeholder="discount" >
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputEmail1">الكمية</label>
                     <input type="number" class="form-control" name="quantity" type="text" placeholder="quantity" required>
                  </div> -->
         
                  <div class="form-group">
                  <label>القياسات</label>
                  <div class="select2-purple">
                        <select class="form-control select2 select2-danger"  multiple="multiple" id="sizes" name="sizes">
             
                   <option class="user" value="standart" >Standart</option>
                       <option class="user" value="s" >S</option>
                       <option class="user" value="m" >M</option>
                       <option class="user" value="l" >L</option>
                       <option class="user" value="xl" >XL</option>
                       <option class="user" value="xxl" >XXL</option>
                       <option class="user" value="xxxl" >XXXL</option>
                       <option class="user" value="36" >36</option>
                        <option class="user" value="38" >38</option>
                        <option class="user" value="40" >40</option>
                        <option class="user" value="42" >42</option>
                        <option class="user" value="44" >44</option>
                        <option class="user" value="46" >46</option>
                           <option class="user" value="36-38" >36-38</option>
                              <option class="user" value="38-40" >38-40</option>
                                 <option class="user" value="40-42" >40-42</option>
                                  <option class="user" value="42-44" >42-44</option>
                                  <option class="user" value="44-46" >44-46</option>
                        </select>

                  </div>
                 </div>
                  <fieldset class="form-group row">
                  <label for="exampleInputEmail1">هل وصل حديثاً</label>
      <div class="col-sm-10">
        <div class="form-check">
          <label class="form-check-label radio_rtl">
            <input class="form-check-input radio-inline input_radio" type="radio" name="is_arrived" id="gridRadios1" value=1 checked>
           نعم</label>
            <label class="form-check-label radio_rtl">
            <input class="form-check-input radio-inline input_radio" type="radio" name="is_arrived" id="gridRadios2" value=0>
         لا</label>
           
        </div>
        
    </fieldset>
    <fieldset class="form-group row">
                  <label for="exampleInputEmail1">هل هو جديد</label>
      <div class="col-sm-10">
        <div class="form-check">
          <label class="form-check-label radio_rtl">
            <input class="form-check-input radio-inline input_radio" type="radio" name="isnew" id="gridRadios1" value=1 checked >
           نعم</label>
            <label class="form-check-label radio_rtl">
            <input class="form-check-input radio-inline input_radio" type="radio" name="isnew" id="gridRadios2" value=0>
         لا</label>
           
        </div>
        
    </fieldset>

                <img id="uploaded_image" src="" class="form-group" style="max-width: 50%">
            </div>

                  <div class="form-group">
                    <label for="image">الصورة</label>
                    <input type="file" name="image" class="form-control" id="image" >
                  </div>
                
                
                <!-- /.card-body -->

                <div class="card-footer" style="display: flex;">
                    <a>
                    <button type="submit" name="save" class="btn button_color">حفظ</button></a>
                    &ensp;
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
</div>
<script src="<?=site_url('assets/')?>plugins/select2/js/select2.full.min.js"></script>
<script src="<?=site_url('assets/')?>plugins/fileinput/js/fileinput.min.js"></script>
<script>
  $("#image").fileinput();
$('#addform').submit(function () {
 
          var form_data = new FormData($("#addform")[0]);
         var sizes = $('#sizes').val();
          form_data.append('action','add')
          form_data.append('sizes',sizes)
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'admin/Products/save_product'?>',
              data: form_data,
              type: 'post',
              //  cache:false,
              processData: false,
              contentType: false,
              beforeSend: function(){
                 
              },
              success: function(output) {
                  var response=JSON.stringify(output)

                  if(output==true){
                      window.location="<?=site_url()?>Admin/Products";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
              }
          });
      });
      var maxField = 10; //Input fields increment limitation
      var addButton = $('.add_button'); //Add button selector
      var preButton = $('.pre_button'); //Add button selector
      var sliderButton = $('.slider_btn'); //Add button selector
      var trofyButton = $('.trofy_button'); //Add button selector
      var trofyrapper = ('.card-body'); //Input field wrapper
      var wrapper = ('.field_wrapper'); //Input field wrapper
      var slidwrapper = ('.slider_wrap'); //Input field wrapper
      var numberIncr = 1;
      var numberInc = 1;
      var numbertrof = 1;
      var preButton = $('.pre_button');
      $(preButton).click(function(e){
          e.preventDefault();
          //Once add button is clicked
          var theWrapper = $(this).closest(wrapper);

          var fieldHTML = '<div class="field_wrapper form-group"><div class="row"><div class="form-group"><input type="color" name="color['+ numberInc +']" class="form-control"  placeholder="اللون"><div class="form-group"><label for="exampleInputFile">صورة المنتج</label><div class="input-group"><div class="custom-file"><input type="file"  id="images['+ numberInc +']"  name="images['+ numberInc +']"  class="custom-file-input"><label class="custom-file-label" for="exampleInputFile">Choose Icon</label> </div> <div class="input-group-append"><span class="input-group-text" id="">Upload</span></div></div></div></div><a href="javascript:void(0);" class="remove_button btn bg-purple" style="height:38px" title="Remove field">remove</a></div></div>'; //New input field html
          var numOfChildren = theWrapper.children().length;
          if( numOfChildren < maxField){ //Check maximum number of input fields
              theWrapper.append($('<div class="field_wrapper form-group"><div class=""><div class="form-group"><input type="color" name="color['+ numberInc +']" class="form-control"  placeholder="اللون"><div class="form-group"><label for="exampleInputFile">صورة المنتج </label><div class="input-group"><div class="custom-file"><input type="file"  id="images['+ numberInc +']"  name="images['+ numberInc +']"  class="custom-file-input"><label class="custom-file-label" for="exampleInputFile"></label> </div> <div class="input-group-append"><span class="input-group-text" id="">تحميل</span></div></div></div></div><a href="javascript:void(0);" class="remove_button btn bg-purple" style="height:38px;float:right" title="Remove field">حذف</a></div></div>'));


              numberInc++;
          }
      });
      $('.remove_button').click(function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });
</script>
            