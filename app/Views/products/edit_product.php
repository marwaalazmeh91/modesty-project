<?php   $uri = service('uri');
  
  $id=$uri->getSegment(4);
  $translation_id=$uri->getSegment(5);
 $langeages=array();
 $langeages2=array();
 
  ?>
  <!-- Content Wrapper. Contains page content -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>Add Category</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">تعديل منتج</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
<!--                <h3 class="card-title">Add Category</h3>-->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" id="addform">
                <div class="card-body">
                <?php foreach($data_update as $data){?>
                    <?php if($data->language=='ar'){
                      
                      array_push($langeages,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالعربي</label>
                     <input class="form-control" name="title_ar" type="text" placeholder="Title" required  value="<?php if(($data->language=='ar') && isset($data->name) && strlen($data->name)!=0) echo $data->name; ?>">
                  </div>
                  <?php } ?>
                  <?php if($data->language=='en'){
                      
                      array_push($langeages,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالانكليزي</label>
                     <input class="form-control" name="title_en" type="text" placeholder="Title" required  value="<?php if(($data->language=='en') && isset($data->name) && strlen($data->name)!=0) echo $data->name; ?>">
                  </div>
                  <?php } ?>
                  <?php if($data->language=='de'){
                      
                      array_push($langeages,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالألماني</label>
                     <input class="form-control" name="title_de" type="text" placeholder="Title" required value="<?php if(($data->language=='de') && isset($data->name) && strlen($data->name)!=0) echo $data->name; ?>">
                  </div>
                  <?php } ?>
                  <?php if($data->language=='tr'){
                      
                      array_push($langeages,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالتركي</label>
                     <input class="form-control" name="title_tr" type="text" placeholder="Title" required value="<?php if(($data->language=='tr') && isset($data->name) && strlen($data->name)!=0) echo $data->name; ?>">
                  </div>
                  <?php } ?>
                  <?php } ?>
                  <?php 

             
                    if(!in_array('ar',$langeages)){

                    ?>
                      <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالعربي</label>
                     <input class="form-control" name="title_new_ar" type="text" placeholder="Title" required>
                  </div>
                  <?php } ?>
                  <?php 
                    if(!in_array('en',$langeages)){

                    ?>
                      <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالانكليزي</label>
                     <input class="form-control" name="title_new_en" type="text" placeholder="Title" required >
                  </div>
                  <?php } ?>
                  <?php 
                    if(!in_array('de',$langeages)){

                    ?>
                     <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالألماني</label>
                     <input class="form-control" name="title_new_de" type="text" placeholder="Title" required>
                  </div>
                  <?php } ?>
                  <?php 
                    if(!in_array('tr',$langeages)){

                    ?>
                     <div class="form-group">
                    <label for="exampleInputEmail1">الاسم بالتركي</label>
                     <input class="form-control" name="title_new_tr" type="text" placeholder="Title" >
                  </div>
                  <?php } ?>
                  <?php foreach($data_update as $data){?>
                    <?php if($data->language=='ar'){
                      
                      array_push($langeages2,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الوصف بالعربي</label>
                     <input class="form-control" name="description_ar" type="text" placeholder="Description" required value="<?php if(($data->language=='ar') && isset($data->desception) && strlen($data->desception)!=0) echo $data->desception; ?>">
                  </div>
                  <?php } ?>
                  <?php if($data->language=='en'){
                      
                      array_push($langeages2,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الوصف بالانكليزي</label>
                     <input class="form-control" name="description_en" type="text" placeholder="Description" required value="<?php if(($data->language=='en') && isset($data->desception) && strlen($data->desception)!=0) echo $data->desception; ?>">
                  </div>
                  <?php } ?>
                  <?php if($data->language=='de'){
                      
                      array_push($langeages2,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الوصف بالألماني</label>
                     <input class="form-control" name="description_de" type="text" placeholder="Description" required value="<?php if(($data->language=='de') && isset($data->desception) && strlen($data->desception)!=0) echo $data->desception; ?>">
                  </div>
                  <?php } ?>
                  <?php if($data->language=='tr'){
                      
                      array_push($langeages2,$data->language);
                      ?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الوصف بالتركي</label>
                     <input class="form-control" name="description_tr" type="text" placeholder="Description" required value="<?php if(($data->language=='tr') && isset($data->desception) && strlen($data->desception)!=0) echo $data->desception; ?>">
                  </div>
                  <?php } ?>
                  <?php } ?>
                             
           <?php       if(!in_array('ar',$langeages2)){

?>
  <div class="form-group">
<label for="exampleInputEmail1">الوصف بالعربي</label>
 <input class="form-control" name="description_new_ar" type="text" placeholder="Title" required>
</div>
<?php } ?>
<?php 
if(!in_array('en',$langeages2)){

?>
  <div class="form-group">
<label for="exampleInputEmail1">الوصف بالانكليزي</label>
 <input class="form-control" name="description_new_en" type="text" placeholder="Title" required >
</div>
<?php } ?>
<?php 
if(!in_array('de',$langeages2)){

?>
 <div class="form-group">
<label for="exampleInputEmail1">الوصف بالألماني</label>
 <input class="form-control" name="description_new_de" type="text" placeholder="Title" required>
</div>
<?php } ?>
<?php 
if(!in_array('tr',$langeages2)){

?>
 <div class="form-group">
<label for="exampleInputEmail1">الوصف بالتركي</label>
 <input class="form-control" name="description_new_tr" type="text" placeholder="Title" >
</div>
<?php }


?>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الصنف الفرعي</label>
                    <select class="form-control" name="category"  required>
                    <option value="">Please Choose</option>
                    <?php if(isset($category) && !empty($category)){
                        foreach($category as $data){
                        ?>
                    <option value="<?php echo $data->id;?>" <?php if($data_update[0]->sub_category_id==$data->id) echo "selected" ?>><?php echo $data->name;  ?></option>
                    <?php }} ?>
                    </select>
                  </div>
                        <div class="form-group">
                    <label for="exampleInputEmail1">رمز المنتج</label>
                     <input class="form-control" name="bar_code" type="text" placeholder="barcode" required value="<?php echo $data_update[0]->barcod; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">السعر</label>
                     <input type="number" class="form-control" name="price" type="text" placeholder="price" required value="<?php echo $data_update[0]->price; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الحسم</label>
                     <input  type="number" class="form-control" name="dicount" type="text" placeholder="discount" value="<?php echo $data_update[0]->discount; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الكمية</label>
                     <input type="number" class="form-control" name="quantity" type="text" placeholder="quantity" required value="<?php echo $data_update[0]->quantity; ?>">
                  </div>
         
                  <div class="form-group">
                  <label>القياسات</label>
                  <div class="select2-purple">
                        <select class="form-control select2 select2-danger"  multiple="multiple" id="sizes" name="sizes">
             <option class="user" value="standart"  <?php if (strpos( $data_update[0]->sizes, 'standart') !== false) echo "selected" ?>>Standart</option>
                       <option class="user" value="s" <?php if (strpos( $data_update[0]->sizes, 's') !== false) echo "selected" ?>>S</option>
                       <option class="user" value="m"  <?php if (strpos( $data_update[0]->sizes, 'm') !== false) echo "selected" ?>>M</option>
                       <option class="user" value="l"  <?php if (strpos( $data_update[0]->sizes, 'l') !== false) echo "selected" ?>>L</option>
                       <option class="user" value="xl"  <?php if (strpos( $data_update[0]->sizes, 'xl') !== false) echo "selected" ?>>XL</option>
                         <option class="user" value="xxl" <?php if (strpos( $data_update[0]->sizes, 'xxl') !== false) echo "selected" ?>>XXL</option>
                       <option class="user" value="xxxl" <?php if (strpos( $data_update[0]->sizes, 'xxxl') !== false) echo "selected" ?>>XXXL</option>
                       <option class="user" value="36" <?php if (strpos( $data_update[0]->sizes, '36') !== false) echo "selected" ?>>36</option>
                        <option class="user" value="38" <?php if (strpos( $data_update[0]->sizes, '38') !== false) echo "selected" ?>>38</option>
                        <option class="user" value="40" <?php if (strpos( $data_update[0]->sizes, '40') !== false) echo "selected" ?>>40</option>
                        <option class="user" value="42" <?php if (strpos( $data_update[0]->sizes, '42') !== false) echo "selected" ?>>42</option>
                        <option class="user" value="44" <?php if (strpos( $data_update[0]->sizes, '44') !== false) echo "selected" ?>>44</option>
                        <option class="user" value="46" <?php if (strpos( $data_update[0]->sizes, '46') !== false) echo "selected" ?>>46</option>
                           <option class="user" value="36-38" <?php if (strpos( $data_update[0]->sizes, '36-38') !== false) echo "selected" ?>>36-38</option>
                              <option class="user" value="38-40" <?php if (strpos( $data_update[0]->sizes, '38-40') !== false) echo "selected" ?>>38-40</option>
                                 <option class="user" value="40-42" <?php if (strpos( $data_update[0]->sizes, '40-42') !== false) echo "selected" ?>>40-42</option>
                                  <option class="user" value="42-44" <?php if (strpos( $data_update[0]->sizes, '42-44') !== false) echo "selected" ?>>42-44</option>
                                  <option class="user" value="44-46" <?php if (strpos( $data_update[0]->sizes, '44-46') !== false) echo "selected" ?>>44-46</option>
                        </select>

                  </div>
                 </div>
                  <fieldset class="form-group row">
                  <label for="exampleInputEmail1">هل وصل حديثاً</label>
      <div class="col-sm-10">
        <div class="form-check">
          <label class="form-check-label radio_rtl">
            <input class="form-check-input radio-inline input_radio" type="radio" name="is_arrived" id="gridRadios1" value=1  <?php if($data_update[0]->is_new_arrived==1) echo "checked"?>>
           نعم</label>
            <label class="form-check-label radio_rtl">
            <input class="form-check-input radio-inline input_radio" type="radio" name="is_arrived" id="gridRadios2" value=0  <?php if($data_update[0]->is_new_arrived==0) echo "checked"?>>
         لا</label>
           
        </div>
        
    </fieldset>
    <fieldset class="form-group row">
                  <label for="exampleInputEmail1">هل هو جديد</label>
      <div class="col-sm-10">
        <div class="form-check">
          <label class="form-check-label radio_rtl">
            <input class="form-check-input radio-inline input_radio" type="radio" name="isnew" id="gridRadios1" value=1  <?php if($data_update[0]->is_new==1) echo "checked"?> >
           نعم</label>
            <label class="form-check-label radio_rtl">
            <input class="form-check-input radio-inline input_radio" type="radio" name="isnew" id="gridRadios2" value=0  <?php if($data_update[0]->is_new==0) echo "checked"?>>
         لا</label>
           
        </div>
        
    </fieldset>
                  <div class="form-group">
                    <label for="image">الصورة</label>
                    <input type="file" name="image" class="form-control" id="image" data-name="<?php if(isset($data_update[0]->image_url) && !empty($data_update[0]->image_url)) echo $data_update[0]->image_url; ?>">
                  </div>
                
                
                <!-- /.card-body -->

                <div class="card-footer" style="display: flex;">
                    <a>
                    <button type="submit" name="save" class="btn button_color">حفظ</button></a>
                    &ensp;
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
</div>
<script src="<?=site_url('assets/')?>plugins/select2/js/select2.full.min.js"></script>
<script src="<?=site_url('assets/')?>plugins/fileinput/js/fileinput.min.js"></script>
<script>
    var image=$('#image').data('name');
var url="<?php echo image_url?>";
    $("#image").fileinput({initialPreview: [
  "<img src='"+url+image+"' class='file-preview-image' style='hegiht:100px;width:100px'>",
  
],});
$('#addform').submit(function () {
          var form_data = new FormData($("#addform")[0]);
//          var files = $('#files')[0].files;
var sizes = $('#sizes').val();
          form_data.append('action','add')
          form_data.append('sizes',sizes)
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'admin/Products/update_product/'.$id?>',
              data: form_data,
              type: 'post',
              //  cache:false,
              processData: false,
              contentType: false,
              beforeSend: function(){
                 
              },
              success: function(output) {
                  var response=JSON.stringify(output)

                  if(output==true){
                      window.location="<?=site_url()?>Admin/Products";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
              }
          });
      });
      



</script>
            