<!-- Content Wrapper. Contains page content -->
<?php 
  $uri = service('uri');
  
  $id=$uri->getSegment(4);
  $size=$uri->getSegment(5);
 $langeages=array();
  ?>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>Add Category</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">تعديل لون المنتج</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
<!--                <h3 class="card-title">Add Category</h3>-->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" id="addform">

                <div class="field_wrapper form-group">
                <div class="form-group">
                    <label for="exampleInputEmail1">الكمية</label>
                     <input type="number" class="form-control" name="quantity[]" type="text" placeholder="quantity" required>
                  </div>
            <label for="inputName">صور المنتجات</label>
                  <div  class="form-group">
                  
                      <input class="form-control" name="color[]" type="color" placeholder="اللون">
              </div>
           
              <div class="form-group">
                <label for="exampleInputFile">صورة المنتج </label>
                <div class="input-group">
                    <div class="custom-file">
                    <!-- <input type="file" class="form-control name="image[]" multiple="multiple"/> -->
                        <input type="file" name="images[]" class="custom-file-input">
                        <label class="custom-file-label" for="exampleInputFile"></label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">تحميل</span>
                    </div>
                </div>
                <img id="uploaded_image" src="" class="form-group" style="max-width: 50%">
            </div>
            <a href="javascript:void(0);"   style="margin-bottom: 2% ;float:right";
               } class="btn bg-purple pre_button btn-md" title="Add field">إضافة </a>
               <br/>
               <br/>
        </div>
        <div class="card-footer" style="display: flex;">
                    <a>
                    <button type="submit" name="save" class="btn button_color">حفظ</button></a>
                    &ensp;
                  
                </div>
              </form>
                <div class="card-body">
                </div>
        </div>
      </section>
</div>
<script src="<?=site_url('assets/')?>plugins/fileinput/js/fileinput.min.js"></script>

<script>
      $("#image").fileinput();
      $('#addform').submit(function () {
 
 var form_data = new FormData($("#addform")[0]);
var sizes = $('#sizes').val();
 form_data.append('action','add')
 form_data.append('sizes',sizes)
 event.preventDefault();
 $.ajax({ url: '<?php echo  site_url().'admin/Products/save_product_color/'.$id .'/'.$size?>?>',
     data: form_data,
     type: 'post',
     //  cache:false,
     processData: false,
     contentType: false,
     beforeSend: function(){
        
     },
     success: function(output) {
         var response=JSON.stringify(output)

         if(output==true){
             window.location="<?=site_url()?>Admin/Products/color_product/"+<?php echo $id;?>;
         }
         else{
             $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

         }
     }
 });
});
var maxField = 100000; //Input fields increment limitation
var addButton = $('.add_button'); //Add button selector
var preButton = $('.pre_button'); //Add button selector
var sliderButton = $('.slider_btn'); //Add button selector
var trofyButton = $('.trofy_button'); //Add button selector
var trofyrapper = ('.card-body'); //Input field wrapper
var wrapper = ('.field_wrapper'); //Input field wrapper
var slidwrapper = ('.slider_wrap'); //Input field wrapper
var numberIncr = 1;
var numberInc = 1;
var numbertrof = 1;
var preButton = $('.pre_button');
$(preButton).click(function(e){
 e.preventDefault();
 //Once add button is clicked
 var theWrapper = $(this).closest(wrapper);

 var fieldHTML = '<div class="field_wrapper form-group"><div class="row"><div class="form-group"><input type="color" name="color['+ numberInc +']" class="form-control"  placeholder="اللون"><div class="form-group"><label for="exampleInputFile">صورة المنتج</label><div class="input-group"><div class="custom-file"><input type="file"  id="images['+ numberInc +']"  name="images['+ numberInc +']"  class="custom-file-input"><label class="custom-file-label" for="exampleInputFile">Choose Icon</label> </div> <div class="input-group-append"><span class="input-group-text" id="">Upload</span></div></div></div></div><a href="javascript:void(0);" class="remove_button btn bg-purple" style="height:38px" title="Remove field">remove</a></div></div>'; //New input field html
 var numOfChildren = theWrapper.children().length;
 if( numOfChildren < maxField){ //Check maximum number of input fields
     theWrapper.append($('<div class="field_wrapper form-group"><div class=""> <div class="form-group"> <label for="exampleInputEmail1">الكمية</label> <input type="number" class="form-control" name="quantity['+ numberInc +']" type="text" placeholder="quantity" required></div><div class="form-group"><input type="color" name="color['+ numberInc +']" class="form-control"  placeholder="اللون"><div class="form-group"><label for="exampleInputFile">صورة المنتج </label><div class="input-group"><div class="custom-file"><input type="file"  id="images['+ numberInc +']"  name="images['+ numberInc +']"  class="custom-file-input"><label class="custom-file-label" for="exampleInputFile"></label> </div> <div class="input-group-append"><span class="input-group-text" id="">تحميل</span></div></div></div></div></div></div>'));


     numberInc++;
 }
});

      </script>