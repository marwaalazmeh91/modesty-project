<!-- Content Wrapper. Contains page content -->
<link rel="stylesheet" href="<?php echo site_url('assets/'); ?>plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
<!--            <h1>Categories</h1>-->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة  الرئيسية</a></li>
              <li class="breadcrumb-item active">الكوبونات</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
         
          <!-- ./col -->
        
        
          <!-- ./col -->
        
          <!-- ./col -->
        </div>
        <br/>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>
                  <div class="card-tools">
                  <div class="input-group input-group-sm" style="">
                    <!-- <input type="text" name="table_search" class="form-control float-right" placeholder="Search"> -->

                    <div class="input-group-append">
                      <!-- <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button> -->
                    </div>
                  </div>
                </div>
                
              </div>
                
              <!-- /.card-header -->
                
<!--
                           <button type="button" class="btn btn-outline-success btn-sm btn-flat">Add</button>
                        
-->

              <div class="card-body">
              <table class="table table-striped projects" id="example2">
                  <thead>
                  <tr>
                  
                    <th>الكود</th>
                    <th>تاريخ البداية</th>
                    <th>تاريخ النهاية</th>
                    <th>نسبة الحسم</th>
                   
<!--                    <th>Is Selected</th>-->
                    <th>الاجراءات</th>

                  </tr>
                  </thead>
                  <tbody>
                      <?php if(isset($data) && !empty($data)) { ?>
                      <?php foreach($data as $d1) { ?>
                  <tr>
                  
                    <td ><?php 
                        echo $d1->code;
                    ?>
                  </td>
           
                  <td >
                    <?php
                     echo $d1->start_date;
                    ?>
                  </td>
                  <td >
                    <?php
                     echo $d1->end_date;
                    ?>
                  </td>
                  <td >
                    <?php
                     echo $d1->discount;
                    ?>
                  </td>
                  <td>
                        <button type="button"  class="btn btn-danger btn-sm delete" data-id="<?php echo $d1->id; ?>"> <i class="fas fa-trash">
                              </i>حذف</button>
                       
                         
                          <!-- <a class="btn btn-success btn-sm reset"  href="<?=site_url('Ads/ads_category/'.$d1->id)?>">
                          <i class="fas fa-sort"></i>
                            الاعلانات
                          </a> -->
                      </td>
                  </tr>
                      <?php } } ?>

                  </tbody>
                </table>
                   <div class="card-footer clearfix">
              
              </div>
              </div>
                
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="<?=site_url('assets/')?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=site_url('assets/')?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=site_url('assets/')?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=site_url('assets/')?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=site_url('assets/')?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=site_url('assets/')?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?=site_url('assets/')?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo site_url('assets/') ?>plugins/sweetalert2/sweetalert2.min.js"></script>
  <script>
   $('#example2').DataTable({
    
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $(".delete").click(function() {
  
  const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: false
})


var id=$(this).data('id');



swalWithBootstrapButtons.fire({
  title: 'Are you Sure',
  text: 'to delete it?',
  icon: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Ok',
  cancelButtonText: 'Cancel',
  reverseButtons: true
}).then((result) => {
  if (result.value) {
    $.ajax({
            type: 'POST',
            url: "<?php echo site_url() ?>Admin/Category/delete/"+id,
            dataType: "text",
           
            type: 'POST',
            success: function (output) {
              // alert(output)
              if(output==1){
                swalWithBootstrapButtons.fire(
      'Ok',
      'The operation was successful',
      'success'
    )
    $("#body").load(window.location + "#body");
             }
                    else{
                      swalWithBootstrapButtons.fire(
      'Ok',
      'Something went wrong, please try again later',
      'warning'
    )
                    }

            }
        });
    
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Cancelled',
      
      
    )
    window.location="<?php echo site_url() ?>Category";
  }
})
});
  </script>