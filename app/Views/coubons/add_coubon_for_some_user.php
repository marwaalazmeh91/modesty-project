

  <!-- Content Wrapper. Contains page content -->
  <link rel="stylesheet" href="<?=site_url('assets/')?>plugins/select2/css/select2.min.css">
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2 mainbeard">
          <div class="col-sm-6">
            <!-- <h1>Add Category</h1> -->
          </div>
          <div class="col-sm-6 breadrtl">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">الصفحة الرئيسية</a></li>
              <li class="breadcrumb-item active">إضافة كوبون</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
<!--                <h3 class="card-title">Add Category</h3>-->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" id="addform">
                <div class="card-body">
    
                <div class="form-group">
                  <label>المستخدمين</label>
                  <div class="select2-purple">
                        <select class="form-control select2 select2-danger"  multiple="multiple" id="products" name="users">
             <?php if(isset($users) && !empty($users)){  
               foreach($users as $user){
               ?>
                       <option class="user" value="<?php echo $user->token ?>" ><?php echo $user->first_name.' '.$user->last_name ?></option>
                
                       <?php }} ?>
                        </select>

                  </div>
                 </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">تاريخ البداية</label>
                    <input  class="form-control" name="start_date"  type="datetime-local" required>
                   
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">تاريخ النهاية</label>
                    <input  class="form-control" name="end_date"  type="datetime-local" required>
                   
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">الحسم</label>
                     <input  type="number" class="form-control" name="discount" type="text" placeholder="discount" >
                  </div>
                <!-- /.card-body -->

                <div class="card-footer" style="display: flex;">
                    <a>
                    <button type="submit" name="save" class="btn button_color">حفظ</button></a>
                    &ensp;
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
</div>

<script>

$('#addform').submit(function () {
          var form_data = new FormData($("#addform")[0]);
     
          form_data.append('action','add')
 
          form_data.append('action','add')
          event.preventDefault();
          $.ajax({ url: '<?php echo  site_url().'admin/Coubons/save_coubon_for_some_user'?>',
              data: form_data,
              type: 'post',
              //  cache:false,
              processData: false,
              contentType: false,
              beforeSend: function(){
                 
              },
              success: function(output) {
                  var response=JSON.stringify(output)

                  if(output==true){
                      window.location="<?=site_url()?>admin/Coubons";
                  }
                  else{
                      $('#result').append('<div class="alert alert-danger" role="alert">Some thing wrong please try later</div>');

                  }
              }
          });
      });
      



</script>
            