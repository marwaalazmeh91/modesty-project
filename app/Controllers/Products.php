<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use App\Models\ProductModel;
use App\Models\CartModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class Products extends BaseController
{ use ResponseTrait;
    public function __construct()
    {
      
    }
    public function GetProducDetails(){
        $product_id=$this->request->getVar('product_id');
        $user_id=$this->request->getVar('user_id');
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if(!$product_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie ein Produktfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen bir ürün alanı girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter a product field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المنتج');
                return $this->respond($result,400);
                exit;
            }
        }
           $producr_model=new ProductModel();
           $cart_model=new CartModel();
           $user_model=new UserModel();
           $rate="0";
           $product=$producr_model->get_product_details($product_id,$language->getValue());
   
          $discount=$product[0]->price*($product[0]->discount/100);
            $product[0]->price_after_discouunt=number_format($product[0]->price-$discount,2, '.', '');
           $product_color=$producr_model->get_product_color($product_id);
           $product_reviews=$producr_model->get_product_reviews($product_id);
           $cart_user2=array();
           $favourite_user2=array();
           if(isset($user_id)&&!empty($user_id)){
            $cart_user=$cart_model->get_cart_user($user_id);
    
            foreach($cart_user as $cart){
array_push($cart_user2,$cart->product_id);
            }
        }
     
        if(isset($user_id)&&!empty($user_id)){
            $favouirte_user=$user_model->get_favourite_user($user_id);
  
            foreach($favouirte_user as $f){
array_push($favourite_user2,$f->product_id);
            }
        }
           if(!empty($product_reviews)){
            if(!empty($product_reviews[0]->sum)){
                $rate2=($product_reviews[0]->sum* 5)/(5*$product_reviews[0]->count);
                $rate=number_format(floor($rate2*100)/100,1, '.', '');
            }
            
            else{
                $rate="0";
            }
           }
           if(!empty($cart_user2)){
            if(in_array($product_id,$cart_user2)){
                $product[0]->is_found=1;
            }
            else{
                $product[0]->is_found=0;
            }
        }
        else{
            $product[0]->is_found=0;
        }
    
        if(!empty($favourite_user2)){
            if(in_array($product_id,$favourite_user2)){
                $product[0]->is_favourite=1;
            }
            else{
                $product[0]->is_favourite=0;
            }
        }
        else{
            $product[0]->is_favourite=0;
        }
      $colors=array();
      $new_colors=array();
        foreach ($product_color as $element) {
            $colors[$element->size][] = $element;
        }
        $a=0;
        $b=0;
       
        foreach($colors as $key=>$value){
           
        $new_colors[$a]['size']=$key;
        foreach($value as $c){
          if($c->quantity!=0){
            $new_colors[$a]['property'][]=array('color'=>$c->color,'qunatity'=>$c->quantity,'image_url'=>$c->image_url);
            $b++;
          }
          else{
            $new_colors[$a]['property']=[];
          }
        }
        $a++;
        }
     
           $data=array('product_details'=>$product,'product_color'=>$new_colors,'product_reviews'=>$rate);
           $result=array('code'=>1,'msg'=>'success','data'=>$data);
return $this->respond($result,200);
    }
    public function GetUserFavouirteProducts(){
        if($this->request->getMethod()=='get'){
  
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $check = new Check(); // Create an instance
        $result=$check->check();
    
        if($result['code']==1){
    
      
            $user_id=$this->request->getVar('user_id');
        if(!$user_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the item field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                return $this->respond($result,400);
                exit;
            }
        }
        $user_model=new UserModel();
        $cart_model = new CartModel();
        $favourite_product=$user_model->get_favourite_products_by_user($language->getValue(),$user_id);
         
        $products=array();
     
        $cart_user=0;
        if(isset($user_id)&&!empty($user_id)){
            $cart_user=$cart_model->get_cart_user($user_id);
        }
  
        $cart_user2=array();
        if(!empty($cart_user)){
        foreach($cart_user as $cart){
array_push($cart_user2,$cart->product_id);
        }
    }
      
        if(isset($favourite_product) && !empty($favourite_product)){
            $i=0;
            foreach($favourite_product as $proudct){
                if($proudct->id!=null){
                $products[$i]['id']=$proudct->id;
            $products[$i]['name']=$proudct->name;
            $products[$i]['price']=$proudct->price;
            $products[$i]['discount']=$proudct->discount;
            $products[$i]['image_url']=$proudct->image_url;
            $products[$i]['is_new']=$proudct->is_new;
            if(!empty($proudct->sum)){
                $rate2=($proudct->sum* 5)/(5*$proudct->count);
                $rate=number_format(floor($rate2*100)/100,1, '.', '');
            }
            
            else{
                $rate="0";
            }
            $products[$i]['rate']=$rate;
            $discount=$proudct->price*($proudct->discount/100);
            $products[$i]['price_after_discouunt']=number_format($proudct->price-$discount,2, '.', '');
            if(!empty($cart_user2)){
                if(in_array($proudct->id,$cart_user2)){
                    $products[$i]['is_found']=1;
                }
                else{
                    $products[$i]['is_found']=0;
                }
            }
            else{
                $products[$i]['is_found']=0;
            }
            $products[$i]['favourite_id']=$proudct->favourite_id;
        }
            $i++;
            }
        }
  
        $result=array('code'=>1,'msg'=>'success','data'=>$products);
        return $this->respond($result,200);
    }

else{
    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
);
return $this->respond($result,400);
}
}
else{
    $data=array('code'=>-1,'msg'=>'Method must be GET','data'=>[]);
return	$this->respond($data, 200);
    }
    }
    public function AddFavouriteProduct(){
        if($this->request->getMethod()=='post'){
          
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            $check = new Check(); // Create an instance
            $result=$check->check();
        
            if($result['code']==1){

                $user_id=$this->request->getVar('user_id');
                $product_id=$this->request->getVar('product_id');
               
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Benutzerfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the user field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستخدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$product_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Produkt ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen ürünü girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the product');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال  المنتج');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                $product_model=new ProductModel();
                $favourte_user=0;
        
                    $favourte_user=$product_model->get_favourte_user($user_id,$product_id);
                
        
                if(!empty($favourte_user)){
                    
                        if( $language->getValue()=="de"){
                            $result=array('code'=>-1,'msg'=>'Das Produkt ist bereits in den Favoriten');
                            return $this->respond($result,200);
                            exit;
                        }
                        if( $language->getValue()=="tr"){
                            $result=array('code'=>-1,'msg'=>'Ürün zaten favorilerde');
                            return $this->respond($result,200);
                            exit;
                        }
                        if( $language->getValue()=="en"){
                            $result=array('code'=>-1,'msg'=>'The product is already in the favourites');
                            return $this->respond($result,200);
                            exit;
                        }
                        if( $language->getValue()=="ar"){
                            $result=array('code'=>-1,'msg'=>'المنتج موجود مسبقاً في المفضلة');
                            return $this->respond($result,200);
                            exit;
                        }
                    
                }
              
                    $data=array('user_id'=>$user_id,'product_id'=>$product_id);
                    $insert=$product_model->add_user_favourite($data);
                    if($insert>0){
                        $result=array('code'=>1,'msg'=>'success');
                        return $this->respond($result,200);
                        exit;
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'fail');
                        return $this->respond($result,200);
                        exit;
                    }
                  
                
                }

                else{
                    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
                );
                return $this->respond($result,400);
                }
        }
        else{
            $result=array('code'=>-1,'msg'=>'Method must be POST',
            );
            return $this->respond($result,400);
        }
    }
    public function DeleteProductFromFavourite(){
        if($this->request->getMethod()=='post'){
          
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            $check = new Check(); // Create an instance
            $result=$check->check();
        
            if($result['code']==1){

                $id=$this->request->getVar('id');
                $user_id=$this->request->getVar('user_id');
        
                if(!$id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Tariffeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen tarife alanını giriniz');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the id field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال  حقل التعرفة');
                        return $this->respond($result,400);
                        exit;
                    }
                }
              
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Benutzerfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the user field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستخدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
        $user_model=new UserModel();
                    $data=array('id'=>$id);$user_id=array('user_id'=>$user_id);
                    $delete=$user_model->delete_from_favourite($data,$user_id);
                    if($delete==1){
                        $result=array('code'=>1,'msg'=>'success');
                        return $this->respond($result,200);
                        exit;
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'fail');
                        return $this->respond($result,200);
                        exit;
                    }
                  
                
                }

                else{
                    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
                );
                return $this->respond($result,400);
                }
        }
        else{
            $result=array('code'=>-1,'msg'=>'Method must be POST',
            );
            return $this->respond($result,400);
        }
    }
    public function SearchProducts(){
        if($this->request->getMethod()=='get'){
  
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
    
    
      
            $user_id=$this->request->getVar('user_id');
            $key=$this->request->getVar('key');
        if(!$key){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Schlüsselfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen anahtar alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the key field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل المفتاح');
                return $this->respond($result,400);
                exit;
            }
        }
        $product_model=new ProductModel();
    $new_products=array();
        $products=$product_model->search_products($language->getValue(),$key);
          
     if(!empty($products)){
       
         $i=0;
         foreach($products as $product){
            $new_products[$i]['id']=$product->id  ;      
$new_products[$i]['name']=$product->name;
$new_products[$i]['image_url']=$product->image_url;
if(isset($user_id) && !empty($user_id)){
    $keyisfound=$product_model->get_user_result($user_id,$product->id,$language->getValue());
    if(empty($keyisfound)){
 
    $data=array('product_id'=>$product->id,'user_id'=>$user_id,'language'=>$language->getValue());
    
$product_model->add_key($data);
         }
}
        $i++;
     }
$result_search=$product_model->get_search_result($user_id,$language->getValue());

  $result=array('results_search'=>$new_products,'lates_result'=>$result_search);
        $result=array('code'=>1,'msg'=>'success','data'=>$result);
        return $this->respond($result,200);
    }
    else{
        $result=array('code'=>-1,'msg'=>'no data found');
               return $this->respond($result,400);
    }

}
else{
    $data=array('code'=>-1,'msg'=>'Method must be GET','data'=>[]);
return	$this->respond($data, 200);
    }
    }
}