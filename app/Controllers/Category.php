<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\CategoryModel;
use App\Models\ProductModel;
use App\Models\CartModel;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class Category extends BaseController
{ use ResponseTrait;
    public function __construct()
    {
      
    }
    public function GetAllCategory(){
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $category_model=new CategoryModel();
        $product=$category_model->get_all_category($language->getValue());
        $result=array('code'=>1,'msg'=>'success','data'=>$product);
        return $this->respond($result,200);
    }
    public function GetSubCategoryByCategory(){
        $category_id=$this->request->getVar('category_id');
        $sub_category_id=$this->request->getVar('sub_category_id');
        $user_id=$this->request->getVar('user_id');
        $language= $this->request->getHeader('lang');
        $page=$this->request->getVar('page');
              
			$limit=$this->request->getVar('limit');
        if($user_id){
            $check = new Check(); // Create an instance
			$result=$check->check();
            if($result['code']!=1){
                    $result=array('code'=>$result['code'],'msg'=>$result['messages']);
                    return $this->respond($result,400);
            }
        }
      
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if(!$category_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the item field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  الصنف');
                return $this->respond($result,400);
                exit;
            }
        }
        $category_model=new CategoryModel();
        $product_model=new ProductModel();
        $cart_model = new CartModel();

        $user_model = new UserModel();
        $sub_category=$category_model->get_sub_category_by_category($language->getValue(),$category_id);
        if(empty($sub_category_id)){
            if(!empty($sub_category[0]->id)){
            $product_by_sub=$product_model->get_products_by_sub_category($language->getValue(),$sub_category[0]->id);
            }
        }
        else{
            $product_by_sub=$product_model->get_products_by_sub_category($language->getValue(),$sub_category_id);
        }
            
        $products=array();
     
        $cart_user=0;
        $favourite_user2=array();
        if(isset($user_id)&&!empty($user_id)){
            $cart_user=$cart_model->get_cart_user($user_id);
        }
  
        $cart_user2=array();
        if(!empty($cart_user)){
        foreach($cart_user as $cart){
array_push($cart_user2,$cart->product_id);
        }
    }
    if(isset($user_id)&&!empty($user_id)){
        $favouirte_user=$user_model->get_favourite_user($user_id);

        foreach($favouirte_user as $f){
array_push($favourite_user2,$f->product_id);
        }
    }
 
        if(isset($product_by_sub) && !empty($product_by_sub)){
            $i=0;
            foreach($product_by_sub as $proudct){
                if($proudct->id!=null){
                $products[$i]['id']=$proudct->id;
            $products[$i]['name']=$proudct->name;
            $products[$i]['price']=$proudct->price;
            $products[$i]['discount']=$proudct->discount;
            $products[$i]['image_url']=$proudct->image_url;
            $products[$i]['is_new']=$proudct->is_new;
            if(!empty($proudct->sum)){
                $rate2=($proudct->sum* 5)/(5*$proudct->count);
                $rate=number_format(floor($rate2*100)/100,1, '.', '');
            }
            
            else{
                $rate="0";
            }
            $products[$i]['rate']=$rate;
            $discount=$proudct->price*($proudct->discount/100);
            $products[$i]['price_after_discouunt']=number_format($proudct->price-$discount,2, '.', '');
            if(!empty($cart_user2)){
                if(in_array($proudct->id,$cart_user2)){
                    $products[$i]['is_found']=1;
                }
                else{
                    $products[$i]['is_found']=0;
                }
            }
            else{
                $products[$i]['is_found']=0;
            }
            if(!empty($favourite_user2)){
                if(in_array($proudct->id,$favourite_user2)){
                    $products[$i]['is_favourite']=1;
                }
                else{
                    $products[$i]['is_favourite']=0;
                }
            }
            else{
                $products[$i]['is_favourite']=0;
            }
            $i++;
            }
            }
        }
        $data=array('sub_category'=>$sub_category,'products'=>$products);
        $result=array('code'=>1,'msg'=>'success','data'=>$data);
        return $this->respond($result,200);
    }
}