<?php namespace App\Controllers;
use App\Models\UserModel;
use App\Models\SchoolModel;

use CodeIgniter\RESTful\ResourceController;
// . ' and tickets_reply.id in (select max(id) from tickets_reply group by ticket_id)  order by tickets_reply.id desc's
use \Firebase\JWT\JWT;
class Auth extends ResourceController
{
    
        public function FirstRegister(){
            if( $this->request->getMethod() =='post'){
          
            $model = new UserModel();
            $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
  
       
            $mr=$this->request->getVar('mr');
            $first_name=$this->request->getVar('first_name');
            $last_name=$this->request->getVar('last_name');
     
            $email=$this->request->getVar('email');
            $password=$this->request->getVar('password');
       
            $phone=$this->request->getVar('phone');

            
            if(!$mr){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Feld für den Benutzertyp ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı türü alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter user type field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل نوع المستخدم');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$first_name){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Vornamensfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen ad alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the first name field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الاسم الأول');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$last_name){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Nachnamensfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen soyadı alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the last name field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الاسم الأخير');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$phone){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Handynummernfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen cep telefonu numarası alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the mobile number field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل رقم الموبايل');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$email){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the email field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                    return $this->respond($result,400);
                    exit;
                }
        
            }
            if(!$password){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Passwortfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen şifre alanını giriniz');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the password field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$phone){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الهاتف  ');
              return $this->respond($result,400);
              exit;
            }
          
          $model=new UserModel();
          $check_email=$model->get_user_email($email);
          $check_phone=$model->get_user_phone($phone);
          if(!empty($check_phone)){
            $result=array('code'=>-1,'msg'=>'رقم الهاتف موجود مسبقا');
            return $this->respond($result,400);
            exit;
          }
          if(empty($check_email)){
     
    
   
    
            $to = $email;
            $code='';
            for ($i = 0; $i<4; $i++) 
            {
                $code .= mt_rand(1,9);
            }
$subject = 'Verfiy Account';

$headers = "From: modesty@gmail.com\r\n";
$headers .= "Reply-To: ". $email . "\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

$message = '<p>Code for Verfiy Account is '.$code.'</p>';


if(mail($to, $subject, $message, $headers)){
    
    $data = [
 
        'mr'=>$mr,
        'password'=>md5($password),
        'first_name'=>$first_name,
        'last_name'=>$last_name,
        'phone'=>$phone,
        'email'=>$email,
   'code'=>$code,
        'active'=>0
      
        ];

    $save = $model->add_user($data);

if($save>0){
    $result=array('code'=>1,'msg'=>'success','data'=>$save
);
return $this->respond($result,200);
}
else{
    $result=array('code'=>-1,'msg'=>'fail',
);
return $this->respond($result,200);
}
}
           
else{
    $result=array('code'=>-1,'msg'=>'Something went wrong, please try again later');
    return $this->respond($result,400);
    exit;
}  
    }
    else{
        if( $language->getValue()=="de"){
            $result=array('code'=>-10,'msg'=>'E-Mail existiert bereits');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="tr"){
            $result=array('code'=>-10,'msg'=>'Bu e-posta zaten var');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="en"){
            $result=array('code'=>-10,'msg'=>'Email already exists');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="ar"){
            $result=array('code'=>-10,'msg'=>'الايميل موجود مسبقا');
            return $this->respond($result,400);
            exit;
        }
        
    }

    

    }
    else{
        $result=array('code'=>-1,'msg'=>'Method must be POST',
        );
        return $this->respond($result,400);
    }
    }
  public function test(){
   
    $check = new Check(); // Create an instance
   $result=$check->check();
   var_dump($result['code']);
   $result=json_encode($result['data']->exp);
   return $this->respond($result,400);
  
  }
  public function SecondRegister(){
    if( $this->request->getMethod() =='post'){
  
    $model = new UserModel();
    $language= $this->request->getHeader('lang');
    if(!isset($language)){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    if( strlen( $language->getValue())==0){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }


    $country=$this->request->getVar('country');

    $city=$this->request->getVar('city');

    $street=$this->request->getVar('street');
    $home_number=$this->request->getVar('home_number');

    $note=$this->request->getVar('note');
    $state=$this->request->getVar('state');
    $user_id=$this->request->getVar('user_id');
    
    if(!$user_id){
        if( $language->getValue()=="de"){
            $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Benutzerfeld ein');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="tr"){
            $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı alanını girin');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="en"){
            $result=array('code'=>-1,'msg'=>'Please enter the user field');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="ar"){
            $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستخدم');
            return $this->respond($result,400);
            exit;
        }
    }
    if(!$country){
        if( $language->getValue()=="de"){
            $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Länderfeld ein');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="tr"){
            $result=array('code'=>-1,'msg'=>'Lütfen ülke alanını girin');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="en"){
            $result=array('code'=>-1,'msg'=>'Please enter the country field');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="ar"){
            $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الدولة');
            return $this->respond($result,400);
            exit;
        }
    }
    
    if(!$city){
        if( $language->getValue()=="de"){
            $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Stadtfeld ein');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="tr"){
            $result=array('code'=>-1,'msg'=>'Lütfen şehir alanını girin');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="en"){
            $result=array('code'=>-1,'msg'=>'Please enter the city field');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="ar"){
            $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل المدينة ');
            return $this->respond($result,400);
            exit;
        }
    }
    if(!$street){
        if( $language->getValue()=="de"){
            $result=array('code'=>-1,'msg'=>'Bitte Straßenfeld eingeben');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="tr"){
            $result=array('code'=>-1,'msg'=>'Lütfen sokak alanını girin');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="en"){
            $result=array('code'=>-1,'msg'=>'Please enter street field');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="ar"){
            $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الشارع');
            return $this->respond($result,400);
            exit;
        }

    }

  
  $model=new UserModel();

$data = [

'country'=>$country,
'city'=>$city,
'street'=>$street,
'home_number'=>$home_number,
'note'=>$note,
'user_id'=>$user_id,
'is_deafult'=>1

];

$save = $model->add_address($data);

if($save>0){
                $iat=date('Y-m-d');

            $a=strtotime($iat.'+1 year');
           
      
            $exp =  date('Y-m-d',$a);
    
          
            $payload = array(
                "iss" => "The_modesty",
                "aud" => "The_rewr",
                "iat" => $iat,
             
                "exp" => $exp,
                "data" => $user_id,
            );
           
            $kunci='M0dEstY';
            $output = JWT::encode($payload,$kunci );
      $data['token']=$output;
$result=array('code'=>1,'msg'=>'success','data'=>$data
);
return $this->respond($result,200);
}
else{
$result=array('code'=>-1,'msg'=>'fail',
);
return $this->respond($result,200);
}
  

}
else{
$result=array('code'=>-1,'msg'=>'Method must be POST',
);
return $this->respond($result,400);
}
  }
  public function RegisterWithFacebook(){
    if( $this->request->getMethod() =='post'){
  
    $model = new UserModel();
    $language= $this->request->getHeader('lang');
    if(!isset($language)){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    if( strlen( $language->getValue())==0){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }


    $email=$this->request->getVar('email');
    $profile_img=$this->request->getVar('profile_img');
    $name=$this->request->getVar('name');
    
    if(!$email){
        if( $language->getValue()=="de"){
            $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="tr"){
            $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="en"){
            $result=array('code'=>-1,'msg'=>'Please enter the email field');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="ar"){
            $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
            return $this->respond($result,400);
            exit;
        }

    }
    
   
    $model=new UserModel();
    
    $check_email=$model->get_user_email($email);
  if(empty($check_email)){
$name=explode(" ",$name);

$data = [

'email'=>$email,
'first_name'=>$name[0],
'last_name'=>$name[1],
'profile_img'=>$profile_img,
'active'=>1

];

$save = $model->add_user($data);
$iat=date('Y-m-d');

$a=strtotime($iat.'+1 year');


$exp =  date('Y-m-d',$a);


$payload = array(
    "iss" => "The_modesty",
    "aud" => "The_rewr",
    "iat" => $iat,
 
    "exp" => $exp,
    "data" => $save,
);

$kunci='M0dEstY';
$output = JWT::encode($payload,$kunci );

if($save>0){
    $data=array('email'=>$email,'user_id'=>$save,'token'=>$output);
$result=array('code'=>1,'msg'=>'success','data'=>$data
);
return $this->respond($result,200);
}
else{
$result=array('code'=>-1,'msg'=>'fail',
);
return $this->respond($result,200);
}
  
  }
  else{
    $result=$model->loginwithfacebook($email);
      
    if(!empty($result)){

        $iat=date('Y-m-d');

        $a=strtotime($iat.'+1 year');
       
  
        $exp =  date('Y-m-d',$a);

      
        $payload = array(
            "iss" => "The_modesty",
            "aud" => "The_rewr",
            "iat" => $iat,
         
            "exp" => $exp,
            "data" => $result->id,
        );
       
        $kunci='M0dEstY';
        $output = JWT::encode($payload,$kunci );
  
    
        $data=array('email'=>$result->email,'user_id'=>$result->id,'token'=>$output,'first_name'=>$result->first_name,'last_name'=>$result->last_name);
        
        $result=array('code'=>1,'msg'=>'success','data'=>$data
    );
    return $this->respond($result,200);
    }
    else{
        $result=array('code'=>-1,'msg'=>'Username or Password incorrect',
    );
    return $this->respond($result,400);
    }
}
    }

  

else{
$result=array('code'=>-1,'msg'=>'Method must be POST',
);
return $this->respond($result,400);
}
  }
// public function test(){

// $check = new Check(); // Create an instance
// $result=$check->check();
// var_dump($result['code']);
// $result=json_encode($result['data']->exp);
// return $this->respond($result,400);

// }
  public function Login(){
    if( $this->request->getMethod() =='post'){
        $email=$this->request->getVar('email');
        $password=$this->request->getVar('password');
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
		}
		if( strlen( $language->getValue())==0){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
        }
       
        if(!$email){
            if( $language->getValue()=="ger"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tur"){
                $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the email field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                return $this->respond($result,400);
                exit;
            }
    
        }
        if(!$password){
            if( $language->getValue()=="ger"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Passwortfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tur"){
                $result=array('code'=>-1,'msg'=>'Lütfen şifre alanını giriniz');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the password field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                return $this->respond($result,400);
                exit;
            }
        }
        $model=new UserModel();
        $result=$model->login($email,$password);
      
        if(!empty($result)){

            $iat=date('Y-m-d');

            $a=strtotime($iat.'+1 year');
           
      
            $exp =  date('Y-m-d',$a);
    
          
            $payload = array(
                "iss" => "The_modesty",
                "aud" => "The_rewr",
                "iat" => $iat,
             
                "exp" => $exp,
                "data" => $result->id,
            );
           
            $kunci='M0dEstY';
            $output = JWT::encode($payload,$kunci );
      
        
            $data=array('email'=>$result->email,'user_id'=>$result->id,'token'=>$output,'first_name'=>$result->first_name,'last_name'=>$result->last_name,'profile_img'=>$result->profile_img);
            
            $result=array('code'=>1,'msg'=>'success','data'=>$data
        );
        return $this->respond($result,200);
        }
        else{
            $result=array('code'=>-1,'msg'=>'Username or Password incorrect',
        );
        return $this->respond($result,400);
        }
    }
  else{
        $result=array('code'=>-1,'msg'=>'Method must be POST',
        );
        return $this->respond($result,400);
    }
}
public function LoginWithFacebook(){
    if( $this->request->getMethod() =='post'){
        $email=$this->request->getVar('email');
    
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
		}
		if( strlen( $language->getValue())==0){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
        }
       
        if(!$email){
            if( $language->getValue()=="ger"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tur"){
                $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the email field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                return $this->respond($result,400);
                exit;
            }
    
        }
        
        $model=new UserModel();
        $result=$model->loginwithfacebook($email);
      
        if(!empty($result)){

            $iat=date('Y-m-d');

            $a=strtotime($iat.'+1 year');
           
      
            $exp =  date('Y-m-d',$a);
    
          
            $payload = array(
                "iss" => "The_modesty",
                "aud" => "The_rewr",
                "iat" => $iat,
             
                "exp" => $exp,
                "data" => $result->id,
            );
           
            $kunci='M0dEstY';
            $output = JWT::encode($payload,$kunci );
      
        
            $data=array('email'=>$result->email,'user_id'=>$result->id,'token'=>$output,'first_name'=>$result->first_name,'last_name'=>$result->last_name);
            
            $result=array('code'=>1,'msg'=>'success','data'=>$data
        );
        return $this->respond($result,200);
        }
        else{
            $result=array('code'=>-1,'msg'=>'Username or Password incorrect',
        );
        return $this->respond($result,400);
        }
    }
  else{
        $result=array('code'=>-1,'msg'=>'Method must be POST',
        );
        return $this->respond($result,400);
    }
}
public function SendRequestResetPassword(){
    if( $this->request->getMethod() =='post'){
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
		}
		if( strlen( $language->getValue())==0){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
        }
        $email=$this->request->getVar('email');
        if(!$email){
            if( $language->getValue()=="ger"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tur"){
                $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the email field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                return $this->respond($result,400);
                exit;
            }
        }
        $model=new UserModel();
        $check_email=$model->get_user_email($email);
        if(empty($check_email)){
            $result=array('code'=>-1,'msg'=>'الايميل غير موجود ',
        );
        return $this->respond($result,400);
        }
        else{
            $to = $email;
            $code='';
            for ($i = 0; $i<4; $i++) 
            {
                $code .= mt_rand(1,9);
            }
$subject = 'Reset password';

$headers = "From: modesty@gmail.com\r\n";
$headers .= "Reply-To: ". $email . "\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

$message = '<p>Code for Reset password is '.$code.'</p>';


if(mail($to, $subject, $message, $headers)){
    
    $data=array('code'=>$code);
$update=$model->update_user_code($email,$data);
if($update==1){
    $result=array('code'=>1,'msg'=>'success','data'=>$data
);
return $this->respond($result,200);
}
else{
    $result=array('code'=>-1,'msg'=>'fail',
);
return $this->respond($result,200);
}
}
else{
    $result=array('code'=>-1,'msg'=>'fail',
);
return $this->respond($result,200);
}
        }
    }
    else{
        $result=array('code'=>-1,'msg'=>'Method must be POST',
        );
        return $this->respond($result,400);
    }
}
public function ConfirmResetPassword(){
    if( $this->request->getMethod() =='post'){
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
		}
		if( strlen( $language->getValue())==0){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
        }
        $email=$this->request->getVar('email');
        $code=$this->request->getVar('code');
        if(!$email){
            if( $language->getValue()=="ger"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tur"){
                $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the email field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                return $this->respond($result,400);
                exit;
            }
        }
        if(!$code){
            if( $language->getValue()=="ger"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tur"){
                $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the email field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                return $this->respond($result,400);
                exit;
            }
        }
        $model=new UserModel();
        $check_email=$model->get_user_email_code($email,$code);
        if(empty($check_email)){
            $result=array('code'=>-1,'msg'=>'رمز التأكيد خطأ',
        );
        return $this->respond($result,400);
        }
        else{
    
    $result=array('code'=>1,'msg'=>'success',
    );
    return $this->respond($result,200);
   

        }
    }
    else{
        $result=array('code'=>-1,'msg'=>'Method must be POST',
        );
        return $this->respond($result,400);
    }
}
public function ResetPassword(){
    if( $this->request->getMethod() =='post'){
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
		}
		if( strlen( $language->getValue())==0){
			$result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
			return $this->respond($result,400);
            exit;
        }
        $email=$this->request->getVar('email');
        $password=$this->request->getVar('password');
        if(!$email){
            if( $language->getValue()=="ger"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tur"){
                $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the email field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                return $this->respond($result,400);
                exit;
            }
        }
        if(!$password){
            if( $language->getValue()=="ger"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Passwortfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tur"){
                $result=array('code'=>-1,'msg'=>'Lütfen şifre alanını giriniz');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the password field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                return $this->respond($result,400);
                exit;
            }
        }
        $model=new UserModel();
        $check_email=$model->get_user_email($email);
        if(empty($check_email)){
            $result=array('code'=>-1,'msg'=>'الايميل غير موجود ',
        );
        return $this->respond($result,400);
        }
        else{
         $password_new=array('password'=>md5($password));  
        
$result=$model->resetpassword($email,$password_new);
if($result==1){
    $result=array('code'=>1,'msg'=>'success',
    );
    return $this->respond($result,200);
    }
    else{
        $result=array('code'=>-1,'msg'=>'fail',
    );
    return $this->respond($result,400);
    }

        }
    }
    else{
        $result=array('code'=>-1,'msg'=>'Method must be POST',
        );
        return $this->respond($result,400);
    }
}
public function Logout(){

    if($this->request->getMethod()=='post'){
        $check = new Check(); // Create an instance
        $result=$check->check();
    
        if($result['code']==1){
    
            $request = service('request');
            $test= $request->getHeader('Authorization');
          $token=$test->getValue();
            unset($token);
            
        
    }
    else{
        $result=array('code'=>$result['code'],'msg'=>$result['messages'],
    );
    return $this->respond($result,400);
    }
    }
else{
$data=array('code'=>-1,'msg'=>'Method must be POST','data'=>[]);
return	$this->respond($data, 200);
}
}
}


   
