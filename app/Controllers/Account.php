<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class Account extends BaseController
{ use ResponseTrait;
    public function CheckUserAccount(){
        if($this->request->getMethod()=='get'){
        
		
		
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
                $user_id=$this->request->getVar('user_id');
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Benutzerfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the user field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستخدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                $model=new UserModel(); 
                $result=$model->get_user_account_status($user_id);
                
                if(empty($result)){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Das Konto ist nicht aktiviert');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Hesap etkinleştirilmedi');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'The account is not activated');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الحساب غير مفعل');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                else{
                    $result=array('code'=>1,'msg'=>'success');
                    return $this->respond($result,200);
                    exit;
                }
            
        }
    }
    public function ActiveAccount(){
        if($this->request->getMethod()=='post'){
          
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
                $user_id=$this->request->getVar('user_id');
                $code=$this->request->getVar('code');
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Benutzerfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the user field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستخدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$code){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie den Aktivierungscode ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen aktivasyon kodunu girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the activation code');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال رمز التفعيل');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                $model=new UserModel(); 
                $result=$model->get_user_code($user_id,$code);
                if(empty($result)){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Der Code ist falsch');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'kod yanlış');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'The code is incorrect');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرمز غير صحيح');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                else{
                    $data=array('active'=>1);
                    $update=$model->update_user_account($user_id,$data);
                    if($update==1){
                        $profile=$model->get_profile($user_id);
                        $result=array('code'=>1,'msg'=>'success','data'=>$profile);
                        return $this->respond($result,200);
                        exit;
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'fail');
                        return $this->respond($result,200);
                        exit;
                    }
                  
                }
            
        }
        else{
            $result=array('code'=>-1,'msg'=>'Method must be POST',
            );
            return $this->respond($result,400);
        }
    }
}