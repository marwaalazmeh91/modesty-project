<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\HomeModel;
use App\Models\CartModel;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class Home extends BaseController
{ use ResponseTrait;

    public function index(){
      
        echo view('home_page'); 
        
     }
    public function __construct()
    {
      
    }
    public function GetHome(){
        if($this->request->getMethod()=='get'){
            $check = new Check(); // Create an instance
			$result=$check->check();
		
			// if($result['code']==1){
                $language= $this->request->getHeader('lang');
                if(!isset($language)){
                    $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                    return $this->respond($result,400);
                    exit;
                }
                if( strlen( $language->getValue())==0){
                    $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                    return $this->respond($result,400);
                    exit;
                }
                $user_id=$this->request->getVar('user_id');
                $cart_user2=array();
                $favourite_user2=array();
                $home_model = new HomeModel();
                $cart_model = new CartModel();
                $user_model = new UserModel();
                $sliders=$home_model->get_slider();
                $category=$home_model->get_category($language->getValue());
                $isnewarrived=$home_model->get_product_is_new_arrived($language->getValue());
                $ischoose=$home_model->get_product_is_choose($language->getValue());
                $ads=$home_model->get_ads();
                $is_most_view=$home_model->get_product_is_most_view($language->getValue());
                $cart_user=0;
                if(isset($user_id)&&!empty($user_id)){
                    $cart_user=$cart_model->get_cart_user($user_id);
            
                    foreach($cart_user as $cart){
    array_push($cart_user2,$cart->product_id);
                    }
                }
             
                if(isset($user_id)&&!empty($user_id)){
                    $favouirte_user=$user_model->get_favourite_user($user_id);
            
                    foreach($favouirte_user as $f){
    array_push($favourite_user2,$f->product_id);
                    }
                }
             
                $productarrived=array();
                $productchoose=array();
                $productmostview=array();
                if(isset($isnewarrived) && !empty($isnewarrived)){
                    $i=0;
                    foreach($isnewarrived as $proudct){
                        $productarrived[$i]['id']=$proudct->id;
                    $productarrived[$i]['name']=$proudct->name;
                    $productarrived[$i]['price']=$proudct->price;
                    $productarrived[$i]['discount']=$proudct->discount;
                    $productarrived[$i]['image_url']=$proudct->image_url;
                    $productarrived[$i]['is_new']=$proudct->is_new;
                    if(!empty($proudct->sum)){
                        $rate2=($proudct->sum* 5)/(5*$proudct->count);
                        $rate=number_format(floor($rate2*100)/100,1, '.', '');
                    }
                    
                    else{
                        $rate="0";
                    }
                    $productarrived[$i]['rate']=$rate;
                    $discount=$proudct->price*($proudct->discount/100);
                    $productarrived[$i]['price_after_discouunt']=number_format($proudct->price-$discount,2, '.', '');
                    if(!empty($cart_user2)){
                        if(in_array($proudct->id,$cart_user2)){
                            $productarrived[$i]['is_found']=1;
                        }
                        else{
                            $productarrived[$i]['is_found']=0;
                        }
                    }
                    else{
                        $productarrived[$i]['is_found']=0;
                    }
                    if(!empty($favourite_user2)){
                        if(in_array($proudct->id,$favourite_user2)){
                            $productarrived[$i]['is_favourite']=1;
                        }
                        else{
                            $productarrived[$i]['is_favourite']=0;
                        }
                    }
                    else{
                        $productarrived[$i]['is_favourite']=0;
                    }
                    $i++;
                    }
                }
                if(isset($ischoose) && !empty($ischoose)){
                    $i=0;
                    foreach($ischoose as $proudct){
                        $productchoose[$i]['id']=$proudct->id;
                    $productchoose[$i]['name']=$proudct->name;
                    $productchoose[$i]['price']=$proudct->price;
                    $productchoose[$i]['discount']=$proudct->discount;
                    $productchoose[$i]['image_url']=$proudct->image_url;
                    $productchoose[$i]['is_new']=$proudct->is_new;
                    if(!empty($proudct->sum)){
                        $rate2=($proudct->sum* 5)/(5*$proudct->count);
                        $rate=number_format(floor($rate2*100)/100,1, '.', '');
                    }
                    
                    else{
                        $rate="0";
                    }
                    $productchoose[$i]['rate']=$rate;
                    $discount=$proudct->price*($proudct->discount/100);
                    $productchoose[$i]['price_after_discouunt']=number_format($proudct->price-$discount,2, '.', '');
                    if(!empty($cart_user2)){
                        if(in_array($proudct->id,$cart_user2)){
                            $productchoose[$i]['is_found']=1;
                        }
                        else{
                            $productchoose[$i]['is_found']=0;
                        }
                    }
                    else{
                        $productchoose[$i]['is_found']=0;
                    }
                    if(!empty($favourite_user2)){
                        if(in_array($proudct->id,$favourite_user2)){
                            $productchoose[$i]['is_favourite']=1;
                        }
                        else{
                            $productchoose[$i]['is_favourite']=0;
                        }
                    }
                    else{
                        $productchoose[$i]['is_favourite']=0;
                    }
                    $i++;
                    }
                }
                if(isset($is_most_view) && !empty($is_most_view)){
                    $i=0;
                    foreach($is_most_view as $proudct){
                        $productmostview[$i]['id']=$proudct->id;
                    $productmostview[$i]['name']=$proudct->name;
                    $productmostview[$i]['price']=$proudct->price;
                    $productmostview[$i]['discount']=$proudct->discount;
                    $productmostview[$i]['image_url']=$proudct->image_url;
                    $productmostview[$i]['is_new']=$proudct->is_new;
                    if(!empty($proudct->sum)){
                        $rate2=($proudct->sum* 5)/(5*$proudct->count);
                        $rate=number_format(floor($rate2*100)/100,1, '.', '');
                    }
                    
                    else{
                        $rate="0";
                    }
                    $productmostview[$i]['rate']=$rate;
                    $discount=$proudct->price*($proudct->discount/100);
                    $productmostview[$i]['price_after_discouunt']=number_format($proudct->price-$discount,2, '.', '');
                    if(!empty($cart_user2)){
                        if(in_array($proudct->id,$cart_user2)){
                            $productmostview[$i]['is_found']=1;
                        }
                        else{
                            $productmostview[$i]['is_found']=0;
                        }
                    }
                    else{
                        $productmostview[$i]['is_found']=0;
                    }
                    if(!empty($favourite_user2)){
                        if(in_array($proudct->id,$favourite_user2)){
                            $productmostview[$i]['is_favourite']=1;
                        }
                        else{
                            $productmostview[$i]['is_favourite']=0;
                        }
                    }
                    else{
                        $productmostview[$i]['is_favourite']=0;
                    }
                    $i++;
                    }
                }
$data=array('sliders'=>$sliders,'categorty'=>$category,'product_is_new_arrived'=>$productarrived,'product_is_choose'=>$productchoose,'ads'=>$ads,'product_most_view'=>$productmostview);
$result=array('code'=>1,'msg'=>'success','data'=>$data);
return $this->respond($result,200);
            // }
            // else{
            //     $result=array('code'=>$result['code'],'msg'=>$result['messages'],
            // );
            // return $this->respond($result,400);
            // }
        }
        else{
            $data=array('code'=>-1,'msg'=>'Method must be GET','data'=>[]);
        return	$this->respond($data, 200);
            }
    }

}