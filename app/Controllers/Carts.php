<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\OffersModel;
use App\Models\ProductModel;
use App\Models\CartModel;
use App\Models\UserModel;
use App\Models\CoubonsModel;
use App\Models\OrderModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
use \Firebase\SendNotification\SendNotification;
require APPPATH.'/Libraries/SendNotification.php';
class Carts extends BaseController
{ use ResponseTrait;
    public function __construct()
    {
      
    }
    public function GetUserCarts(){
        if($this->request->getMethod()=='get'){
  
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $check = new Check(); // Create an instance
        $result=$check->check();
    
        if($result['code']==1){
    
      
            $user_id=$this->request->getVar('user_id');
        if(!$user_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the item field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                return $this->respond($result,400);
                exit;
            }
        }
     
        $cart_model = new CartModel();
        $user_model = new UserModel();
        $carts=$cart_model->get_carts($language->getValue(),$user_id);
         
        $products=array();
     
   
        $favourite_user2=array();
        if(isset($user_id)&&!empty($user_id)){
            $favouirte_user=$user_model->get_favourite_user($user_id);
    
            foreach($favouirte_user as $f){
array_push($favourite_user2,$f->product_id);
            }
        }
        if(isset($carts) && !empty($carts)){
            $i=0;
            foreach($carts as $proudct){
                if($proudct->id!=null){
                $products[$i]['id']=$proudct->id;
            $products[$i]['name']=$proudct->name;
            $products[$i]['price']=$proudct->price;
            $products[$i]['discount']=$proudct->discount;
            $products[$i]['image_url']=$proudct->image_url;
            $products[$i]['is_new']=$proudct->is_new;
            if(!empty($proudct->sum)){
                $rate2=($proudct->sum* 5)/(5*$proudct->count);
                $rate=number_format(floor($rate2*100)/100,1, '.', '');
            }
            
            else{
                $rate="0";
            }
            $products[$i]['rate']=$rate;
            $products[$i]['quantity']=$proudct->quantity;
            $products[$i]['all_price']=$proudct->cart_price;
            $discount=$proudct->price*($proudct->discount/100);
            $products[$i]['price_after_discouunt']=number_format($proudct->price-$discount,2, '.', '');
            $products[$i]['cart_id']=$proudct->cart_id;
            if(!empty($favourite_user2)){
                if(in_array($proudct->id,$favourite_user2)){
                    $products[$i]['is_favourite']=1;
                }
                else{
                    $products[$i]['is_favourite']=0;
                }
            }
            else{
                $products[$i]['is_favourite']=0;
            }
        }
            $i++;
            }
        }
  
        $result=array('code'=>1,'msg'=>'success','data'=>$products);
        return $this->respond($result,200);
    }

else{
    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
);
return $this->respond($result,400);
}
}
else{
    $data=array('code'=>-1,'msg'=>'Method must be GET','data'=>[]);
return	$this->respond($data, 200);
    }
    }
    public function AddProductToCart(){
        if($this->request->getMethod()=='post'){
          
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            $check = new Check(); // Create an instance
            $result=$check->check();
        
            if($result['code']==1){

                $user_id=$this->request->getVar('user_id');
                $product_id=$this->request->getVar('product_id');
                $color=$this->request->getVar('color');
                $size=$this->request->getVar('size');
                $quantity=$this->request->getVar('quantity');
                $offer_id=$this->request->getVar('offer_id');
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Benutzerfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the user field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستخدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$product_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Produkt ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen ürünü girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the product');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال  المنتج');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$color){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie ein Farbfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen bir renk alanı girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter a color field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال  حقل اللون');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$size){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie ein Größenfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen bir beden alanı girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter a size field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال  حقل المقاس');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$quantity){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie ein Mengenfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen bir miktar alanı girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter a quantity field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال  حقل الكمية');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                $cart_model=new CartModel();
                $favourte_user=0;
        
                    $cart_user=$cart_model->get_cart_user2($user_id,$product_id);
                
           
                if(!empty($cart_user)){
                    
                        if( $language->getValue()=="de"){
                            $result=array('code'=>-1,'msg'=>'Das Produkt ist bereits im Warenkorb');
                            return $this->respond($result,400);
                            exit;
                        }
                        if( $language->getValue()=="tr"){
                            $result=array('code'=>-1,'msg'=>'Ürün zaten sepette');
                            return $this->respond($result,400);
                            exit;
                        }
                        if( $language->getValue()=="en"){
                            $result=array('code'=>-1,'msg'=>'The product is already in the cart');
                            return $this->respond($result,400);
                            exit;
                        }
                        if( $language->getValue()=="ar"){
                            $result=array('code'=>-1,'msg'=>'المنتج موجود مسبقاً في السلة');
                            return $this->respond($result,400);
                            exit;
                        }
                    
                }
              $proudct_model=new ProductModel();
              $offer_model=new OffersModel();
              $price=$proudct_model->get_product_by_id($product_id);
            $product_price=0;
            $discount=0;
         
              if(!empty($price)){
                if(!empty($offer_id)){
                    $offer_dicount=$offer_model->get_offer_discount_by_id($offer_id);
                    if(!empty($offer_dicount->discount)){
     
                    $discount=$price->price*($offer_dicount->discount/100);
                       }
                     $product_price=$price->price-$discount;
                 
              }
            else{
                if(!empty($price->discount)){
                    $discount=$price->price*($price->discount/100);
                      }
                    $product_price=$price->price-$discount;
            }
            }
              $product_price=$quantity*$product_price;
                    $data=array('user_id'=>$user_id,'product_id'=>$product_id,'size'=>$size,'color'=>$color,'quantity'=>$quantity,'status'=>0,'price'=>$product_price,'offer_id'=>$offer_id);
                    $insert=$cart_model->add_user_cart($data);
                    if($insert>0){
                        $result=array('code'=>1,'msg'=>'success');
                        return $this->respond($result,200);
                        exit;
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'fail');
                        return $this->respond($result,200);
                        exit;
                    }
                  
                
                }

                else{
                    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
                );
                return $this->respond($result,400);
                }
        }
        else{
            $result=array('code'=>-1,'msg'=>'Method must be POST',
            );
            return $this->respond($result,400);
        }
    }
    public function DeleteProductFromCart(){
        if($this->request->getMethod()=='post'){
          
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            $check = new Check(); // Create an instance
            $result=$check->check();
        
            if($result['code']==1){

                $cart_id=$this->request->getVar('id');
                $user_id=$this->request->getVar('user_id');
        
                if(!$cart_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Tariffeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen tarife alanını giriniz');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the id field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال  حقل التعرفة');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Benutzerfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the user field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستخدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                
        $cart_model=new CartModel();
                    $data=array('id'=>$cart_id);
                    $user_id=array('user_id'=>$user_id);
                    $delete=$cart_model->delete_cart($data,$user_id);
                    if($delete==1){
                        $result=array('code'=>1,'msg'=>'success');
                        return $this->respond($result,200);
                        exit;
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'fail');
                        return $this->respond($result,200);
                        exit;
                    }
                  
                
                }

                else{
                    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
                );
                return $this->respond($result,400);
                }
        }
        else{
            $result=array('code'=>-1,'msg'=>'Method must be POST',
            );
            return $this->respond($result,400);
        }
    }
    public function AddAddress(){
        if( $this->request->getMethod() =='post'){
      
        $model = new UserModel();
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
    
    
        $country=$this->request->getVar('country');
        $full_name=$this->request->getVar('full_name');
        $phone=$this->request->getVar('phone');
        $state=$this->request->getVar('state');
        $city=$this->request->getVar('city');
    
        $street=$this->request->getVar('street');
        $home_number=$this->request->getVar('home_number');
    $address_name=$this->request->getVar('address_name');
        $note=$this->request->getVar('note');
        $user_id=$this->request->getVar('user_id');
        if(!$full_name){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Vornamensfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen ad alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the first name field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الاسم الأول');
                return $this->respond($result,400);
                exit;
            }
        }

        if(!$phone){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Handynummernfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen cep telefonu numarası alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the mobile number field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل رقم الموبايل');
                return $this->respond($result,400);
                exit;
            }
        }
        if(!$user_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Benutzerfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the user field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستخدم');
                return $this->respond($result,400);
                exit;
            }
        }
        if(!$country){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Länderfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen ülke alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the country field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الدولة');
                return $this->respond($result,400);
                exit;
            }
        }
      
        if(!$city){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Stadtfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen şehir alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the city field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل المدينة ');
                return $this->respond($result,400);
                exit;
            }
        }
        if(!$street){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte Straßenfeld eingeben');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen sokak alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter street field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الشارع');
                return $this->respond($result,400);
                exit;
            }
    
        }
        if(!$street){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte Straßenfeld eingeben');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen sokak alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter street field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الشارع');
                return $this->respond($result,400);
                exit;
            }
    
        }
      
      $model=new UserModel();
    
    $data = [
    'full_name'=>$full_name,
    'phone'=>$phone,
    'country'=>$country,
    'state'=>$state,
    'city'=>$city,
    'street'=>$street,
    'home_number'=>$home_number,
    'note'=>$note,
    'user_id'=>$user_id,
    'is_deafult'=>0,
    'address_name'=>$address_name
    
    ];
    
    $save = $model->add_address($data);
    
    if($save>0){
    $result=array('code'=>1,'msg'=>'success','data'=>$data
    );
    return $this->respond($result,200);
    }
    else{
    $result=array('code'=>-1,'msg'=>'fail',
    );
    return $this->respond($result,200);
    }
      
    
    }
    else{
    $result=array('code'=>-1,'msg'=>'Method must be POST',
    );
    return $this->respond($result,400);
    }
      }
      public function GetAddress(){
        
        $user_id=$this->request->getVar('user_id');
        $check = new Check(); // Create an instance
        $result=$check->check();
        if($result['code']==1){
            $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            $user_id=$this->request->getVar('user_id');
            if(!$user_id){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the item field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                    return $this->respond($result,400);
                    exit;
                }
            }
            $user_model = new UserModel();
            $address=$user_model->get_address($user_id,$language->getValue());
            $data=array();
            if(isset($address) && !empty($address)){
                $i=0;
                foreach($address as $d){

$data[$i]['full_name']=$d->full_name;
$data[$i]['id']=$d->id;
$data[$i]['phone']=$d->phone;
$data[$i]['country']=$d->country;
$data[$i]['city_code']=$d->city_code;
$data[$i]['city']=$d->city;
$data[$i]['street']=$d->street;
$data[$i]['home_number']=$d->home_number;
$data[$i]['note']=$d->note;
$data[$i]['is_deafult']=$d->is_deafult;
$data[$i]['address_id']=$d->address_name;
if($d->address_name!=null){
$address_name=$user_model->get_address_name($d->address_name,$language->getValue());

$data[$i]['address_name']=$address_name->name;
}
else{
    $data[$i]['address_name']=null;
}
$i++;
                }
                $result=array('code'=>1,'msg'=>'success','data'=>$data);
                return $this->respond($result,200);
            }
            else{
                $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
                return $this->respond($result,200);
            }
    }
   else{
    $result=array('code'=>$result['code'],'msg'=>$result['messages']);
    return $this->respond($result,400);
   }
    }
    public function GetAddressNames(){
        

  
            $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
  
          
            $user_model = new UserModel();
            $address=$user_model->get_address_names($language->getValue());
            $data=array();
            if(isset($address) && !empty($address)){
            

                $result=array('code'=>1,'msg'=>'success','data'=>$address);
                return $this->respond($result,200);
            }
            else{
                $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
                return $this->respond($result,200);
            }
    
 
    } 
    public function checkproduct(){
        if($this->request->getMethod()=='post'){
            $this->db = \Config\Database::connect();
                $language= $this->request->getHeader('lang');
                if(!isset($language)){
                    $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                    return $this->respond($result,400);
                    exit;
                }
                if( strlen( $language->getValue())==0){
                    $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                    return $this->respond($result,400);
                    exit;
                }
                $check = new Check(); // Create an instance
                $result=$check->check();
            
                if($result['code']==1){


                    $user_id=$this->request->getVar('user_id');

 if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the item field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                $cart_model=new CartModel();
                $proudct_model=new ProductModel();
                $carts=$cart_model->get_carts($language->getValue(),$user_id);
                if(!empty($carts)){
                    foreach($carts as $cart){
                        
                        $quantity=$proudct_model->get_product_quantity($cart->id,$cart->size,$cart->color);
                      if(!empty($quantity)){
                        if($quantity[0]->quantity>0){
                            $q=$quantity[0]->quantity-$cart->quantity;
                         
                            if($q>=0){
                             
                        // $product=array('quantity'=>$q,'size'=>$cart->size,'color'=>$cart->color);
                        // $proudct_model->update_product_quantity($cart->id,$product);
                            }
                            else{
                                $result=array('code'=>-10,'msg'=>'no quantity enough exist');
                                return $this->respond($result,200);
                                exit;
                            }
                        }
                        else{
                            $result=array('code'=>-10,'msg'=>'no quantity enough exist');
                            return $this->respond($result,200);
                            exit;
                        }
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'no product  exist in your cart');
                        return $this->respond($result,200);
                        exit;
                    }
                }
                
                }
                else{
                    $result=array('code'=>-1,'msg'=>'no product  exist in your cart');
                    return $this->respond($result,200);
                    exit;
                }
                }
            

            else{
                $result=array('code'=>$result['code'],'msg'=>$result['messages'],
            );
            return $this->respond($result,400);
            }
                    
            }
            else{
                $result=array('code'=>-1,'msg'=>'Method must be POST',
                );
                return $this->respond($result,400);
            }
    }
    public function CompletePayment(){
        if($this->request->getMethod()=='post'){
            $this->db = \Config\Database::connect();
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            $check = new Check(); // Create an instance
            $result=$check->check();
        
            if($result['code']==1){

                $address_id=$this->request->getVar('address_id');
                $payment_method=$this->request->getVar('payment_method');
                $user_id=$this->request->getVar('user_id');
                $code=$this->request->getVar('code');
                if(!$address_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Adressfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen adres alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the address field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل العنوان');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the item field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
               
        $cart_model=new CartModel();
        $proudct_model=new ProductModel();
        $this->db->transStart();
        $carts=$cart_model->get_carts($language->getValue(),$user_id);
   
        if(!empty($carts)){
        foreach($carts as $cart){
            
            $quantity=$proudct_model->get_product_quantity($cart->id,$cart->size,$cart->color);
          if(!empty($quantity)){
            if($quantity[0]->quantity>0){
                $q=$quantity[0]->quantity-$cart->quantity;
             
                if($q>=0){
                 
            $product=array('quantity'=>$q,'size'=>$cart->size,'color'=>$cart->color);
            $proudct_model->update_product_quantity($cart->id,$product);
                }
                else{
                    $result=array('code'=>-10,'msg'=>'no quantity enough exist');
                    return $this->respond($result,200);
                    exit;
                }
            }
            else{
                $result=array('code'=>-10,'msg'=>'no quantity enough exist');
                return $this->respond($result,200);
                exit;
            }
        }
        else{
            $result=array('code'=>-1,'msg'=>'no product  exist in your cart');
            return $this->respond($result,200);
            exit;
        }
    }
    
    }
    else{
        $result=array('code'=>-1,'msg'=>'no product  exist in your cart');
        return $this->respond($result,200);
        exit;
    }
        $status=array('status'=>1,'pervious_status'=>0);
    if($cart_model->update_cart($user_id,$status)){
        $order_id=$this->generateRandomString();
        $data=array('address_id'=>$address_id,'payment_method'=>$payment_method,'order_id'=>$order_id,'status'=>1);
           $insert=$cart_model->complete_payment($data);
           if($insert>0){
            $status=array('order_id'=>$insert,'pervious_status'=>1);
            if($cart_model->update_cart($user_id,$status)){
                if($code){
                $coubon=$this->ApplyCoubon($order_id);
            //  var_dump($coubon);
            //     $coubon2=json_encode($coubon);
            //     var_dump($coubon2);
            //     exit;
                if($coubon['code']==1){
                    $this->db->transComplete();
                    $result=array('code'=>1,'msg'=>'success');
                    return $this->respond($result,200);
                    exit;
                }
                else{
                    $result=array('code'=> $coubon['code'],'msg'=>$coubon['msg']);
                    return $this->respond($result,200);
                    exit;
                }
                
                }
                $this->db->transComplete();
                        $result=array('code'=>1,'msg'=>'success');
                        return $this->respond($result,200);
                        exit;
                        
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'fail');
                        return $this->respond($result,200);
                        exit;
                    }
                }
                else{
                    $result=array('code'=>-1,'msg'=>'fail');
                    return $this->respond($result,200);
                    exit;
                }
                  
    }
    else{
        $result=array('code'=>-1,'msg'=>'fail');
        return $this->respond($result,200);
        exit;
    }
                }

                else{
                    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
                );
                return $this->respond($result,400);
                }
        }
        else{
            $result=array('code'=>-1,'msg'=>'Method must be POST',
            );
            return $this->respond($result,400);
        }
    }
    public function DeleteAddress(){
        if($this->request->getMethod()=='post'){
          
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            $check = new Check(); // Create an instance
            $result=$check->check();
        
            if($result['code']==1){

                $id=$this->request->getVar('id');
             
        
                if(!$id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Tariffeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen tarife alanını giriniz');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the id field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال  حقل التعرفة');
                        return $this->respond($result,400);
                        exit;
                    }
                }
               
        $user_model=new UserModel();
                    $data=array('id'=>$id);
              
                    $delete=$user_model->delete_address($data);
                    if($delete==1){
                        $result=array('code'=>1,'msg'=>'success');
                        return $this->respond($result,200);
                        exit;
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'fail');
                        return $this->respond($result,200);
                        exit;
                    }
                  
                
                }

                else{
                    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
                );
                return $this->respond($result,400);
                }
        }
        else{
            $result=array('code'=>-1,'msg'=>'Method must be POST',
            );
            return $this->respond($result,400);
        }
    }
    public function ApplyCoubon($order_id){
        if($this->request->getMethod()=='post'){
          
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $result;
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $result;
                exit;
            }
            $check = new Check(); // Create an instance
            $result=$check->check();
        
            if($result['code']==1){
                $user_id=$this->request->getVar('user_id');
                $code=$this->request->getVar('code');
                // $order_id=$this->request->getVar('order_id');
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the item field');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                        return $result;
                        exit;
                    }
                }
                if(!$code){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Codefeld ein');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen kod alanını girin');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the code field');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  الكود');
                        return $result;
                        exit;
                    }
                }
                if(!$order_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Bestellfeld ein');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen sipariş alanını giriniz');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the order field');
                        return $result;
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  الطلب');
                        return $result;
                        exit;
                    }
                }
                $model=new CoubonsModel();
                $order_model=new OrderModel();
                $coubon=$model->getcubonbycode($code);
                
                if(!empty($coubon)){
                    if($coubon[0]->type==2){
                        if(strpos($coubon[0]->user, $user_id) === false){
                            $result=array('code'=>-5,'msg'=>'The coupon is not intended for you');
                            return $result;
                    }
                }
                    $coubonbyuser=$model->getcubonbyuser($user_id,$order_id);
                    if(empty($coubonbyuser)){
                $coubonbydate=$model->getcubonbydate($code);
                       if(!empty($coubonbydate)){
                 $price=$order_model->getpricebyorder($order_id);
                 $discount=$coubonbydate[0]->discount/100;
                 $lastdiscount=  $price->price*$discount;
                 $price_after_discount=$price->price-$lastdiscount;
          
                 $data=array('user_id'=>$user_id,'order_id'=>$order_id,'code_id'=>$coubonbydate[0]->id,'price'=>$price->price,'discount'=>$lastdiscount,'price_after_discount'=>$price_after_discount);
                 $add=$model->addcoubonuser($data);
                 if($add>0){
                    $result=array('code'=>1,'msg'=>'success');
                    return $result;
                 }
                 else{
                    $result=array('code'=>-1,'msg'=>'please try later');
                    return $result;
                 }
                       }
                       else{
                        $result=array('code'=>-4,'msg'=>'Coubon must be less than start date and greater than end date');
                        return $result;
                       }
                    }
                    else{
                        $result=array('code'=>-4,'msg'=>'coubon was user by this order');
                        return $result;
                    }
               
            }
           
                else{
                    $result=array('code'=>-2,'msg'=>'Coubon not exsit');
                    return $result;
                }
            }
            else{
                $result=array('code'=>$result['code'],'msg'=>$result['messages'],
            );
            return $result;
            }
        }
        else{
            $result=array('code'=>-1,'msg'=>'Method must be POST',
            );
            return $result;
        }
        }
        public function SendNotifications(){
           $data_ar='ffffffsf';
           $token=array('f4tCe1W_Rt6P2oRumL3KbX:APA91bEbloZp4NDmmdUJ-p555uSvsSB1NJWumGUewPst5n8F7KpPDDANnDfQdC7bxsVf3hKSTqJptP_uAhXoi614pyV0eDx1yAIRwfJ0kScoNDbSjb9_T59A3CSDaAbtFw12uxlmm99t');
           $title_ar='تجهيز الطلب';
           $body_ar='طلبك قيد التجهيز';
           $title_en='Order processing';
           $body_en='Your order is being processed';
           $title_de='Auftragsabwicklung';
           $body_de='Ihre Bestellung wird bearbeitet';
           $title_tr='Sipariş düzenleniyor';
           $body_tr='Siparişiniz işleniyor';
            $send=SendNotification::Sendalluser($token,$title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
            if($send){
                $result=array('code'=>1,'msg'=>'success',
            );
            return $this->respond($result,200);
            } 
            else{
                $result=array('code'=>-1,'msg'=>'fail',
            );
            return $this->respond($result,200);
            }
            
        }
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}