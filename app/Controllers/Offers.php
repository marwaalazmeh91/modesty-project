<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\OffersModel;
use App\Models\ProductModel;
use App\Models\CartModel;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class Offers extends BaseController
{ use ResponseTrait;
    public function __construct()
    {
      
    }
public function GetUserOffersProducts(){
        if($this->request->getMethod()=='get'){
  
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
       
    

           
      
            $user_id=$this->request->getVar('user_id');
        if($user_id){
            $check = new Check(); // Create an instance
            $result=$check->check();
            if($result['code']!=1){
                $result=array('code'=>$result['code'],'msg'=>$result['messages'],
            );
            return $this->respond($result,400);
            }
           
        }
        $offer_model=new OffersModel();
        $cart_model = new CartModel();
        $user_model=new UserModel();
        $offers=$offer_model->get_offers_tody($language->getValue());
         $today_offers=array();
        $products=array();
        $products2=array();
        $cart_user=0;
        $favourite_user2=array();
        if(isset($user_id)&&!empty($user_id)){
            $cart_user=$cart_model->get_cart_user($user_id);
         
        }
  
        $cart_user2=array();
        if(!empty($cart_user)){
        foreach($cart_user as $cart){
array_push($cart_user2,$cart->product_id);
        }
    }     
    if(isset($user_id)&&!empty($user_id)){
        $favouirte_user=$user_model->get_favourite_user($user_id);

        foreach($favouirte_user as $f){
array_push($favourite_user2,$f->product_id);
        }
    } 
    $date=date('Y-m-d');
    if(isset($offers) && !empty($offers)){
        $i=0;
        foreach($offers as $proudct){
            if($proudct->id!=null){
               
                if((strtotime($date) == strtotime($proudct->start_date))){

                
            $products2[$i]['id']=$proudct->id;
        $products2[$i]['name']=$proudct->name;
        $products2[$i]['price']=$proudct->price;
        $products2[$i]['discount']=$proudct->discount;
        $products2[$i]['image_url']=$proudct->image_url;
        $products2[$i]['is_new']=$proudct->is_new;
        $products2[$i]['is_offer']=1;
        $products2[$i]['offer_id']=$proudct->offer_id;
        if(!empty($proudct->sum)){
            $rate2=($proudct->sum* 5)/(5*$proudct->count);
            $rate=number_format(floor($rate2*100)/100,1, '.', '');
        }
        
        else{
            $rate="0";
        }
        $products2[$i]['rate']=$rate;
        $discount=$proudct->price*($proudct->discount/100);
        $products2[$i]['price_after_discouunt']=number_format($proudct->price-$discount,2, '.', '');
        if(!empty($cart_user2)){
            if(in_array($proudct->id,$cart_user2)){
                $products2[$i]['is_found']=1;
            }
            else{
                $products2[$i]['is_found']=0;
            }
        }
        else{
            $products2[$i]['is_found']=0;
        }
        if(!empty($favourite_user2)){
            if(in_array($proudct->id,$favourite_user2)){
                $products2[$i]['is_favourite']=1;
            }
            else{
                $products2[$i]['is_favourite']=0;
            }
        }
        else{
            $products2[$i]['is_favourite']=0;
        }
    }
    
}
        $i++;
        }
    }
   
        if(isset($offers) && !empty($offers)){
            $i=0;
            foreach($offers as $proudct){
                if($proudct->id!=null){
                $products[$i]['id']=$proudct->id;
            $products[$i]['name']=$proudct->name;
            $products[$i]['price']=$proudct->price;
            $products[$i]['discount']=$proudct->discount;
            $products[$i]['image_url']=$proudct->image_url;
            $products[$i]['is_new']=$proudct->is_new;
            $products[$i]['is_offer']=1;
            $products[$i]['offer_id']=$proudct->offer_id;
            if(!empty($proudct->sum)){
                $rate2=($proudct->sum* 5)/(5*$proudct->count);
                $rate=number_format(floor($rate2*100)/100,1, '.', '');
            }
            
            else{
                $rate="0";
            }
            $products[$i]['rate']=$rate;
            $discount=$proudct->price*($proudct->discount/100);
            $products[$i]['price_after_discouunt']=number_format($proudct->price-$discount,2, '.', '');
            if(!empty($cart_user2)){
                if(in_array($proudct->id,$cart_user2)){
                    $products[$i]['is_found']=1;
                }
                else{
                    $products[$i]['is_found']=0;
                }
            }
            else{
                $products[$i]['is_found']=0;
            }
               if(!empty($favourite_user2)){
            if(in_array($proudct->id,$favourite_user2)){
                $products[$i]['is_favourite']=1;
            }
            else{
                $products[$i]['is_favourite']=0;
            }
        }
        else{
            $products[$i]['is_favourite']=0;
        }
        }
     
            $i++;
            }
        }
  $data=array('offer_today'=>$products2,'offers'=>$products);
        $result=array('code'=>1,'msg'=>'success','data'=>$data);
        return $this->respond($result,200);

}
else{
    $data=array('code'=>-1,'msg'=>'Method must be GET','data'=>[]);
return	$this->respond($data, 200);
    }
    }
}