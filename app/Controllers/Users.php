<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

use App\Models\MainModel;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;


class Users extends BaseController {
    use ResponseTrait;
    protected $session;


    function __construct()
    {

        $this->session = \Config\Services::session();
        $this->session->start();


    }


    public function delete($id)
	{  
        
        // $this->check_session();
        $user_model = new UserModel();
        $data=array('is_deleted'=>1);
        $delete=$user_model->delete_user($id,$data);
       if($delete==1){
           echo 1;
       }
       else{
           echo 0;
       }
		
    }
    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
     public function update($id)
     {  $this->check_session();
      
         $email = $this->input->post('email');
         $phone =$this->input->post('phone');
 $username=$this->input->post('username');
 $resume=$this->input->post('resume');
 $country=$this->input->post('country');
         if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
             $upload=$this->Users_model->do_upload();
        if($upload['code']==1){
         $image = site_url().'assets/images/'.$upload['data'];
         $data = array(
            'id'=>$id,
            'email' => $email,
            'phone'=> $phone,
            'username'=>$username,
            'country'=>$country,
            'resume'=>$resume,
            'image_url'=>$image
            
         );
        }
        else{
            echo 0;
        }
       
        
       
         }
      
       else{
         $data = array(
            'id'=>$id,
            'email' => $email,
            'phone'=> $phone,
            'username'=>$username,
            'country'=>$country,
            'resume'=>$resume
            
         );
       }
        if($this->Users_model->edit_user($data))
        {
            echo 1;
        }
        else{
            echo 0;
        }
     
    
     }
    public function users()
	{
       
           
            // $this->check_session();
            $user_model = new UserModel();
            $data['users'] =$user_model->get_users();
           
            $data['_view']= 'users/users';
            echo view('home',$data);
            
        }
        public function edit_user($id=null)
        {   
            
         
            
            $data['data']=$this->Users_model->get_user($id);
       
            $data['_view']= 'users/edit_user';
            $this->load->view('home', $data);
            
        }
    public function index()
	{
     $this->load->view('login');
		
	}
    
    public function login()
        {

            $username=$this->request->getVar('username');
            
            $password=$this->request->getVar('password');
            $main_model = new MainModel();
            $result = $main_model->can_login($username, $password);
           
            if(!empty($result)){
                $session_data= array(
                    'username' =>$username,
                    'user_id'=> $result->id
                );
                $this->session->set($session_data); 
                return redirect()->to('users/users');
               
            }else{
              
                $this->index();
            }
            
       

        }
       
    public function enter(){
        if($this->session->userdata('username') != ''){
            redirect('Projects');
            
        }else{
            $this->index();
        }
    }

    public function logout(){
        $this->session->unset_userdata('username');
            redirect('user');
       
    }
  
    public function EditFCMtoken(){
        if($this->request->getMethod()=='post'){
          
                $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
       
        
        
                $language= $this->request->getHeader('lang');
                if(!isset($language)){
                    $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                    return $this->respond($result,400);
                    exit;
                }
                if( strlen( $language->getValue())==0){
                    $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                    return $this->respond($result,400);
                    exit;
                }
      
           
                $token=$this->request->getVar('token');
                $user_id=$this->request->getVar('user_id');
                if(!$user_id){
                    if( $language->getValue()=="de"){
                        $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="tr"){
                        $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="en"){
                        $result=array('code'=>-1,'msg'=>'Please enter the item field');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                        return $this->respond($result,400);
                        exit;
                    }
                }
                if(!$token){
                  
                    if( $language->getValue()=="ar"){
                        $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل التوكن');
                        return $this->respond($result,400);
                        exit;
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'Please enter the token field');
                        return $this->respond($result,400);
                        exit;
                    }
                }
    
              
        $model=new UserModel();
        $data = [
    
           
            'token'=>$token,
     
    
            
            ];
            
        if($model->update_user($user_id,$data)){
    
                $result=array('code'=>1,'msg'=>'success');
                    return $this->respond($result,200);
                    exit;
                }
                else{
                    $result=array('code'=>-1,'msg'=>'fail');
                    return $this->respond($result,200);
                    exit;
                }
            }
            else{
                $result=array('code'=>$result['code'],'msg'=>$result['messages'],
            );
            return $this->respond($result,400);
            }
    }
 
    
    
}