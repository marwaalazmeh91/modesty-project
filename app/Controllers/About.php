<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\AboutModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class About extends BaseController
{ use ResponseTrait;
    
    public function TermsofReturns(){
        
        
            $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
           
        
            $model = new AboutModel();
            $data=$model->get_Terms_of_Returns( $language->getValue());
            if(isset($data) && !empty($data)){
                $result=array('code'=>1,'msg'=>'success','data'=>$data);
                return $this->respond($result,200);
            }
            else{
                $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
                return $this->respond($result,200);
            }
    
    
}
public function AbotApp(){
        
        
    $language= $this->request->getHeader('lang');
    if(!isset($language)){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    if( strlen( $language->getValue())==0){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
   

    $model = new AboutModel();
    $data=$model->get_about_app( $language->getValue());
    if(isset($data) && !empty($data)){
        $result=array('code'=>1,'msg'=>'success','data'=>$data);
        return $this->respond($result,200);
    }
    else{
        $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
        return $this->respond($result,200);
    }


}
public function Termsofservice(){
        
        
    $language= $this->request->getHeader('lang');
    if(!isset($language)){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    if( strlen( $language->getValue())==0){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
   

    $model = new AboutModel();
    $data=$model->get_terms_of_service( $language->getValue());
    if(isset($data) && !empty($data)){
        $result=array('code'=>1,'msg'=>'success','data'=>$data);
        return $this->respond($result,200);
    }
    else{
        $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
        return $this->respond($result,200);
    }


}
public function PrvaicyPolicy(){
        
        
    $language= $this->request->getHeader('lang');
    if(!isset($language)){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    if( strlen( $language->getValue())==0){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
   

    $model = new AboutModel();
    $data=$model->get_privacy_policy( $language->getValue());
    if(isset($data) && !empty($data)){
        $result=array('code'=>1,'msg'=>'success','data'=>$data);
        return $this->respond($result,200);
    }
    else{
        $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
        return $this->respond($result,200);
    }


}
public function GetCityByCode(){
        
        
    $language= $this->request->getHeader('lang');
    if(!isset($language)){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    if( strlen( $language->getValue())==0){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    $code=$this->request->getVar('code');
    if(!$code){
        if( $language->getValue()=="ger"){
            $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="tur"){
            $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="en"){
            $result=array('code'=>-1,'msg'=>'Please enter the email field');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="ar"){
            $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
            return $this->respond($result,400);
            exit;
        }
    }
    $model = new AboutModel();
    $data=$model->get_city_by_code($code, $language->getValue());
    if(isset($data) && !empty($data)){
        $result=array('code'=>1,'msg'=>'success','data'=>$data);
        return $this->respond($result,200);
    }
    else{
        $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
        return $this->respond($result,200);
    }


}
}