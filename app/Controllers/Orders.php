<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\OffersModel;
use App\Models\CartModel;
use App\Models\OrderModel;
use App\Models\ProductModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class Orders extends BaseController
{ use ResponseTrait;
   
       
public function GetMyOrders(){
        
    $user_id=$this->request->getVar('user_id');
    $status=$this->request->getVar('status');
    $check = new Check(); // Create an instance
    $result=$check->check();
    if($result['code']==1){
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $user_id=$this->request->getVar('user_id');
        if(!$user_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the item field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                return $this->respond($result,400);
                exit;
            }
        }
        if(!$status){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the item field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  الحالة');
                return $this->respond($result,400);
                exit;
            }
        }
        $model = new OrderModel();
        $orders=$model->get_my_order($user_id,$status, $language->getValue());
        $data=array();
$price=0;
        if(isset($orders) && !empty($orders)){
            $new_array=array();
            $new_orders=array();
              foreach ($orders as $element) {
             
                $new_array[$element->id][]= $element;
        
              }
         
              $a=0;
              $b=0;
             $price=0;
              foreach($new_array as $key=>$value){
              
                 $price=0;
              $new_orders[$a]['id']=$key;
         
              foreach($value as $c){
             $price=$price+$c->price;
             $date = strtotime($c->create_date);
                  $new_orders[$a]['property'][]=array('name'=>$c->name,'image_url'=>$c->image_url,'create_date'=> date('D F d h:m',$date),'payment_method'=>$c->payment_method);
             
                
               
              }
              $new_orders[$a]['price']=number_format($price,2, '.', '');$price;
              
              $a++;
              }
       
            $result=array('code'=>1,'msg'=>'success','data'=>$new_orders);
            return $this->respond($result,200);
        }
        else{
            $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
            return $this->respond($result,200);
        }
}
else{
$result=array('code'=>$result['code'],'msg'=>$result['messages']);
return $this->respond($result,400);
}
}
public function GetOrderStatus(){
        
    $order_id=$this->request->getVar('order_id');

    $check = new Check(); // Create an instance
    $result=$check->check();
    if($result['code']==1){
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
       
        if(!$order_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Bestellfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen sipariş alanını giriniz');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the order field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  الطلب');
                return $this->respond($result,400);
                exit;
            }
        }

        $model = new OrderModel();
        $orders=$model->get_order_status($order_id);
        if(isset($orders) && !empty($orders)){
            $result=array('code'=>1,'msg'=>'success','data'=>$orders);
            return $this->respond($result,200);
        }
        else{
            $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
            return $this->respond($result,200);
        }
}
else{
$result=array('code'=>$result['code'],'msg'=>$result['messages']);
return $this->respond($result,400);
}
}
public function ReturnOrder(){
    if($this->request->getMethod()=='post'){
      
            $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $check = new Check(); // Create an instance
        $result=$check->check();
    
        if($result['code']==1){

            $order_id=$this->request->getVar('order_id');

 
       
            if(!$order_id){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Bestellfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen sipariş alanını giriniz');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the order field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  الطلب');
                    return $this->respond($result,400);
                    exit;
                }
            }
            
            $model = new OrderModel();
            $cart_model=new CartModel();
            $orders=$model->get_cart_by_order($order_id);
            if(!empty($orders)){
                foreach($orders as $order){
                 
                    $data=array('user_id'=>$order->user_id,'product_id'=>$order->product_id,'size'=>$order->size,'color'=>$order->color,'quantity'=>$order->quantity,'status'=>0,'price'=>$order->price);
                    $insert=$cart_model->add_user_cart($data);
                }
                if($insert>0){
                    $result=array('code'=>1,'msg'=>'success');
                    return $this->respond($result,200);
                    exit;
                }
                else{
                    $result=array('code'=>-1,'msg'=>'fail');
                    return $this->respond($result,200);
                    exit;
                }
            }
            $result=array('code'=>-1,'msg'=>'no data found');
            return $this->respond($result,200);
            exit;

            }

            else{
                $result=array('code'=>$result['code'],'msg'=>$result['messages'],
            );
            return $this->respond($result,400);
            }
    }
    else{
        $result=array('code'=>-1,'msg'=>'Method must be POST',
        );
        return $this->respond($result,400);
    }
}
public function GetOrderDetails(){
    $order_id=$this->request->getVar('order_id');
    
    $language= $this->request->getHeader('lang');
    if(!isset($language)){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    if( strlen( $language->getValue())==0){
        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
        return $this->respond($result,400);
        exit;
    }
    if(!$order_id){
        if( $language->getValue()=="de"){
            $result=array('code'=>-1,'msg'=>'Bitte geben Sie ein Bestellfeld ein');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="tr"){
            $result=array('code'=>-1,'msg'=>'Lütfen bir sipariş alanı girin');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="en"){
            $result=array('code'=>-1,'msg'=>'Please enter a order field');
            return $this->respond($result,400);
            exit;
        }
        if( $language->getValue()=="ar"){
            $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  الطلب');
            return $this->respond($result,400);
            exit;
        }
    }
       $model=new OrderModel();
       $product_model=new ProductModel();
       $sizes=array();
       $products=array();
$i=0;
       $orders=$model->get_order_details($order_id,$language->getValue());
       foreach($orders as $key=>$value){
        $products[$i]['name']=$value->name;
       $image=$product_model->get_product_color_by_size($value->product_id,$value->size);
        $products[$i]['image']=$image[0]->image_url;
        $products[$i]['color']=$value->color;
        $products[$i]['color']=$value->color;
      $i++;
       }
 
        

       

 
    
       $result=array('code'=>1,'msg'=>'success','data'=>$products);
return $this->respond($result,200);
}
}