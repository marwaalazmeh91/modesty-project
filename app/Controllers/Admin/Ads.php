<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\AdsModel;
use CodeIgniter\HTTP\RequestInterface;


class Ads extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
    
    public function index()
	{   
       
        // $this->check_session();
        $ads_model = new AdsModel();
    
        $data['data'] =$ads_model->get_ads();
        
		$data['_view']= 'ads/index';
	    echo view('home',$data); 
		
    }
    public function do_upload($image)
    {
        $validated = $this->validate([
            'image' => [
                'uploaded[image]',
                'mime_in[image,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[image,4096]',
            ],
        ]);
        if ($validated) {
          
            $image->move(ROOTPATH . 'assets/images');
            $image = $image->getClientName();
          
            return array('code'=>1,'data'=>$image);
        }
    
    else{
        return array('code'=>-1,'msg'=>'Please insert valid type for image');
       
    }


    }
    public function add_ad($id=null)
    {   
        
       //  $this->check_session();

       $ads_model = new AdsModel();


        $data['_view']= 'ads/add_ad';
        echo view('home',$data); 
        
    }
    public function save_ad($id=null)
     {   
        $ads_model = new AdsModel();
        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            $image = $this->request->getFile('image');
        $upload=$this->do_upload($image);
   if($upload['code']==1){
    $image =$upload['data'];
    $start_date=$this->request->getVar('start_date');
    $end_date=$this->request->getVar('end_date');



     
    $data=array('image_url'=>$image,'start_date'=>$start_date,'end_date'=>$end_date);
 $add=$ads_model->add($data);
    if($add>0)
   {
    
     echo 1;
   }
   else{
       echo 0;
   }
   }
   else{
       echo 0;
   }
  
   
  
    }
    else{
        echo 0;
    }
         
 
        
         
     }
     
     public function edit_ad($id=null)
     {   
         
        //  $this->check_session();

        $ads_model = new AdsModel();
         $data['data_update']=$ads_model->get_ads_by_id($id);

         $data['_view']= 'ads/edit_ad';
         echo view('home',$data); 
         
     }
     public function update_ad($id=null)
	{   
        // 
        // $this->check_session();
  
        $ads_model = new AdsModel();
        $image_url;
        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            $image = $this->request->getFile('image');
            $upload=$this->do_upload($image);
       if($upload['code']==1){
        $image_url =$upload['data'];
       }
       else{
        echo 0;
    }
    }
        $start_date=$this->request->getVar('start_date');
        $end_date=$this->request->getVar('end_date');
        $data=array('id'=>$id,'start_date'=>$start_date,'end_date'=>$end_date);
        if(!empty($image_url)){
            $data['image_url']=$image_url;
        }
        
        $result=$ads_model->update_ad($id,$data);
        if($result>0)
        {
            echo 1;
        }
        else{
            echo 0;
        }
       
     
		
    }
    
    public function delete($id)
	{ 
        //  $this->check_session();
        $ads_model = new AdsModel();
        $delete=$ads_model->delete_ad($id);
       if($delete==1){
           echo 1;
       }
       else{
           echo 0;
       }
		
    }

 
}