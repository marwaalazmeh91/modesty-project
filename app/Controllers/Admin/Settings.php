<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\SettingsModel;
use CodeIgniter\HTTP\RequestInterface;


class Settings extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
    
    public function index()
	{   
       
        // $this->check_session();
        $settings_model = new SettingsModel();
    
        $data['data'] =$settings_model->get_settings();
 
		$data['_view']= 'settings/settings';
	    echo view('home',$data); 
		
    }
    public function update_settings()
    {   
        
        // $this->check_session();
        $model = new SettingsModel();
        $period_return=$this->request->getVar('period_return');
        $period_new=$this->request->getVar('period_new');
        $language=$this->request->getVar('language');


 
            $data=array('period_return'=>$period_return,'period_new'=>$period_new,'default_language'=>$language);
        
         $result=$model->update_settings($data);
         
        

            if($result>0)
           {
               echo 1;
           }
           else{
               echo 0;
           }
       
      
        
    }
}