<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\CityModel;
use App\Models\SettingsModel;
use App\Models\UserModel;
use App\Models\NotificationModel;
use \Firebase\SendNotification\SendNotification;
require APPPATH.'/Libraries/SendNotification.php';
use CodeIgniter\HTTP\RequestInterface;


class Notifications extends BaseController {


 

     public function send_all_user()
     {   
         
  
    
    $data['_view']= 'notifications/send_all_user';
    echo view('home',$data); 
            // $send=SendNotification::Sendalluser($title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
       
        
    
     }
     public function send_all_user_view()
     {   
         
  
      
        $title_ar=$this->request->getVar('title_ar');
        $title_en=$this->request->getVar('title_en');
        $title_de=$this->request->getVar('title_de');
        $title_tr=$this->request->getVar('title_tr');
        $body_ar=$this->request->getVar('body_ar');
    $body_en=$this->request->getVar('body_en');
    $body_de=$this->request->getVar('body_de');
    $body_tr=$this->request->getVar('body_tr');
    $devices_token=array();
    $user_model = new UserModel();
    $users=$user_model->get_users_token();
    foreach($users as $user){
        array_push($devices_token,$user->token);
    }

$notification_mode=new NotificationModel();
if(!empty($title_ar)){
    $data_translation=array('title'=>$title_ar,'text'=>$body_ar,'language'=>'ar');
       $translation=$notification_mode->add_notification_user($data_translation);
       }
       if(!empty($title_en)){
        $data_translation=array('title'=>$title_en,'text'=>$body_en,'language'=>'en');
           $translation=$notification_mode->add_notification_user($data_translation);
           }
           if(!empty($title_de)){
            $data_translation=array('title'=>$title_de,'text'=>$body_de,'language'=>'de');
               $translation=$notification_mode->add_notification_user($data_translation);
               }
               if(!empty($title_tr)){
 $data_translation=array('title'=>$title_tr,'text'=>$body_tr,'language'=>'tr');
                   $translation=$notification_mode->add_notification_user($data_translation);
                   }
                   if(!empty($devices_token)){
            $send=SendNotification::Sendalluser($devices_token,$title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
       
if($send){
    echo 1;
}
else{
    echo 0;
}
                   }
                   else{
                    echo 0;
                   }
    
     }
     
     public function send_some_user()
     {   
         
  
        $user_model = new UserModel();

      $data['users']=$user_model->get_users_token();
    $data['_view']= 'notifications/send_some_user';
    echo view('home',$data);
            // $send=SendNotification::Sendalluser($title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
       
        
    
     }
     public function send_some_user_view()
     {   
         $devices=array();
        $users=$this->request->getVar('users');
      $some_users=$this->request->getVar('users');
        $title_ar=$this->request->getVar('title_ar');
        $title_en=$this->request->getVar('title_en');
        $title_de=$this->request->getVar('title_de');
        $title_tr=$this->request->getVar('title_tr');
        $body_ar=$this->request->getVar('body_ar');
    $body_en=$this->request->getVar('body_en');
    $body_de=$this->request->getVar('body_de');
    $body_tr=$this->request->getVar('body_tr');
    $users=explode(",",$users);
 
    foreach($users as $user){
    array_push($devices,$user);
    }
    
$notification_mode=new NotificationModel();
if(!empty($title_ar)){
    $data_translation=array('title'=>$title_ar,'text'=>$body_ar,'language'=>'ar','users'=>$some_users);
       $translation=$notification_mode->add_notification_user($data_translation);
       }
       if(!empty($title_en)){
        $data_translation=array('title'=>$title_en,'text'=>$body_en,'language'=>'en','users'=>$some_users);
           $translation=$notification_mode->add_notification_user($data_translation);
           }
           if(!empty($title_de)){
            $data_translation=array('title'=>$title_de,'text'=>$body_de,'language'=>'de','users'=>$some_users);
               $translation=$notification_mode->add_notification_user($data_translation);
               }
               if(!empty($title_tr)){
 $data_translation=array('title'=>$title_tr,'text'=>$body_tr,'language'=>'tr','users'=>$some_users);
                   $translation=$notification_mode->add_notification_user($data_translation);
                   }
                   if(!empty($devices)){
    $send=SendNotification::Sendalluser($devices,$title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
       
    if($send){
        echo 1;
    }
    else{
        echo 0;
    }
    
}
else{
    echo 0;
}
        
    
     }
    }