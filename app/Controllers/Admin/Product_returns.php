<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ProductsReturnModel;
use App\Models\SettingsModel;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;


class Product_returns extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
     public function reasons_return()
     {   
        
         // $this->check_session();
         $model = new ProductsReturnModel();
         $settings_model = new SettingsModel();
         $deafult_language=$settings_model->get_deafult_language();
         $data['data'] =$model->get_reasons_return($deafult_language->default_language);
         
         $data['_view']= 'products_return/index';
         echo view('home',$data); 
         
     }
     public function add_reason($id=null)
     {   
         
        //  $this->check_session();
        
         
         $data['_view']= 'products_return/add_reason';
         echo view('home',$data); 
         
     }
     public function save_reason($id=null)
     {   
         
        //  $this->check_session();
        $model = new ProductsReturnModel();
        $title_ar=$this->request->getVar('title_ar');
        $title_en=$this->request->getVar('title_en');
        $title_de=$this->request->getVar('title_de');
        $title_tr=$this->request->getVar('title_tr');
      
      
     
       
        $add=$model->add_returns_reasons();
           if($add>0)
          {
              if(!empty($title_ar)){
           $data_translation=array('text'=>$title_ar,'language'=>'ar','reasons_id'=>$add);
              $translation=$model->add_returns_reasons_translation($data_translation);
              }
              if(!empty($title_en)){
               $data_translation=array('text'=>$title_en,'language'=>'en','reasons_id'=>$add);
                  $translation=$model->add_returns_reasons_translation($data_translation);
                  }
                  if(!empty($title_de)){
                   $data_translation=array('text'=>$title_de,'language'=>'de','reasons_id'=>$add);
                      $translation=$model->add_returns_reasons_translation($data_translation);
                      }
                      if(!empty($title_tr)){
                       $data_translation=array('text'=>$title_tr,'language'=>'tr','reasons_id'=>$add);
                          $translation=$model->add_returns_reasons_translation($data_translation);
                          }
            echo 1;
          }
          else{
            echo 0;
          }       
      
         
     }
     public function edit_reason($id=null)
     {   
         
        //  $this->check_session();
        $model = new ProductsReturnModel();
        $data['data_update']=$model->get_reason_by_id($id);
       
         $data['_view']= 'products_return/edit_reason';

         echo view('home',$data); 
         
     }
     public function update_reason($id=null)
     {   
         
         // $this->check_session();
         $model = new ProductsReturnModel();
         $title_ar=$this->request->getVar('title_ar');
         $title_en=$this->request->getVar('title_en');
         $title_de=$this->request->getVar('title_de');
         $title_tr=$this->request->getVar('title_tr');
         $title_new_ar=$this->request->getVar('title_new_ar');
         $title_new_en=$this->request->getVar('title_new_en');
         $title_new_de=$this->request->getVar('title_new_de');
         $title_new_tr=$this->request->getVar('title_new_tr');
     
 
  
  
        
         if(!empty($title_ar)){
             $data=array('text'=>$title_ar,'language'=>'ar');
         
          $result=$model->update_reason_trasnlation($id,$data);
          
         }
         if(!empty($title_en)){
             $data=array('text'=>$title_en,'language'=>'en');
         
          $result=$model->update_reason_trasnlation($id,$data);
          
         }
         if(!empty($title_de)){
             $data=array('text'=>$title_de,'language'=>'de');
         
          $result=$model->update_reason_trasnlation($id,$data);
          
         }
         if(!empty($title_tr)){
             $data=array('text'=>$title_tr,'language'=>'tr');
         
          $result=$model->update_reason_trasnlation($id,$data);
          
         }
         if(!empty($title_new_ar)){
             $data_translation=array('text'=>$title_new_ar,'language'=>'ar','reasons_id'=>$id);
                $translation=$model->add_returns_reasons_translation($data_translation);
                }
                if(!empty($title_new_en)){
                 $data_translation=array('text'=>$title_new_en,'language'=>'en','reasons_id'=>$id);
                    $translation=$model->add_returns_reasons_translation($data_translation);
                    }
                    if(!empty($title_new_de)){
                     $data_translation=array('text'=>$title_new_de,'language'=>'de','reasons_id'=>$id);
                        $translation=$model->add_returns_reasons_translation($data_translation);
                        }
                        if(!empty($title_new_tr)){
                         $data_translation=array('text'=>$title_new_tr,'language'=>'tr','reasons_id'=>$id);
                            $translation=$model->add_returns_reasons_translation($data_translation);
                            }
             if($result>0)
            {
                echo 1;
            }
            else{
                echo 0;
            }
        
       
         
     }
     public function delete($id)
     { 
         //  $this->check_session();
         $model = new ProductsReturnModel();
         $delete=$model->delete_reason($id);
        if($delete==1){
            echo 1;
        }
        else{
            echo 0;
        }
    }
        public function products_return()
        {   
           
            // $this->check_session();
            $model = new ProductsReturnModel();
            $settings_model = new SettingsModel();
            $deafult_language=$settings_model->get_deafult_language();
            $data['data'] =$model->get_products_returns($deafult_language->default_language);
            
            $data['_view']= 'products_return/returns';
            echo view('home',$data); 
            
        }
        public function accept($id)
        { 
            //  $this->check_session();
            $model = new ProductsReturnModel();
            $status=array('status'=>1);
            $delete=$model->update_return($id,$status);
           if($delete==1){
               echo 1;
           }
           else{
               echo 0;
           }
       }
       public function reject($id)
        { 
            //  $this->check_session();
            $model = new ProductsReturnModel();
            $status=array('status'=>2);
            $delete=$model->update_return($id,$status);
           if($delete==1){
               echo 1;
           }
           else{
               echo 0;
           }
       }
     }
    