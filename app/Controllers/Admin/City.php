<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\CityModel;
use App\Models\SettingsModel;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;


class City extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
    
    public function index()
	{   
       
        // $this->check_session();
        $city_model = new CityModel();
        $settings_model = new SettingsModel();
        $deafult_language=$settings_model->get_deafult_language();
        $data['data'] =$city_model->get_all_city($deafult_language->default_language);
        
		$data['_view']= 'city/index';
	    echo view('home',$data); 
		
    }
    
   
    public function save_city($id=null)
     {   
        $city_model = new CityModel();
        $settings_model = new SettingsModel();
      
  
    $title_ar=$this->request->getVar('title_ar');
    $title_en=$this->request->getVar('title_en');
    $title_de=$this->request->getVar('title_de');
    $title_tr=$this->request->getVar('title_tr');
    $code=$this->request->getVar('code');
$deafult_language=$settings_model->get_deafult_language();
     
    $data=array('code'=>$code);
 $add=$city_model->add($data);
    if($add>0)
   {
       if(!empty($title_ar)){
    $data_translation=array('name'=>$title_ar,'language'=>'ar','city_id'=>$add);
       $translation=$city_model->add_city_translation($data_translation);
       }
       if(!empty($title_en)){
        $data_translation=array('name'=>$title_en,'language'=>'en','city_id'=>$add);
           $translation=$city_model->add_city_translation($data_translation);
           }
           if(!empty($title_de)){
            $data_translation=array('name'=>$title_de,'language'=>'de','city_id'=>$add);
               $translation=$city_model->add_city_translation($data_translation);
               }
               if(!empty($title_tr)){
                $data_translation=array('name'=>$title_tr,'language'=>'tr','city_id'=>$add);
                   $translation=$city_model->add_city_translation($data_translation);
                   }
     echo 1;
   }
   else{
       echo 0;
   }
  
  
   
  
    
 
        
         
     }
   
     public function add_city($id=null)
     {   
         
        //  $this->check_session();
        
         
         $data['_view']= 'city/add_city';
         echo view('home',$data); 
         
     }
    
     
     public function edit_city($id=null)
     {   
         
        //  $this->check_session();

        $city_model = new CityModel();
         $data['data_update']=$city_model->get_city_by_id($id);

         $data['_view']= 'city/edit_city';
         echo view('home',$data); 
         
     }
     public function update_city($id=null)
	{   
        // 
        // $this->check_session();
        $title_ar=$this->request->getVar('title_ar');
    $title_en=$this->request->getVar('title_en');
    $title_de=$this->request->getVar('title_de');
    $title_tr=$this->request->getVar('title_tr');
    $title_new_ar=$this->request->getVar('title_new_ar');
    $title_new_en=$this->request->getVar('title_new_en');
    $title_new_de=$this->request->getVar('title_new_de');
    $title_new_tr=$this->request->getVar('title_new_tr');
    $code=$this->request->getVar('code');
    $city_model = new CityModel();
  
        $data=array('id'=>$id,'code'=>$code);
        $result=$city_model->update_city($id,$data);

    if(!empty($title_ar)){
        $data=array('name'=>$title_ar,'language'=>'ar');
    
     $result=$city_model->update_trasnlation($id,$data);
     
    }
    if(!empty($title_en)){
        $data=array('name'=>$title_en,'language'=>'en');
    
     $result=$city_model->update_trasnlation($id,$data);
     
    }
    if(!empty($title_de)){
        $data=array('name'=>$title_de,'language'=>'de');
    
     $result=$city_model->update_trasnlation($id,$data);
     
    }
    if(!empty($title_tr)){
        $data=array('name'=>$title_ar,'language'=>'tr');
    
     $result=$city_model->update_trasnlation($id,$data);
     
    }
    if(!empty($title_new_ar)){
        $data_translation=array('name'=>$title_new_ar,'language'=>'ar','city_id'=>$id);
           $translation=$city_model->add_city_translation($data_translation);
           }
           if(!empty($title_new_en)){
            $data_translation=array('name'=>$title_new_en,'language'=>'en','city_id'=>$id);
               $translation=$city_model->add_city_translation($data_translation);
               }
               if(!empty($title_new_de)){
                $data_translation=array('name'=>$title_new_de,'language'=>'de','city_id'=>$id);
                   $translation=$city_model->add_city_translation($data_translation);
                   }
                   if(!empty($title_new_tr)){
                    $data_translation=array('name'=>$title_new_tr,'language'=>'tr','city_id'=>$id);
                       $translation=$city_model->add_city_translation($data_translation);
                       }
        if($result>0)
       {
           echo 1;
       }
       else{
           echo 0;
       }
		
    }
    
    public function delete_city($id)
	{ 
        //  $this->check_session();
        $city_model = new CityModel();
        $delete=$city_model->delete_city($id);
       if($delete==1){
           echo 1;
       }
       else{
           echo 0;
       }
		
    }
 
}