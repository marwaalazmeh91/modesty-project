<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\RequestInterface;
use App\Models\CoubonsModel;
use App\Models\UserModel;
use App\Models\NotificationModel;
use \Firebase\SendNotification\SendNotification;
require APPPATH.'/Libraries/SendNotification.php';
class Coubons extends BaseController {
    public function index()
	{   
       
        // $this->check_session();
        $coubons_model = new CoubonsModel();
    
        $data['data'] =$coubons_model->getcubons();
        
		$data['_view']= 'coubons/index';
	    echo view('home',$data); 
		
    }
    public function save_coubon()
    {   
        
       //  $this->check_session();
       $coubons_model = new CoubonsModel();

       $start_date=$this->request->getVar('start_date');
       $end_date=$this->request->getVar('end_date');
       $discount=$this->request->getVar('discount');
      $start_date= strtotime($start_date);
    $start_date=  date('Y-m-d h:i:s',$start_date);
    $end_date= strtotime($end_date);
    $end_date=  date('Y-m-d h:i:s',$end_date);
  
    $code=$this->generateRandomString();

       $data=array('code'=>$code,'start_date'=>$start_date,'end_date'=>$end_date,'discount'=>$discount,'type'=>1);
 
       $add=$coubons_model->add($data);
          if($add>0)
         {
              $title_ar='كود خصم';
              $body_ar='لقد تم إضافة كود خصم'.$code.' بنسبة '.$discount.' بين'.$start_date.'و'.$end_date;
              $title_en='Discount code';
              $body_en='we add discount code'.$code.'for discount'.$discount.'between'.$start_date.'and.'.$end_date;
              $title_de='Rabattcode';
              $body_de='Wir fügen Rabattcode'.$code.'für Rabatt'.$discount.'zwischen'.$start_date.'and.'.$end_date;
              $title_tr='İndirim kodu';
              $body_tr='indirim kodu ekliyoruz'.$code.'indirim için'.$discount.'arasında'.$start_date.'ve.'.$end_date;
          $devices_token=array();
          $user_model = new UserModel();
          $users=$user_model->get_users_token();
          $notification_mode=new NotificationModel();
if(!empty($title_ar)){
    $data_translation=array('title'=>$title_ar,'text'=>$body_ar,'language'=>'ar');
       $translation=$notification_mode->add_notification_user($data_translation);
       }
       if(!empty($title_en)){
        $data_translation=array('title'=>$title_en,'text'=>$body_en,'language'=>'en');
           $translation=$notification_mode->add_notification_user($data_translation);
           }
           if(!empty($title_de)){
            $data_translation=array('title'=>$title_de,'text'=>$body_de,'language'=>'de');
               $translation=$notification_mode->add_notification_user($data_translation);
               }
               if(!empty($title_tr)){
 $data_translation=array('title'=>$title_tr,'text'=>$body_tr,'language'=>'tr');
                   $translation=$notification_mode->add_notification_user($data_translation);
                   }
          foreach($users as $user){
              array_push($devices_token,$user->token);
          }
      if(!empty($devices_token)){
                  $send=SendNotification::Sendalluser($devices_token,$title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
      
      if($send){
          echo 1;
      }
      else{
          echo 0;
      }
    }
    else{
        echo 1;
    }
         }
         else{
           echo 0;
         }       
     
        
    }
    public function add_coubon($id=null)
    {   
        
       //  $this->check_session();
       
        
        $data['_view']= 'coubons/add_coubon';
        echo view('home',$data); 
        
    }
    public function save_coubon_for_some_user()
    {   
        
       //  $this->check_session();
       $coubons_model = new CoubonsModel();

       $start_date=$this->request->getVar('start_date');
       $end_date=$this->request->getVar('end_date');
       $discount=$this->request->getVar('discount');
       $users=$this->request->getVar('users');
       $some_users=$this->request->getVar('users');
      $start_date= strtotime($start_date);
    $start_date=  date('Y-m-d h:i:s',$start_date);
    $end_date= strtotime($end_date);
    $end_date=  date('Y-m-d h:i:s',$end_date);
    $some_users=explode(",",$some_users);
    $user_model = new UserModel();
    $tokens=$user_model->get_users_token_by_id($some_users);
   
    $devices=array();
    foreach($tokens as $user){
        array_push($devices,$user->token);
    }
    $code=$this->generateRandomString();

       $data=array('code'=>$code,'start_date'=>$start_date,'end_date'=>$end_date,'discount'=>$discount,'type'=>2,'user'=>$users);
 
       $add=$coubons_model->add($data);
          if($add>0)
         {
            $title_ar='كود خصم';
            $body_ar='لقد تم إضافة كود خصم'.$code.' بنسبة '.$discount.' بين'.$start_date.'و'.$end_date;
            $title_en='Discount code';
            $body_en='we add discount code'.$code.'for discount'.$discount.'between'.$start_date.'and.'.$end_date;
            $title_de='Rabattcode';
            $body_de='Wir fügen Rabattcode'.$code.'für Rabatt'.$discount.'zwischen'.$start_date.'and.'.$end_date;
            $title_tr='İndirim kodu';
            $body_tr='indirim kodu ekliyoruz'.$code.'indirim için'.$discount.'arasında'.$start_date.'ve.'.$end_date;
            $notification_mode=new NotificationModel();
            if(!empty($title_ar)){
                $data_translation=array('title'=>$title_ar,'text'=>$body_ar,'language'=>'ar','users'=>$users);
                   $translation=$notification_mode->add_notification_user($data_translation);
                   }
                   if(!empty($title_en)){
                    $data_translation=array('title'=>$title_en,'text'=>$body_en,'language'=>'en','users'=>$users);
                       $translation=$notification_mode->add_notification_user($data_translation);
                       }
                       if(!empty($title_de)){
                        $data_translation=array('title'=>$title_de,'text'=>$body_de,'language'=>'de','users'=>$users);
                           $translation=$notification_mode->add_notification_user($data_translation);
                           }
                           if(!empty($title_tr)){
             $data_translation=array('title'=>$title_tr,'text'=>$body_tr,'language'=>'tr','users'=>$users);
                               $translation=$notification_mode->add_notification_user($data_translation);
                               }
                               if(!empty($devices)){
                               $send=SendNotification::Sendalluser($devices,$title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
       
                               if($send){
                                   echo 1;
                               }
                               else{
                                   echo 0;
                               }
                            }
                            else{
                                echo 1;
                            }
         }
         else{
           echo 0;
         }       
     
        
    }
    public function add_coubon_for_some_user($id=null)
    {   
        
       //  $this->check_session();
       $user_model = new UserModel();

       $data['users']=$user_model->get_users_token();
        $data['_view']= 'coubons/add_coubon_for_some_user';
        echo view('home',$data); 
        
    }
    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrs092u3tuvwxyzaskdhfhf9882323ABCDEFGHIJKLMNksadf9044OPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}