<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\OffersModel;
use App\Models\SettingsModel;
use App\Models\ProductModel;
use CodeIgniter\HTTP\RequestInterface;


class Offers extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
    
    public function index()
	{   
       
        // $this->check_session();
        $model = new OffersModel();
        $settings_model = new SettingsModel();
        $deafult_language=$settings_model->get_deafult_language();
        $data['data'] =$model->get_offers($deafult_language->default_language);
        
      
		$data['_view']= 'offers/index';
	    echo view('home',$data); 
		
    }

    public function save_offer($id=null)
     {   
        $model = new OffersModel();
       

    $start_date=$this->request->getVar('start_date');
    $end_date=$this->request->getVar('end_date');
    $products=$this->request->getVar('products');
    $dicount=$this->request->getVar('dicount');
    $data=array('start_date'=>$start_date,'end_date'=>$end_date,'discount'=>$dicount,'product_id'=>$products);
    $add=$model->add($data);

    //  if(!empty($products)){
    //      $products=explode(",",$products);
    //      foreach($products as $product){
    //         $data=array('product_id'=>$product,'offer_id'=>$add);
    //         $add2=$model->add_offers_products($data);
    //      }
       
    //  }

    if($add>0)
   {
    
     echo 1;
   }
   else{
       echo 0;
   }
  
  
         
     }
     public function add_offer($id=null)
     {   
         
        //  $this->check_session();

        
        $settings_model = new SettingsModel();
        $product_model = new ProductModel();    $product_model = new ProductModel();
        $deafult_language=$settings_model->get_deafult_language();
        $data['products'] =$product_model->get_all_products($deafult_language->default_language);

         $data['_view']= 'offers/add_offer';
         echo view('home',$data); 
         
     }
     public function edit_offer($id=null)
     {   
         
        //  $this->check_session();

        $model = new OffersModel();
         
        $settings_model = new SettingsModel();
        $product_model = new ProductModel();
        $products=array();
        $deafult_language=$settings_model->get_deafult_language();
        $data['products'] =$product_model->get_all_products($deafult_language->default_language);
         $data['data_update']=$model->get_offer_by_id($id);
        // $products_offer=$model->get_products_offer($id);
        // foreach($products_offer as $product){
        //      array_push($products,$product->product_id);
        // }
        //  $data['products_offer']=$products;
         $data['_view']= 'offers/edit_offer';
         echo view('home',$data); 
         
     }
     public function update_offer($id=null)
	{   
        // 
        // $this->check_session();
  
        $model = new OffersModel();
$products=array();
        $start_date=$this->request->getVar('start_date');
        $end_date=$this->request->getVar('end_date');
        $products=$this->request->getVar('products');
        $dicount=$this->request->getVar('dicount');
      
   
        $data=array('id'=>$id,'start_date'=>$start_date,'end_date'=>$end_date,'product_id'=>$products,'discount'=>$dicount);
    //     $products_new=explode(",",$products_new);
    //     $products_offer=$model->get_products_offer($id);
    //     foreach($products_offer as $product){
    //         array_push($products,$product->product_id);
    //    }

        $result=$model->update_offer($id,$data);
        if($result>0)
        {
        //     foreach($products_new as $key=>$value){
        //     if(!in_array($value,$products)){
        //         $data_product=array('product_id'=>$value,'offer_id'=>$id);
        //         $model->add_offers_products($data_product);
        //     }
        // }
        // foreach($products as $key=>$value){
        //     if(!in_array($value,$products_new)){
                
        //         $model->delete_products($value,$id);
        //     }
        // }
           
            echo 1;
        }
        else{
            echo 0;
        }
       
     
		
    }
    
    public function delete($id)
	{ 
        //  $this->check_session();
        $model = new OffersModel();
        $delete=$model->delete_offer($id);
       if($delete==1){
           echo 1;
       }
       else{
           echo 0;
       }
		
    }

 
}