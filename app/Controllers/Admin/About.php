<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;

use App\Models\SettingsModel;
use App\Models\AboutModel;
use CodeIgniter\HTTP\RequestInterface;


class About extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
     public function edit_about($id)
     {   
        
         // $this->check_session();
         $model = new AboutModel();
     
   
      
         $data['data'] =$model->get_all_about($id);
         
         $data['_view']= 'about/edit_about';
         echo view('home',$data); 
         
     }

     public function update_about($id=null)
     {   
         
         // $this->check_session();
         $model = new AboutModel();
         $title_ar=$this->request->getVar('title_ar');
         $title_en=$this->request->getVar('title_en');
         $title_de=$this->request->getVar('title_de');
         $title_tr=$this->request->getVar('title_tr');
  
   
 
 
  
        
         if(!empty($title_ar)){
        
             $data=array('about'=>$title_ar,'language'=>'ar');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_en)){
             $data=array('about'=>$title_en,'language'=>'en');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_de)){
             $data=array('about'=>$title_de,'language'=>'de');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_tr)){
             $data=array('about'=>$title_tr,'language'=>'tr');
         
          $result=$model->update_about($id,$data);
          
         }

             if($result>0)
            {
                echo 1;
            }
            else{
                echo 0;
            }
        
       
         
     }
     public function edit_terms($id)
     {   
        
         // $this->check_session();
         $model = new AboutModel();
     
   
      
         $data['data'] =$model->get_all_about($id);
        
         $data['_view']= 'about/edit_terms';
         echo view('home',$data); 
         
     }

     public function update_terms($id=null)
     {   
         
         // $this->check_session();
         $model = new AboutModel();
         $title_ar=$this->request->getVar('title_ar');
         $title_en=$this->request->getVar('title_en');
         $title_de=$this->request->getVar('title_de');
         $title_tr=$this->request->getVar('title_tr');
  
   
 
 
  
        
         if(!empty($title_ar)){
        
             $data=array('terms_of_service'=>$title_ar,'language'=>'ar');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_en)){
             $data=array('terms_of_service'=>$title_en,'language'=>'en');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_de)){
             $data=array('terms_of_service'=>$title_de,'language'=>'de');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_tr)){
             $data=array('terms_of_service'=>$title_tr,'language'=>'tr');
         
          $result=$model->update_about($id,$data);
          
         }

             if($result>0)
            {
                echo 1;
            }
            else{
                echo 0;
            }
        
       
         
     }
     public function edit_privacy_policy($id)
     {   
        
         // $this->check_session();
         $model = new AboutModel();
     
   
      
         $data['data'] =$model->get_all_about($id);
        
         $data['_view']= 'about/edit_privacy_policy';
         echo view('home',$data); 
         
     }

     public function update_privacy_policy($id=null)
     {   
         
         // $this->check_session();
         $model = new AboutModel();
         $title_ar=$this->request->getVar('title_ar');
         $title_en=$this->request->getVar('title_en');
         $title_de=$this->request->getVar('title_de');
         $title_tr=$this->request->getVar('title_tr');
  
   
 
 
  
        
         if(!empty($title_ar)){
        
             $data=array('privacy_policy'=>$title_ar,'language'=>'ar');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_en)){
             $data=array('privacy_policy'=>$title_en,'language'=>'en');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_de)){
             $data=array('privacy_policy'=>$title_de,'language'=>'de');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_tr)){
             $data=array('privacy_policy'=>$title_tr,'language'=>'tr');
         
          $result=$model->update_about($id,$data);
          
         }

             if($result>0)
            {
                echo 1;
            }
            else{
                echo 0;
            }
        
       
         
     }
     public function edit_terms_return_orders($id)
     {   
        
         // $this->check_session();
         $model = new AboutModel();
     
   
      
         $data['data'] =$model->get_all_about($id);
        
         $data['_view']= 'about/edit_terms_return_orders';
         echo view('home',$data); 
         
     }

     public function update_terms_return_orders($id=null)
     {   
         
         // $this->check_session();
         $model = new AboutModel();
         $title_ar=$this->request->getVar('title_ar');
         $title_en=$this->request->getVar('title_en');
         $title_de=$this->request->getVar('title_de');
         $title_tr=$this->request->getVar('title_tr');
  
   
 
 
  
        
         if(!empty($title_ar)){
        
             $data=array('terms_return_orders'=>$title_ar,'language'=>'ar');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_en)){
             $data=array('terms_return_orders'=>$title_en,'language'=>'en');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_de)){
             $data=array('terms_return_orders'=>$title_de,'language'=>'de');
         
          $result=$model->update_about($id,$data);
          
         }
         if(!empty($title_tr)){
             $data=array('terms_return_orders'=>$title_tr,'language'=>'tr');
         
          $result=$model->update_about($id,$data);
          
         }

             if($result>0)
            {
                echo 1;
            }
            else{
                echo 0;
            }
        
       
         
     }
    }