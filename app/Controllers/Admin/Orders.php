<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\OrderModel;
use App\Models\SettingsModel;
use App\Models\ProductModel;
use App\Models\UserModel;
use App\Models\NotificationModel;
use \Firebase\SendNotification\SendNotification;
require APPPATH.'/Libraries/SendNotification.php';
use CodeIgniter\HTTP\RequestInterface;


class Orders extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
    
    public function new_orders()
	{   
       
        // $this->check_session();
        $model = new OrderModel();
        $settings_model = new SettingsModel();
        $deafult_language=$settings_model->get_deafult_language();
        $data['data'] =$model->get_all_orders(1);
        
		$data['_view']= 'orders/new_orders';
	    echo view('home',$data); 
		
    }
    public function active_orders()
	{   
       
        // $this->check_session();
        $model = new OrderModel();
        $settings_model = new SettingsModel();
        $deafult_language=$settings_model->get_deafult_language();
        $data['data'] =$model->get_all_orders(2);
        
		$data['_view']= 'orders/active_orders';
	    echo view('home',$data); 
		
    }
    public function ended_orders()
	{   
       
        // $this->check_session();
        $model = new OrderModel();
        $settings_model = new SettingsModel();
        $deafult_language=$settings_model->get_deafult_language();
        $data['data'] =$model->get_all_orders(4);
        
		$data['_view']= 'orders/ended_orders';
	    echo view('home',$data); 
		
    }
    public function order_details($id=null)
    {   
        
       //  $this->check_session();

       $model = new OrderModel();
        
       $settings_model = new SettingsModel();
  
       $products=array();
       $deafult_language=$settings_model->get_deafult_language();
       $data['data'] =$model->get_order_details($id,$deafult_language->default_language);
     
 
        $data['_view']= 'orders/order_details';
        echo view('home',$data); 
        
    }
    public function order_details_active($id=null)
    {   
        
       //  $this->check_session();

       $model = new OrderModel();
        
       $settings_model = new SettingsModel();
  
       $products=array();
       $deafult_language=$settings_model->get_deafult_language();
       $data['data'] =$model->get_order_details($id,$deafult_language->default_language);
     
 
        $data['_view']= 'orders/active_orders_details';
        echo view('home',$data); 
        
    }
    public function order_details_ended($id=null)
    {   
        
       //  $this->check_session();

       $model = new OrderModel();
        
       $settings_model = new SettingsModel();
  
       $products=array();
       $deafult_language=$settings_model->get_deafult_language();
       $data['data'] =$model->get_order_details($id,$deafult_language->default_language);
     
 
        $data['_view']= 'orders/order_details_ended';
        echo view('home',$data); 
        
    }
    public function update_order($id)
	{ 
        //  $this->check_session();
        $model = new OrderModel();
        $user_model=new UserModel();
        $notification_mode=new NotificationModel();
        $status=array('order_status'=>2,'status'=>2);
      $user=$model->get_user_by_order_id($id);
      $token=$user_model->get_user_token($user->user_id);
       if($model->update_orders($id,$status)){
        $title_ar='تجهيز الطلب';
        $body_ar='طلبك قيد التجهيز';
        $title_en='Order processing';
        $body_en='Your order is being processed';
        $title_de='Auftragsabwicklung';
        $body_de='Ihre Bestellung wird bearbeitet';
        $title_tr='Sipariş düzenleniyor';
        $body_tr='Siparişiniz işleniyor';
        if(!empty($title_ar)){
            $data_translation=array('title'=>$title_ar,'text'=>$body_ar,'language'=>'ar','users'=>$user->user_id);
               $translation=$notification_mode->add_notification_user($data_translation);
               }
               if(!empty($title_en)){
                $data_translation=array('title'=>$title_en,'text'=>$body_en,'language'=>'en','users'=>$user->user_id);
                   $translation=$notification_mode->add_notification_user($data_translation);
                   }
                   if(!empty($title_de)){
                    $data_translation=array('title'=>$title_de,'text'=>$body_de,'language'=>'de','users'=>$user->user_id);
                       $translation=$notification_mode->add_notification_user($data_translation);
                       }
                       if(!empty($title_tr)){
         $data_translation=array('title'=>$title_tr,'text'=>$body_tr,'language'=>'tr','users'=>$user->user_id);
                           $translation=$notification_mode->add_notification_user($data_translation);
                           }
                         
                           if(!empty($user)){
                            $devices_token=array($token->token);
                           $send=SendNotification::Sendalluser($devices_token,$title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
                           if($send){
                            echo 1;
                        }
                           }
                           else{
                            echo 1;
                           }
        
       }
       else{
           echo 0;
       }
		
    }
    public function ended_order($id)
	{ 
        //  $this->check_session();
        $model = new OrderModel();
        $user_model=new UserModel();
        $notification_mode=new NotificationModel();
        $status=array('order_status'=>4,'status'=>4);
      $user=$model->get_user_by_order_id($id);
      $token=$user_model->get_user_token($user->user_id);
       if($model->update_orders($id,$status)){
        $title_ar='تجهيز الطلب';
        $body_ar='طلبك قيد التجهيز';
        $title_en='Order processing';
        $body_en='Your order is being processed';
        $title_de='Auftragsabwicklung';
        $body_de='Ihre Bestellung wird bearbeitet';
        $title_tr='Sipariş düzenleniyor';
        $body_tr='Siparişiniz işleniyor';
        if(!empty($title_ar)){
            $data_translation=array('title'=>$title_ar,'text'=>$body_ar,'language'=>'ar','users'=>$user->user_id);
               $translation=$notification_mode->add_notification_user($data_translation);
               }
               if(!empty($title_en)){
                $data_translation=array('title'=>$title_en,'text'=>$body_en,'language'=>'en','users'=>$user->user_id);
                   $translation=$notification_mode->add_notification_user($data_translation);
                   }
                   if(!empty($title_de)){
                    $data_translation=array('title'=>$title_de,'text'=>$body_de,'language'=>'de','users'=>$user->user_id);
                       $translation=$notification_mode->add_notification_user($data_translation);
                       }
                       if(!empty($title_tr)){
         $data_translation=array('title'=>$title_tr,'text'=>$body_tr,'language'=>'tr','users'=>$user->user_id);
                           $translation=$notification_mode->add_notification_user($data_translation);
                           }
                         
                           if(!empty($user)){
                            $devices_token=array($token->token);
                           $send=SendNotification::Sendalluser($devices_token,$title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr);
                           if($send){
                            echo 1;
                        }
                           }
                           else{
                            echo 1;
                           }
        
       }
       else{
           echo 0;
       }
		
    }
}