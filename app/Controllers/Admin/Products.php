<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ProductModel;
use App\Models\CategoryModel;
use App\Models\SettingsModel;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;


class Products extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
     public function index()
     {   
        
         // $this->check_session();
         $product_model = new ProductModel();
     
         $data['data'] =$product_model->get_all_products('ar');
         
         $data['_view']= 'products/index';
         echo view('home',$data); 
         
     }
     public function colors_products($product_id)
     {   
        
         // $this->check_session();
         $product_model = new ProductModel();
     
         $data['data'] =$product_model->get_colors_by_product($product_id);
         
         $data['_view']= 'products/colors_products';
         echo view('home',$data); 
         
     }
     public function add_product($id=null)
     {   
         
        //  $this->check_session();
        $settings_model = new SettingsModel();
        $category_model = new CategoryModel();
        $deafult_language=$settings_model->get_deafult_language();
     
        $data['category']=$category_model->get_sub_category($deafult_language->default_language);
         
         $data['_view']= 'products/add_product';
         echo view('home',$data); 
         
     }
     public function uploadImage()
    {
        // image validation
        $validated = $this->validate([
            'image' => [
                'uploaded[image]',
                'mime_in[image,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[image,4096]',
               
            ],
        ]);

        if (!$validated) {
            return view('upload', [
                'validation' => $this->validator
            ]);
        }

        // Grab the file by name given in HTML form
        $files = $this->request->getFileMultiple('image');

        $filePreviewName = [];

        foreach($files as $file) {
            if($file->isValid() && !$file->hasMoved()) {
                $newName = $file->getRandomName();
                $image->move(ROOTPATH . 'assets/images');

                $image = new Image;
                $image->save([
                    'name' => $newName
                ]);

                array_push($filePreviewName, $newName);
            }           
        }

        session()->setFlashdata('success', 'Success! image uploaded.');
      
        return redirect()->to(site_url('/upload'))->withInput()->with('previewImage', $filePreviewName);    
    }

     public function save_product($id=null)
     {   
   
        $category_model = new CategoryModel();
        $product_model = new ProductModel();
        $settings_model = new SettingsModel();

        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            $image = $this->request->getFile('image');
        $upload=$category_model->do_upload($image);
      
   if($upload['code']==1){
    $image =$upload['data'];
    $title_ar=$this->request->getVar('title_ar');
    $title_en=$this->request->getVar('title_en');
    $title_de=$this->request->getVar('title_de');
    $title_tr=$this->request->getVar('title_tr');
    $description_ar=$this->request->getVar('description_ar');
    $description_en=$this->request->getVar('description_en');
    $description_de=$this->request->getVar('description_de');
    $description_tr=$this->request->getVar('description_tr');
       $barcod=$this->request->getVar('bar_code');
    $category=$this->request->getVar('category');
    $price=$this->request->getVar('price');
    $dicount=$this->request->getVar('dicount');
    // $quantity=$this->request->getVar('quantity');
    $sizes=$this->request->getVar('sizes');
//    $color=$this->request->getVar('color');
//    $products_images=  $_FILES['images'];
    $is_arrived=$this->request->getVar('is_arrived');
    $isnew=$this->request->getVar('isnew');
//  $images=$this->request->getFileMultiple('images');
//  if(count($color)!=count($products_images)){
//      echo "0";
//  }
// else{
  
    
$deafult_language=$settings_model->get_deafult_language();
     
    $data=array('price'=>$price,'image_url'=>$image,"discount"=>$dicount,'sizes'=>$sizes,'is_new'=>(int)$isnew,'is_new_arrived'=>(int)$is_arrived,'sub_category_id'=>$category,'barcod'=>$barcod);

 $add=$product_model->add($data);
    if($add>0)
   {
       if(!empty($title_ar)){
    $data_translation=array('name'=>$title_ar,'language'=>'ar','desception'=>$description_ar,'product_id'=>$add);
       $translation=$product_model->add_product_translation($data_translation);
       }
       if(!empty($title_en)){
        $data_translation=array('name'=>$title_en,'language'=>'en','desception'=>$description_en,'product_id'=>$add);
           $translation=$product_model->add_product_translation($data_translation);
           }
           if(!empty($title_de)){
            $data_translation=array('name'=>$title_de,'language'=>'de','desception'=>$description_de,'product_id'=>$add);
               $translation=$product_model->add_product_translation($data_translation);
               }
               if(!empty($title_tr)){
                $data_translation=array('name'=>$title_tr,'language'=>'tr','desception'=>$description_tr,'product_id'=>$add);
                   $translation=$product_model->add_product_translation($data_translation);
                   }
            //        foreach($color as $key=>$val){
       
            //         $upload=$category_model->do_upload2($images[$key]);
            //     // $this->uploadImage();
            //         $data2=array('color'=>$val,'image_url'=>$upload['data'],'product_id'=>$add);
            //         $product_model->add_color_image($data2);
            //     // }
            
            // }
               
     echo 1;
   }
   else{
       echo 0;
   }
   }
   else{
       echo 0;
   }
  
   
  
    }
    else{
        echo 0;
    }
         
 
        
         
     }
     public function edit_product($id=null)
     {   
         
        //  $this->check_session();
        $settings_model = new SettingsModel();
        $category_model = new CategoryModel();
        $product_model = new ProductModel();
        $deafult_language=$settings_model->get_deafult_language();
     
        $data['category']=$category_model->get_sub_category($deafult_language->default_language);
        $data['data_update']=$product_model->get_product_by_id2($id);
    
         $data['_view']= 'products/edit_product';
         echo view('home',$data); 
         
     }
     public function update_product($id=null)
     {   
        $category_model = new CategoryModel();
        $product_model = new ProductModel();
        $settings_model = new SettingsModel();
        $data=array();
        $image_url;
        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            $image = $this->request->getFile('image');
        $upload=$category_model->do_upload($image);
   if($upload['code']==1){
    $image =$upload['data'];
$image_url=$image;
   }
   else{
    echo 0;
}
        }
       
    $title_ar=$this->request->getVar('title_ar');
    $title_en=$this->request->getVar('title_en');
    $title_de=$this->request->getVar('title_de');
    $title_tr=$this->request->getVar('title_tr');
    $title_new_ar=$this->request->getVar('title_new_ar');
    $title_new_en=$this->request->getVar('title_new_en');
    $title_new_de=$this->request->getVar('title_new_de');
    $title_new_tr=$this->request->getVar('title_new_tr');
    $description_ar=$this->request->getVar('description_ar');
    $description_en=$this->request->getVar('description_en');
    $description_de=$this->request->getVar('description_de');
    $description_tr=$this->request->getVar('description_tr');
    $description_new_ar=$this->request->getVar('description_new_ar');
    $description_new_en=$this->request->getVar('description_new_en');
    $description_new_de=$this->request->getVar('description_new_de');
    $description_new_tr=$this->request->getVar('description_new_tr');
    $category=$this->request->getVar('category');
    $price=$this->request->getVar('price');
    $barcod=$this->request->getVar('bar_code');
    $dicount=$this->request->getVar('dicount');
    $quantity=$this->request->getVar('quantity');
    $sizes=$this->request->getVar('sizes');
    $is_arrived=$this->request->getVar('is_arrived');
    $isnew=$this->request->getVar('isnew');
 

$deafult_language=$settings_model->get_deafult_language();
     
    $data=array('price'=>$price,"discount"=>$dicount,'quantity'=>(int)$quantity,'sizes'=>$sizes,'is_new'=>(int)$isnew,'is_new_arrived'=>(int)$is_arrived,'sub_category_id'=>$category,'barcod'=>$barcod);

if(!empty($image_url)){
    $data['image_url']=$image_url;
}

//  $add=$product_model->update_product($id,$data);
    if($product_model->update_product($id,$data))
   {
    
    if(!empty($title_ar)){
        $data_translation=array('name'=>$title_ar,'desception'=>$description_ar,'language'=>'ar');
   
     $result=$product_model->update_product_trasnlation($id,$data_translation);
     
    }
    if(!empty($title_en)){
        $data_translation=array('name'=>$title_en,'desception'=>$description_en,'language'=>'en');
    
     $result=$product_model->update_product_trasnlation($id,$data_translation);
     
    }
    if(!empty($title_de)){
        $data_translation=array('name'=>$title_de,'language'=>'de','desception'=>$description_de);
    
     $result=$product_model->update_product_trasnlation($id,$data_translation);
     
    }
    if(!empty($title_tr)){
        $data_translation=array('name'=>$title_ar,'language'=>'tr','desception'=>$description_tr,);
    
     $result=$product_model->update_product_trasnlation($id,$data_translation);
     
    }
       if(!empty($title_new_ar)){
    $data_translation=array('name'=>$title_new_ar,'language'=>'ar','desception'=>$description_new_ar,'product_id'=>$id);
       $translation=$product_model->add_product_translation($data_translation);
       }
       if(!empty($title_new_en)){
        $data_translation=array('name'=>$title_new_en,'language'=>'en','desception'=>$description_new_en,'product_id'=>$id);
           $translation=$product_model->add_product_translation($data_translation);
           }
           if(!empty($title_new_de)){
            $data_translation=array('name'=>$title_new_de,'language'=>'de','desception'=>$description_new_de,'product_id'=>$id);
               $translation=$product_model->add_product_translation($data_translation);
               }
               if(!empty($title_new_tr)){
                $data_translation=array('name'=>$title_new_tr,'language'=>'tr','desception'=>$description_new_tr,'product_id'=>$id);
                   $translation=$product_model->add_product_translation($data_translation);
                   }
     echo 1;
   }
   else{

       echo 0;
   }

   
  
    }
    
         
    public function delete_product($id)
	{ 
        //  $this->check_session();
        $product_model = new ProductModel();
        $delete=$product_model->delete_product($id);
       if($delete==1){
           echo 1;
       }
       else{
           echo 0;
       }
		
    }
        
    public function edit_color($id=null)
    {   
        
       //  $this->check_session();
      
       $product_model = new ProductModel();

       $data['data_update']=$product_model->get_colors_by_id($id);
  
        $data['_view']= 'products/edit_product_color';
        echo view('home',$data); 
        
    }
    public function update_color_product($id=null)
	{   
        // 
        // $this->check_session();
  
        $product_model = new ProductModel();
        $image_url;
        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            $image = $this->request->getFile('image');
            $upload=$this->do_upload($image);
       if($upload['code']==1){
        $image_url =$upload['data'];
       }
       else{
        echo 0;
    }
    }
        $color=$this->request->getVar('color');
$quantity=$this->request->getVar('quantity');
        $data=array('id'=>$id,'color'=>$color,'quantity'=>$quantity);
        if(!empty($image_url)){
            $data['image_url']=$image_url;
        }
      
        $result=$product_model->update_color($id,$data);
        if($result>0)
        {
            echo 1;
        }
        else{
            echo 0;
        }
       
     
		
    }
    public function delete_color($id)
	{ 
        //  $this->check_session();
        $product_model = new ProductModel();
        $delete=$product_model->delete_color($id);
       if($delete==1){
           echo 1;
       }
       else{
           echo 0;
       }
		
    }
    public function color_product($id=null)
    {   
        

       $product_model = new ProductModel();
  
    
       $data['data']=$product_model->get_product_size($id);

   
        $data['_view']= 'products/products_size';
        echo view('home',$data); 
        
    }
    public function add_product_color($id=null,$size=null)
    {   
        

       $product_model = new ProductModel();
  
    
  $sizes=$product_model->get_product_size($id);
$data['sizes']=$sizes;
   
        $data['_view']= 'products/add_product_color2';
        echo view('home',$data); 
        
    }
    public function save_product_color($id=null,$size=null)
    {   
        

       $product_model = new ProductModel();
       $category_model = new CategoryModel();
    
       $quantity=$this->request->getVar('quantity');
      
      $color=$this->request->getVar('color');
    
    $images=$this->request->getFileMultiple('images');
  
    $save=0;
    foreach($color as $key=>$val){
       $save=0;
        $upload=$category_model->do_upload2($images[$key]);
    // $this->uploadImage();
        $data2=array('color'=>$val,'image_url'=>$upload['data'],'product_id'=>$id,'size'=>$size,'quantity'=>$quantity[$key]);
        $product_model->add_color_image($data2);
    // }
    $save=1;

}
  if($save==1){
      echo 1;
  }
  else{
      echo 0;
  }
        
    }
    }