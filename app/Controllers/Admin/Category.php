<?php namespace App\Controllers\Admin;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\CategoryModel;
use App\Models\SettingsModel;
use App\Models\UserModel;

use CodeIgniter\HTTP\RequestInterface;


class Category extends BaseController {


    public function check_session(){
        $this->config->set_item('sess_expire_on_close', '0');
        if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
             
             redirect('user');
        }
        
     }
    
    public function index()
	{   
       
        // $this->check_session();
        $category_model = new CategoryModel();
    
        $data['data'] =$category_model->get_all_category('ar');
        
		$data['_view']= 'category/index';
	    echo view('home',$data); 
		
    }
    public function sub_category()
	{   
       
        // $this->check_session();
        $category_model = new CategoryModel();
    
        $data['data'] =$category_model->get_sub_category('ar');
      
        
		$data['_view']= 'category/sub_category';
        echo view('home',$data); 
		
    }
    public function do_upload()
    {
        if($image){
            $validated = $this->validate([
                'image' => [
                    'uploaded[image]',
                    'mime_in[image,image/jpg,image/jpeg,image/gif,image/png]',
                    'max_size[image,4096]',
                ],
            ]);
            if ($validated) {
              
                $image->move(ROOTPATH . 'html/uploads');
                $image = $image->getClientName();
              
              
        }
        else{
            $result=array('code'=>-1,'msg'=>'Please insert valid type for image');
            return $this->respond($result,400);
            exit;
        }
    }



    }
    public function save_category($id=null)
     {   
        $category_model = new CategoryModel();
        $settings_model = new SettingsModel();
        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            $image = $this->request->getFile('image');
        $upload=$category_model->do_upload($image);
   if($upload['code']==1){
    $image =$upload['data'];
    $title_ar=$this->request->getVar('title_ar');
    $title_en=$this->request->getVar('title_en');
    $title_de=$this->request->getVar('title_de');
    $title_tr=$this->request->getVar('title_tr');

$deafult_language=$settings_model->get_deafult_language();
     
    $data=array('image_url'=>$image);
 $add=$category_model->add($data);
    if($add>0)
   {
       if(!empty($title_ar)){
    $data_translation=array('name'=>$title_ar,'language'=>'ar','category_id'=>$add);
       $translation=$category_model->add_category_translation($data_translation);
       }
       if(!empty($title_en)){
        $data_translation=array('name'=>$title_en,'language'=>'en','category_id'=>$add);
           $translation=$category_model->add_category_translation($data_translation);
           }
           if(!empty($title_de)){
            $data_translation=array('name'=>$title_de,'language'=>'de','category_id'=>$add);
               $translation=$category_model->add_category_translation($data_translation);
               }
               if(!empty($title_tr)){
                $data_translation=array('name'=>$title_tr,'language'=>'tr','category_id'=>$add);
                   $translation=$category_model->add_category_translation($data_translation);
                   }
     echo 1;
   }
   else{
       echo 0;
   }
   }
   else{
       echo 0;
   }
  
   
  
    }
    else{
        echo 0;
    }
         
 
        
         
     }
     public function save_sub_category($id=null)
     {   
         
        //  $this->check_session();
        $category_model = new CategoryModel();
        $title_ar=$this->request->getVar('title_ar');
        $title_en=$this->request->getVar('title_en');
        $title_de=$this->request->getVar('title_de');
        $title_tr=$this->request->getVar('title_tr');
        $category=$this->request->getVar('category');
      
     
        $data=array('category_id'=>$category);
        $add=$category_model->add_sub_category($data);
           if($add>0)
          {
              if(!empty($title_ar)){
           $data_translation=array('name'=>$title_ar,'language'=>'ar','sub_category_id'=>$add);
              $translation=$category_model->add_sub_category_translation($data_translation);
              }
              if(!empty($title_en)){
               $data_translation=array('name'=>$title_en,'language'=>'en','sub_category_id'=>$add);
                  $translation=$category_model->add_sub_category_translation($data_translation);
                  }
                  if(!empty($title_de)){
                   $data_translation=array('name'=>$title_de,'language'=>'de','sub_category_id'=>$add);
                      $translation=$category_model->add_sub_category_translation($data_translation);
                      }
                      if(!empty($title_tr)){
                       $data_translation=array('name'=>$title_tr,'language'=>'tr','sub_category_id'=>$add);
                          $translation=$category_model->add_sub_category_translation($data_translation);
                          }
            echo 1;
          }
          else{
            echo 0;
          }       
      
         
     }
     public function add_category($id=null)
     {   
         
        //  $this->check_session();
        
         
         $data['_view']= 'category/add_category';
         echo view('home',$data); 
         
     }
     public function add_sub_category($id=null)
     {   
         
        //  $this->check_session();
        $settings_model = new SettingsModel();
        $category_model = new CategoryModel();
        $deafult_language=$settings_model->get_deafult_language();
         $data['category']=$category_model->get_all_category($deafult_language->default_language);
         $data['_view']= 'category/add_sub_catergory';
         echo view('home',$data); 
         
     }
     public function edit_sub_category($id=null)
     {   
         
        //  $this->check_session();
        $category_model = new CategoryModel();
        $settings_model = new SettingsModel();
        $category_model = new CategoryModel();
        $deafult_language=$settings_model->get_deafult_language();
       
        $data['data_update']=$category_model->get_sub_category_by_id($id);
        $data['category']=$category_model->get_all_category($deafult_language->default_language);
         $data['_view']= 'category/edit_sub_category';

         echo view('home',$data); 
         
     }
     public function edit_category($id=null)
     {   
         
        //  $this->check_session();

        $category_model = new CategoryModel();
         $data['data_update']=$category_model->get_category_by_id($id);

         $data['_view']= 'category/edit_category';
         echo view('home',$data); 
         
     }
     public function update_category($id=null)
	{   
        // 
        // $this->check_session();
        $title_ar=$this->request->getVar('title_ar');
    $title_en=$this->request->getVar('title_en');
    $title_de=$this->request->getVar('title_de');
    $title_tr=$this->request->getVar('title_tr');
    $title_new_ar=$this->request->getVar('title_new_ar');
    $title_new_en=$this->request->getVar('title_new_en');
    $title_new_de=$this->request->getVar('title_new_de');
    $title_new_tr=$this->request->getVar('title_new_tr');
        $category_model = new CategoryModel();
        if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
            $image = $this->request->getFile('image');
            $upload=$category_model->do_upload($image);
       if($upload['code']==1){
        $image =$upload['data'];
        $data=array('id'=>$id,'image_url'=>$image);
     
        $result=$category_model->update_category($id,$data);
        if($result>0)
        {
            // echo 1;
        }
        else{
            echo 0;
        }
       }
       else{
           echo 0;
       }
      
       
      
        }
    if(!empty($title_ar)){
        $data_ar=array('name'=>$title_ar,'language'=>'ar');
    
     $result=$category_model->update_trasnlation($id,$data_ar);
     
    }
    if(!empty($title_en)){
        $data_en=array('name'=>$title_en,'language'=>'en');
    
     $result=$category_model->update_trasnlation($id,$data_en);
     
    }
    if(!empty($title_de)){
        $data_de=array('name'=>$title_de,'language'=>'de');
    
     $result=$category_model->update_trasnlation($id,$data_de);
     
    }
    if(!empty($title_tr)){
        $data_tr=array('name'=>$title_ar,'language'=>'tr');
    
     $result=$category_model->update_trasnlation($id,$data_tr);
     
    }
    if(!empty($title_new_ar)){
        $data_translation=array('name'=>$title_new_ar,'language'=>'ar','category_id'=>$id);
           $translation=$category_model->add_category_translation($data_translation);
           }
           if(!empty($title_new_en)){
            $data_translation=array('name'=>$title_new_en,'language'=>'en','category_id'=>$id);
               $translation=$category_model->add_category_translation($data_translation);
               }
               if(!empty($title_new_de)){
                $data_translation=array('name'=>$title_new_de,'language'=>'de','category_id'=>$id);
                   $translation=$category_model->add_category_translation($data_translation);
                   }
                   if(!empty($title_new_tr)){
                    $data_translation=array('name'=>$title_new_tr,'language'=>'tr','category_id'=>$id);
                       $translation=$category_model->add_category_translation($data_translation);
                       }
        if($result>0)
       {
           echo 1;
       }
       else{
           echo 0;
       }
		
    }
    public function update_sub_category($id=null)
	{   
        
        // $this->check_session();
        $category_model = new CategoryModel();
        $title_ar=$this->request->getVar('title_ar');
        $title_en=$this->request->getVar('title_en');
        $title_de=$this->request->getVar('title_de');
        $title_tr=$this->request->getVar('title_tr');
        $title_new_ar=$this->request->getVar('title_new_ar');
        $title_new_en=$this->request->getVar('title_new_en');
        $title_new_de=$this->request->getVar('title_new_de');
        $title_new_tr=$this->request->getVar('title_new_tr');
        $category=$this->request->getVar('category');
        $data=array('category_id'=>$category);

     $result=$category_model->update_sub_category($id,$data);
 
       
        if(!empty($title_ar)){
            $data=array('name'=>$title_ar,'language'=>'ar');
        
         $result=$category_model->update_sub_trasnlation($id,$data);
         
        }
        if(!empty($title_en)){
            $data=array('name'=>$title_en,'language'=>'en');
        
         $result=$category_model->update_sub_trasnlation($id,$data);
         
        }
        if(!empty($title_de)){
            $data=array('name'=>$title_de,'language'=>'de');
        
         $result=$category_model->update_sub_trasnlation($id,$data);
         
        }
        if(!empty($title_tr)){
            $data=array('name'=>$title_tr,'language'=>'tr');
        
         $result=$category_model->update_sub_trasnlation($id,$data);
         
        }
        if(!empty($title_new_ar)){
            $data_translation=array('name'=>$title_new_ar,'language'=>'ar','sub_category_id'=>$id);
               $translation=$category_model->add_sub_category_translation($data_translation);
               }
               if(!empty($title_new_en)){
                $data_translation=array('name'=>$title_new_en,'language'=>'en','sub_category_id'=>$id);
                   $translation=$category_model->add_sub_category_translation($data_translation);
                   }
                   if(!empty($title_new_de)){
                    $data_translation=array('name'=>$title_new_de,'language'=>'de','sub_category_id'=>$id);
                       $translation=$category_model->add_sub_category_translation($data_translation);
                       }
                       if(!empty($title_new_tr)){
                        $data_translation=array('name'=>$title_new_tr,'language'=>'tr','sub_category_id'=>$id);
                           $translation=$category_model->add_sub_category_translation($data_translation);
                           }
            if($result>0)
           {
               echo 1;
           }
           else{
               echo 0;
           }
       
      
		
    }
    public function delete($id)
	{ 
        //  $this->check_session();
        $category_model = new CategoryModel();
        $delete=$category_model->delete_category($id);
       if($delete==1){
           echo 1;
       }
       else{
           echo 0;
       }
		
    }
    public function delete_sub_category($id)
	{ 
        //  $this->check_session();
        $category_model = new CategoryModel();
        $delete=$category_model->delete_category($id);
       if($delete==1){
           echo 1;
       }
       else{
           echo 0;
       }
		
    }
 
}