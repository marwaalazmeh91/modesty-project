<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;
use App\Models\OffersModel;
use App\Models\ProductModel;
use App\Models\UserModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class Profile extends BaseController
{ use ResponseTrait;
   
        public function EditFirstProfile(){
            if($this->request->getMethod()=='post'){
              
                    $language= $this->request->getHeader('lang');
                if(!isset($language)){
                    $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                    return $this->respond($result,400);
                    exit;
                }
                if( strlen( $language->getValue())==0){
                    $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                    return $this->respond($result,400);
                    exit;
                }
                $check = new Check(); // Create an instance
                $result=$check->check();
            
                if($result['code']==1){
                    $language= $this->request->getHeader('lang');
                    if(!isset($language)){
                        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                        return $this->respond($result,400);
                        exit;
                    }
                    if( strlen( $language->getValue())==0){
                        $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                        return $this->respond($result,400);
                        exit;
                    }
          
               
                    $mr=$this->request->getVar('mr');
                    $first_name=$this->request->getVar('first_name');
                    $last_name=$this->request->getVar('last_name');
             
                    $email=$this->request->getVar('email');
                    $password=$this->request->getVar('password');
               
                    $phone=$this->request->getVar('phone');
                    $user_id=$this->request->getVar('user_id');     
                    if(!$user_id){
                        if( $language->getValue()=="de"){
                            $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                            return $this->respond($result,400);
                            exit;
                        }
                        if( $language->getValue()=="tr"){
                            $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                            return $this->respond($result,400);
                            exit;
                        }
                        if( $language->getValue()=="en"){
                            $result=array('code'=>-1,'msg'=>'Please enter the item field');
                            return $this->respond($result,400);
                            exit;
                        }
                        if( $language->getValue()=="ar"){
                            $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                            return $this->respond($result,400);
                            exit;
                        }
                    }      
            if(!$mr){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Feld für den Benutzertyp ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen kullanıcı türü alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter user type field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل نوع المستخدم');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$first_name){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Vornamensfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen ad alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the first name field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الاسم الأول');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$last_name){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Nachnamensfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen soyadı alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the last name field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الاسم الأخير');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$phone){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Handynummernfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen cep telefonu numarası alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the mobile number field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل رقم الموبايل');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$email){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das E-Mail-Feld ein ');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen e-posta alanını girin ');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the email field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الايميل');
                    return $this->respond($result,400);
                    exit;
                }
        
            }
            if(!$phone){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Handynummernfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen cep telefonu numarası alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the mobile number field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل رقم الموبايل');
                    return $this->respond($result,400);
                    exit;
                }
            }
            $model=new UserModel();
            $data = [
 
                'mr'=>$mr,
                
                'first_name'=>$first_name,
                'last_name'=>$last_name,
                'phone'=>$phone,
                'email'=>$email,
         
             
                ];
            if($model->update_user($user_id,$data)){

                    $result=array('code'=>1,'msg'=>'success');
                        return $this->respond($result,200);
                        exit;
                    }
                    else{
                        $result=array('code'=>-1,'msg'=>'fail');
                        return $this->respond($result,200);
                        exit;
                    }
                }
                else{
                    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
                );
                return $this->respond($result,400);
                }
    }
    else{
        $result=array('code'=>-1,'msg'=>'Method must be POST',
        );
        return $this->respond($result,400);
    }
}
public function EditSecondProfile(){
    if($this->request->getMethod()=='post'){
      
            $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $check = new Check(); // Create an instance
        $result=$check->check();
    
        if($result['code']==1){
            $language= $this->request->getHeader('lang');
            if(!isset($language)){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
            if( strlen( $language->getValue())==0){
                $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
                return $this->respond($result,400);
                exit;
            }
  
       
            $country=$this->request->getVar('country');
            $city_code=$this->request->getVar('city_code');
            $city=$this->request->getVar('city');
        
            $street=$this->request->getVar('street');
            $home_number=$this->request->getVar('home_number');
        
            $note=$this->request->getVar('note');
   
            $address_id=$this->request->getVar('address_id');  
            if(!$address_id){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Adressfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen adres alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the address field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل العنوان');
                    return $this->respond($result,400);
                    exit;
                }
            }

            if(!$country){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Länderfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen ülke alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the country field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الدولة');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$city_code){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie ein Feld für den Stadtcode ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen bir şehir kodu alanı girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter a city code field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل رمز المدينة');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$city){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Stadtfeld ein');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen şehir alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter the city field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل المدينة ');
                    return $this->respond($result,400);
                    exit;
                }
            }
            if(!$street){
                if( $language->getValue()=="de"){
                    $result=array('code'=>-1,'msg'=>'Bitte Straßenfeld eingeben');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="tr"){
                    $result=array('code'=>-1,'msg'=>'Lütfen sokak alanını girin');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="en"){
                    $result=array('code'=>-1,'msg'=>'Please enter street field');
                    return $this->respond($result,400);
                    exit;
                }
                if( $language->getValue()=="ar"){
                    $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل الشارع');
                    return $this->respond($result,400);
                    exit;
                }
        
            }
        
    $model=new UserModel();
    $data = [

        'country'=>$country,
        'city_code'=>$city_code,
        'city'=>$city,
        'street'=>$street,
        'home_number'=>$home_number,
        'note'=>$note,
 

        
        ];
        
    if($model->update_address($address_id,$data)){

            $result=array('code'=>1,'msg'=>'success');
                return $this->respond($result,200);
                exit;
            }
            else{
                $result=array('code'=>-1,'msg'=>'fail');
                return $this->respond($result,200);
                exit;
            }
        }
        else{
            $result=array('code'=>$result['code'],'msg'=>$result['messages'],
        );
        return $this->respond($result,400);
        }
}
else{
$result=array('code'=>-1,'msg'=>'Method must be POST',
);
return $this->respond($result,400);
}
}
public function GetProfile(){
        
    $user_id=$this->request->getVar('user_id');
    $check = new Check(); // Create an instance
    $result=$check->check();
    if($result['code']==1){
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $user_id=$this->request->getVar('user_id');
        if(!$user_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the item field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                return $this->respond($result,400);
                exit;
            }
        }
        $user_model = new UserModel();
        $profile=$user_model->get_profile($user_id);
        if(isset($profile) && !empty($profile)){
            $result=array('code'=>1,'msg'=>'success','data'=>$profile);
            return $this->respond($result,200);
        }
        else{
            $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
            return $this->respond($result,200);
        }
}
else{
$result=array('code'=>$result['code'],'msg'=>$result['messages']);
return $this->respond($result,400);
}
}
public function GetDefaultAddress(){
        
    $user_id=$this->request->getVar('user_id');
    $check = new Check(); // Create an instance
    $result=$check->check();
    if($result['code']==1){
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $user_id=$this->request->getVar('user_id');
        if(!$user_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the item field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                return $this->respond($result,400);
                exit;
            }
        }
        $user_model = new UserModel();
        $profile=$user_model->get_address_default($user_id);
        if(isset($profile) && !empty($profile)){
            $result=array('code'=>1,'msg'=>'success','data'=>$profile);
            return $this->respond($result,200);
        }
        else{
            $result=array('code'=>-1,'msg'=>'no data found','data'=>[]);
            return $this->respond($result,200);
        }
}
else{
$result=array('code'=>$result['code'],'msg'=>$result['messages']);
return $this->respond($result,400);
}
}
}