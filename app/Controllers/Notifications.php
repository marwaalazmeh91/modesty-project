<?php namespace App\Controllers;
use CodeIgniter\API\ResponseTrait;

use App\Models\NotificationModel;
use CodeIgniter\HTTP\RequestInterface;
use App\Controllers\Check;
class Notifications extends BaseController
{ use ResponseTrait;

    
    public function GetUserNotifications(){
        if($this->request->getMethod()=='get'){
  
        $language= $this->request->getHeader('lang');
        if(!isset($language)){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        if( strlen( $language->getValue())==0){
            $result=array('code'=>-1,'msg'=>'Please insert pramater lanaguage');
            return $this->respond($result,400);
            exit;
        }
        $check = new Check(); // Create an instance
        $result=$check->check();
    
        if($result['code']==1){
    
      
            $user_id=$this->request->getVar('user_id');
        if(!$user_id){
            if( $language->getValue()=="de"){
                $result=array('code'=>-1,'msg'=>'Bitte geben Sie das Artikelfeld ein');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="tr"){
                $result=array('code'=>-1,'msg'=>'Lütfen öğe alanını girin');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="en"){
                $result=array('code'=>-1,'msg'=>'Please enter the item field');
                return $this->respond($result,400);
                exit;
            }
            if( $language->getValue()=="ar"){
                $result=array('code'=>-1,'msg'=>'الرجاء إدخال حقل  المستحدم');
                return $this->respond($result,400);
                exit;
            }
        }
     
        $model = new NotificationModel();

        $general_notification=$model->get_notifications($language->getValue());
        $user_notification=$model->get_notifications_users($user_id,$language->getValue());
    $all_not= array_merge($general_notification,$user_notification);
     
   
  
  
  
        $result=array('code'=>1,'msg'=>'success','data'=>$all_not);
        return $this->respond($result,200);
    }

else{
    $result=array('code'=>$result['code'],'msg'=>$result['messages'],
);
return $this->respond($result,400);
}
}
else{
    $data=array('code'=>-1,'msg'=>'Method must be GET','data'=>[]);
return	$this->respond($data, 200);
    }
    }
}