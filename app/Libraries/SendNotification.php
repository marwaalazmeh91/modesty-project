<?php namespace Firebase\SendNotification;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;

require __DIR__.'/../../vendor/autoload.php';

class SendNotification 
{
    public static function Send($token, $title, $body,$data=null)
    {
        // var_dump($_SERVER['DOCUMENT_ROOT']);
        // exit;
        $factory = (new Factory)->withServiceAccount(__DIR__.'/../../modesty-a0d17-firebase-adminsdk-wjzgo-42894a42bf.json');
        $messaging = $factory->createMessaging();
        
     
       if($data==null){
$message = CloudMessage::withTarget('token', $token);
// optional
       }
       else{
        $message = CloudMessage::withTarget('token', $token)
        ->withNotification($notification)->withData($data); // opt
       }
try{
// $result=$messaging->send($message);
if ($messaging->send($message)) {
 return true;
}
else{
return false;
}

} catch(InvalidMessage  $e){
   return $e->errors();
}

    }
    public static function Sendalluser($deviceTokens, $title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr)
    {

      
            $firebase=(new Factory)->withServiceAccount(__DIR__.'/../../modesty-a0d17-firebase-adminsdk-wjzgo-42894a42bf.json');

            $messaging = $firebase->createMessaging();
            $data=array('title_en'=>$title_en,'title_ar'=>$title_ar,'title_de'=>$title_de,'title_tr'=>$title_tr,'body_en'=>$body_en,'body_ar'=>$body_ar,'body_de'=>$body_de,'body_tr'=>$body_tr);
        
            $message = CloudMessage::new(); // optional
            $message = $message->withData($data);
            try {
                if($messaging->sendMulticast($message, $deviceTokens)){
                    return true;
                }
                else{
                    return false;
                }
            } catch (Throwable $e) {
               
                $result= $e->getMessage(); 
            }
}
public static function Send_some_user($devices,$title_en,$title_ar,$title_de,$title_tr, $body_en,$body_ar,$body_de,$body_tr)
    {
        // var_dump($_SERVER['DOCUMENT_ROOT']);
        // exit;
        $factory = (new Factory)->withServiceAccount(__DIR__.'/../../shamil-308308-firebase-adminsdk-bf1fd-afce181d53.json');
        $messaging = $factory->createMessaging();
        $title=array('title_en'=>$title_en,'title_ar'=>$title_ar,'title_de'=>$title_de,'title_tr'=>$title_tr);
        $body=array('body_en'=>$body_en,'body_ar'=>$body_ar,'body_de'=>$body_de,'body_tr'=>$body_tr);
        $notification =[
            'title' => $title,
            'body' => $body,
         
        ];
$message = CloudMessage::sendMulticast($devices )
    ->withNotification($notification); // optional
  
try{
$result=$messaging->send($message);
$count=$result->successes()->count();
return $count;
} catch(InvalidMessage  $e){
   return $e->errors();
}

    }
}