<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class CityModel extends Model
{
    public function getcategories(){
        $db = \Config\Database::connect();
        $builder = $db->table('category');
    
        $builder->select('category.id,name,category_translation.id translation_id');
        $builder->join('category_translation','category.id=category_translation.category_id');
     
   
 

                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_all_city($lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('city');
    
        $builder->select('city.id,code,name,city_translation.id translation_id');
        $builder->join('city_translation','city.id=city_translation.city_id');
     
   
    $builder->where('language',$lang);

                   $query   = $builder->get();
       
        return $query->getResult();
    }

    public function get_sub_category_by_category($lang,$category_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('sub_category');
    
        $builder->select('sub_category.id,name');
        $builder->join('sub_category_translation','sub_category.id=sub_category_translation.sub_category_id');
     
   
    $builder->where('language',$lang);
    $builder->where('category_id',$category_id);
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    
    public function do_upload($image)
    {
        $validated = $this->validate([
            'image' => [
                'uploaded[image]',
                'mime_in[image,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[image,4096]',
            ],
        ]);

        if ($validated) {
          
            $image->move(ROOTPATH . 'assets/images');
            $image = $image->getClientName();
          
            return array('code'=>1,'data'=>$image);
        }
    
    else{
        return array('code'=>-1,'msg'=>'Please insert valid type for image');
       
    }


    }
    
    public function do_upload2($image)
    {
        $validated = $this->validate([
            'images' => [
                'uploaded[images]',
                'mime_in[images,image/jpg,image/jpeg,image/gif,image/png]',
                'max_size[images,4096]',
            ],
        ]);
        
        if ($validated) {
          
            $image->move(ROOTPATH . 'assets/images');
            $image = $image->getClientName();
          
            return array('code'=>1,'data'=>$image);
        }
    
    else{
        return array('code'=>-1,'msg'=>'Please insert valid type for image');
       
    }


    }
    public function add($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('city');
         $builder->insert($data);
       return  $db->insertID();
         }
         public function add_city_translation($data){
            $db      = \Config\Database::connect();
            $builder = $db->table('city_translation');
             $builder->insert($data);
           return  $db->insertID();
             }
             public function get_city_by_id($id){
                
        $db = \Config\Database::connect();
        $builder = $db->table('city');
    
        $builder->select('city.id,code,name,city_translation.id translation_id,language');
        $builder->join('city_translation','city.id=city_translation.city_id');
     
        $builder->where('city_translation.city_id',$id);
 

                   $query   = $builder->get();
       
        return $query->getResult();
                }
                public function update_city($id, $update){
         
       
                    $db      = \Config\Database::connect();
                    $builder = $db->table('city');
                    $builder->where('id',$id);
                     $builder->update($update);
                   return  $db->affectedRows();;
               }
               public function update_trasnlation($id, $update){
         
       
                $db      = \Config\Database::connect();
                $builder = $db->table('city_translation');
                $builder->where('city_id',$id);
                $builder->where('language',$update['language']);
                 $builder->update($update);
               return  $db->affectedRows();;
           }
           public function get_sub_category($lang){
      
    
     
            $db = \Config\Database::connect();
            $builder = $db->table('sub_category');
        
            $builder->select('sub_category.id,name');
            $builder->join('sub_category_translation','sub_category.id=sub_category_translation.sub_category_id');
         
       
        $builder->where('language',$lang);
      
                       $query   = $builder->get();
           
            return $query->getResult();
        }
        public function add_sub_category($data){
            $db      = \Config\Database::connect();
            $builder = $db->table('sub_category');
             $builder->insert($data);
           return  $db->insertID();
             }
             public function add_sub_category_translation($data){
                $db      = \Config\Database::connect();
                $builder = $db->table('sub_category_translation');
                 $builder->insert($data);
               return  $db->insertID();
                 }
                 public function get_sub_category_by_id($id){
                
                    $db = \Config\Database::connect();
                    $builder = $db->table('sub_category');
                
                    $builder->select('sub_category.id,name,sub_category_translation.id translation_id,language,sub_category_id');
                    $builder->join('sub_category_translation','sub_category.id=sub_category_translation.sub_category_id');
                 
                    $builder->where('sub_category_translation.sub_category_id',$id);
             
            
                               $query   = $builder->get();
                   
                    return $query->getResult();
                            }
                            public function update_sub_category($id, $update){
         
       
                                $db      = \Config\Database::connect();
                                $builder = $db->table('sub_category');
                                $builder->where('id',$id);
                                 $builder->update($update);
                               return  $db->affectedRows();;
                           }
                           public function update_sub_trasnlation($id, $update){
                     
                   
                            $db      = \Config\Database::connect();
                            $builder = $db->table('sub_category_translation');
                            $builder->where('sub_category_id',$id);
                            $builder->where('language',$update['language']);
                             $builder->update($update);
                           return  $db->affectedRows();;
                       }
                       public function delete_city($id){
                        $db = \Config\Database::connect();
                        $builder = $db->table('city');
                        $builder->where('id',$id);
                         $builder->delete();
                         return $db->affectedRows();
                       
                    }
                    public function delete_sub_category($id){
                        $db = \Config\Database::connect();
                        $builder = $db->table('sub_category');
                        $builder->where('id',$id);
                         $builder->delete();
                         return $db->affectedRows();
                       
                    }
                    
}