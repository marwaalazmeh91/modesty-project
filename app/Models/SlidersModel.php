<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class SlidersModel extends Model
{
 
    public function get_sliders(){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('sliders');
        $builder->select('image_url,start_date,end_date,id');

        $builder->orderBy('sliders.create_date', 'DESC');
      
            $query   = $builder->get();
       
        return $query->getResult();
    }
 

    public function get_slider_by_id($id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('sliders');
        $builder->select('image_url,start_date,end_date');
  
    $builder->where('id',$id);
    
      
                   $query   = $builder->get();
       
        return $query->getRow();
    }


     public function add($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('sliders');
         $builder->insert($data);
       return  $db->insertID();
         }

                    public function update_slider($id, $update){
         
       
                        $db      = \Config\Database::connect();
                        $builder = $db->table('sliders');
                        $builder->where('id',$id);
                       return  $builder->update($update);
                    
                   }
      
               public function delete_slider($id){
                $db = \Config\Database::connect();
                $builder = $db->table('sliders');
                $builder->where('id',$id);
                 $builder->delete();
                 return $db->affectedRows();
               
            }
}