<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class SettingsModel extends Model
{
    public function get_deafult_language(){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('settings');
        $builder->select('default_language');
   
            $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_settings(){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('settings');
        $builder->select('default_language,period_return,period_new');
   
            $query   = $builder->get();
       
        return $query->getRow();
    }
    public function update_settings( $update){
                     
                   
        $db      = \Config\Database::connect();
        $builder = $db->table('settings');

       return  $builder->update($update);
     
   }
}