<?php 
namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class NotificationModel extends Model
{   
public function add_notification_user($data){
    $db      = \Config\Database::connect();
    $builder=$db->table('notifications');
   $builder->insert($data);
  
    return $db->insertID(); ;
}



public function get_notifications($language){
    $db      = \Config\Database::connect();
    $date=date('Y-m-d h:i:s',strtotime("-10 days"));

    $builder=$db->table('notifications');
    $builder->select('title,text,create_date');
    $builder->where('users is NULL', NULL, FALSE); 
    $builder->where('language', $language); 
    $builder->where('create_date>=', $date); 
    $query = $builder->get();
    return $query->getResult() ;
  
}
public function get_notifications_users($user_id,$language){
    $db      = \Config\Database::connect();
    $date=date('Y-m-d h:i:s',strtotime("-10 days"));
    $builder=$db->table('notifications');
    $builder->select('title,text,create_date');
   
    $builder->like('users', $user_id);  
    $builder->where('language', $language); 
    $builder->where('create_date>=', $date); 
    $query = $builder->get();
    return $query->getResult() ;
  
}
}
