<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class ProductsReturnModel extends Model
{
 
    public function get_reasons_return($lang){
      
       
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('returns_reasons');
    
        $builder->select('returns_reasons.id,text');
        $builder->join('returns_reasons_translations','returns_reasons.id=returns_reasons_translations.reasons_id');
     
   
    $builder->where('language',$lang);
  
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function add_returns_reasons(){
        $date=date('Y-m-d h:i');
        $data=array('create_date'=>$date);
        $db      = \Config\Database::connect();
        $builder = $db->table('returns_reasons');
         $builder->insert( $data);
       return  $db->insertID();
         }
         public function add_returns_reasons_translation($data){
            $db      = \Config\Database::connect();
            $builder = $db->table('returns_reasons_translations');
             $builder->insert($data);
           return  $db->insertID();
             }
             public function get_reason_by_id($id){
                
                $db = \Config\Database::connect();
                $builder = $db->table('returns_reasons');
            
                $builder->select('returns_reasons.id,text,returns_reasons_translations.id translation_id,language,reasons_id');
                $builder->join('returns_reasons_translations','returns_reasons.id=returns_reasons_translations.reasons_id');
             
                $builder->where('returns_reasons_translations.reasons_id',$id);
         
        
                           $query   = $builder->get();
               
                return $query->getResult();
                        }
                        public function update_reason_trasnlation($id, $update){
                     
                   
                            $db      = \Config\Database::connect();
                            $builder = $db->table('returns_reasons_translations');
                            $builder->where('reasons_id',$id);
                            $builder->where('language',$update['language']);
                             $builder->update($update);
                           return  $db->affectedRows();;
                       }
                       public function delete_reason($id){
                        $db = \Config\Database::connect();
                        $builder = $db->table('returns_reasons');
                        $builder->where('id',$id);
                         $builder->delete();
                         return $db->affectedRows();
                       
                    }
                    public function get_products_returns($lang){
                
                      $db = \Config\Database::connect();
                      $builder = $db->table('returns');
                  
                      $builder->select('returns.id,returns_reasons_translations.text reason_name,products_translation.name product_name');
                      $builder->join('carts','returns.card_id=carts.id');
                      $builder->join('returns_reasons_translations','returns.reasons_id=returns_reasons_translations.reasons_id');
                      $builder->join('products_translation','carts.product_id=products_translation.product_id');
                      $builder->where('products_translation.language',$lang);
                      $builder->where('returns_reasons_translations.language',$lang);
              
                                 $query   = $builder->get();
                     
                      return $query->getResult();
                              }
                              public function update_return($id, $status){
                     
                   
                                $db      = \Config\Database::connect();
                                $builder = $db->table('returns');
                                $builder->where('id',$id);
                            
                                 $builder->update($status);
                               return  $db->affectedRows();;
                           }
}