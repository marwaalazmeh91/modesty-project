<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class OrderModel extends Model
{
    public function get_my_order($user_id,$status,$lang){
                
        $db = \Config\Database::connect();
        $builder = $db->table('carts');
    
        $builder->select('orders.id,name,image_url,carts.price,orders.create_date,orders.payment_method');
        $builder->join('orders','orders.id=carts.order_id');
        $builder->join('products','carts.product_id=products.id');
        $builder->join('products_translation','products.id=products_translation.product_id');
        $builder->where('orders.status',$status);
        $builder->where('carts.user_id',$user_id);
        $builder->where('products_translation.language',$lang);

                   $query   = $builder->get();
       
        return $query->getResult();
                }
                public function get_order_status($order_id){
                
                    $db = \Config\Database::connect();
                    $builder = $db->table('orders');
                
                    $builder->select('order_status');
                  
              
                    $builder->where('id',$order_id);
                  
                               $query   = $builder->get();
                   
                    return $query->getRow();
                            }
                            public function get_cart_by_order($order_id){
                
                                $db = \Config\Database::connect();
                                $builder = $db->table('carts');
                            
                                $builder->select('*');
                              
                          
                                $builder->where('order_id',$order_id);
                              
                                           $query   = $builder->get();
                               
                                return $query->getResult();
                                        }
                                        public function get_all_orders($status){
                
                                            $db = \Config\Database::connect();
                                            $builder = $db->table('orders');
                                        
                                            $builder->select('orders.id,order_id,payment_method,country,city,street,home_number');
                                            $builder->join('address','orders.address_id=address.id');
                                        
                                            $builder->where('orders.order_status',$status);
                                    
                                        
                                                       $query   = $builder->get();
                                           
                                            return $query->getResult();
                                                    }
                                                    public function get_order_details($order_id,$lang){
                
                                                        $db = \Config\Database::connect();
                                                        $builder = $db->table('carts');
                                                    
                                                        $builder->select('carts.product_id,carts.price,carts.order_id,name,carts.color,carts.size,carts.quantity,payment_method,orders.create_date');
                                                        $builder->join('orders','carts.order_id=orders.id');
                                                        $builder->join('products','carts.product_id=products.id');
                                                        // $builder->join('product_color_image','carts.product_id=product_color_image.product_id');
                                                        $builder->join('products_translation','products.id=products_translation.product_id');
                                                        
                                                        $builder->where('carts.order_id',$order_id);
                                            
                                                        $builder->where('products_translation.language',$lang);
                                                                   $query   = $builder->get();
                                                       
                                                        return $query->getResult();
                                                                }
                                                                public function update_orders($id,$status){
                                                                    $db = \Config\Database::connect();
                                                                
                                                                    $builder = $db->table('orders');
                                                                    $builder->where('id',$id);
                                                     
                                                                    return $builder->update($status);
                                                                    
                                                                   
                                                                }
                                                                public function getpricebyorder($id){
                                                                    $db = \Config\Database::connect();
 
                                                                    $builder = $db->table('carts');
                                                                
                                                                    $builder->select('sum(price)price');
                                                              
                                                              
                                                                    $builder->where('order_id',$id);
                                                                    $builder->groupBy("carts.order_id");
                                                              
                                                              
                                                                               $query   = $builder->get();
                                                                   
                                                                    return $query->getRow();
                                                                }
                                                                public function get_user_by_order_id($id){
                
                                                                    $db = \Config\Database::connect();
                                                                    $builder = $db->table('carts');
                                                                
                                                                    $builder->select('carts.user_id');
                                                                  
                                                                
                                                                    $builder->where('carts.order_id',$id);
                                                            
                                                                
                                                                               $query   = $builder->get();
                                                                   
                                                                    return $query->getRow();
                                                                            }
                                                               
}