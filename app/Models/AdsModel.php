<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class AdsModel extends Model
{
 
    public function get_ads(){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('ads');
        $builder->select('image_url,start_date,end_date,id');

        $builder->orderBy('ads.create_date', 'DESC');
      
            $query   = $builder->get();
       
        return $query->getResult();
    }
 

    public function get_ads_by_id($id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('ads');
        $builder->select('image_url,start_date,end_date');
  
    $builder->where('id',$id);
    
      
                   $query   = $builder->get();
       
        return $query->getRow();
    }


     public function add($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('ads');
         $builder->insert($data);
       return  $db->insertID();
         }

                    public function update_ad($id, $update){
         
       
                        $db      = \Config\Database::connect();
                        $builder = $db->table('ads');
                        $builder->where('id',$id);
                       return  $builder->update($update);
                    
                   }
      
               public function delete_ad($id){
                $db = \Config\Database::connect();
                $builder = $db->table('ads');
                $builder->where('id',$id);
                 $builder->delete();
                 return $db->affectedRows();
               
            }
}