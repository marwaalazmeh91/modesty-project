<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class CoubonsModel extends Model
{
    public function getcubons(){
        $db = \Config\Database::connect();
        $builder = $db->table('coubons');
    
        $builder->select('id,code,start_date,end_date,discount');

     
   
 

                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function add($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('coubons');
         $builder->insert($data);
       return  $db->insertID();
         }
         public function getcubonbycode($code){
          $db = \Config\Database::connect();
          $builder = $db->table('coubons');
      
          $builder->select('id,code,start_date,end_date,discount,type,user');
  
          $builder->where('code',$code);
     
   
  
                     $query   = $builder->get();
         
          return $query->getResult();
      }
      public function getcubonbydate($code){
        $db = \Config\Database::connect();
        date_default_timezone_set('Europe/Berlin'); 
        $date=date('Y-m-d h:i:s');
        $builder = $db->table('coubons');
    
        $builder->select('id,code,start_date,end_date,discount');
        $builder->where('code',$code);
        $builder->where('start_date<=',$date);
        $builder->where('end_date>',$date);
   
 

                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function getcubonbyuser($user_id,$order_id){
      $db = \Config\Database::connect();
 
      $builder = $db->table('coubons_users');
  
      $builder->select('id');

      $builder->where('user_id',$user_id);
      $builder->where('order_id',$order_id);
 


                 $query   = $builder->get();
     
      return $query->getResult();
  }
  public function addcoubonuser($data){
    $db      = \Config\Database::connect();
    $builder = $db->table('coubons_users');
     $builder->insert($data);
   return  $db->insertID();
     }
}