<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class CartModel extends Model
{
    public function get_cart_user($user_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('carts');
        $builder->select('product_id');
  
    $builder->where('user_id',$user_id);
  
    $builder->where('status',0);
      
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_cart_user2($user_id,$product_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('carts');
        $builder->select('product_id');
  
    $builder->where('user_id',$user_id);
     $builder->where('product_id',$product_id);
    $builder->where('status',0);
      
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_carts($lang,$user_id){
      
   
    

        $db = \Config\Database::connect();
        $builder = $db->table('carts');
        $builder->select('carts.id cart_id,products.id,image_url,name,carts.price cart_price,products.price,discount,is_new,carts.quantity,sum(rate) sum,count(rate) count,carts.size,carts.color');
        $builder->join('products','carts.product_id=products.id');
    $builder->join('products_translation','products.id=products_translation.product_id');
    $builder->join('product_reviews','product_reviews.product_id=products.id','left');
  
    $builder->where('language',$lang);
    $builder->where('carts.user_id',$user_id);
    $builder->where('status',0);
    $builder->orderBy('carts.create_date', 'DESC');
   
    $builder->groupBy("products.id");

                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function add_user_cart($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('carts');
         $builder->insert($data);
       return  $db->insertID();
    }
    public function delete_cart($id,$user_id){
        $db = \Config\Database::connect();
        $builder = $db->table('carts');
        $builder->where('product_id',$id);
        $builder->where('user_id',$user_id);
        $builder->where('status',0);
         $builder->delete();
         return $db->affectedRows();
       
    }
    public function complete_payment($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('orders');
         $builder->insert($data);
       return  $db->insertID();
    }
    public function update_cart($user_id,$status){
        $db = \Config\Database::connect();
       $pervstatus=$status['pervious_status'];
       unset($status['pervious_status']);
        $builder = $db->table('carts');
        $builder->where('user_id',$user_id);
        $builder->where('status',$pervstatus);
        return $builder->update($status);
        
       
    }
}