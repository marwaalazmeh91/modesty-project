<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class ProductModel extends Model
{
    public function get_product_details($product_id,$lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('products');
    
        $builder->select('products.id,image_url,name,price,discount,is_new,sizes,quantity,desception');
        $builder->join('products_translation','products.id=products_translation.product_id');
     
    $builder->where('products.id',$product_id);
    $builder->where('language',$lang);
    // $builder->where('quantity!=',0);
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_product_color($product_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('product_color_image');
    
        $builder->select('color,image_url,size,quantity');
       
     
    $builder->where('product_id',$product_id);
 $builder->where('quantity!=',0);

                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_product_reviews($product_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('product_reviews');
        $builder->select('sum(rate) sum,count(rate) count');
   
  
    $builder->where('product_id',$product_id);
 
       
        $builder->groupBy("product_reviews.product_id");
            $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_products_by_sub_category($lang,$sub_category_id){
      
    
        $db = \Config\Database::connect();
        $builder = $db->table('products');
        $builder->select('products.id,image_url,name,price,discount,is_new,sum(rate) sum,count(rate) count');
    $builder->join('products_translation','products.id=products_translation.product_id');
    $builder->join('product_reviews','products.id=product_reviews.product_id','left');
  
    $builder->where('language',$lang);
     
   
   
    $builder->where('sub_category_id',$sub_category_id);
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function add_user_favourite($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('favourite_products');
         $builder->insert($data);
       return  $db->insertID();
    }
    public function get_favourte_user($user_id,$product_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('favourite_products');
        $builder->select('product_id');
  
    $builder->where('user_id',$user_id);
    $builder->where('product_id',$product_id);
      
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_product_by_id($product_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('products');
        $builder->select('price,discount');
  
    $builder->where('id',$product_id);
    
      
                   $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_all_products($lang){
      
    
        $db = \Config\Database::connect();
        $builder = $db->table('products');
        $builder->select('products.id,image_url,name,price,discount');
    $builder->join('products_translation','products.id=products_translation.product_id');

  
    $builder->where('language',$lang);
     
   
   
    
                   $query   = $builder->get();
       
                   return $query->getResult();
}
public function add_product_translation($data){
    $db      = \Config\Database::connect();
    $builder = $db->table('products_translation');
     $builder->insert($data);
   return  $db->insertID();
     }
     public function add($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('products');
         $builder->insert($data);
       return  $db->insertID();
         }
         public function get_product_by_id2($id){
                
            $db = \Config\Database::connect();
            $builder = $db->table('products');
            $builder->select('products.id,image_url,name,desception,price,discount,quantity,sizes,is_new,is_new_arrived,sub_category_id,language,barcod');
        $builder->join('products_translation','products.id=products_translation.product_id');
    
         
            $builder->where('products_translation.product_id',$id);
     
    
                       $query   = $builder->get();
           
            return $query->getResult();
                    }
                    public function update_product($id, $update){
         
       
                        $db      = \Config\Database::connect();
                        $builder = $db->table('products');
                        $builder->where('id',$id);
                       return  $builder->update($update);
                    
                   }
                   public function update_product_trasnlation($id, $update){
             
           
                    $db      = \Config\Database::connect();
                    $builder = $db->table('products_translation');
                    $builder->where('product_id',$id);
                    $builder->where('language',$update['language']);
                     $builder->update($update);
                   return  $db->affectedRows();;
               }
               public function delete_product($id){
                $db = \Config\Database::connect();
                $builder = $db->table('products');
                $builder->where('id',$id);
                 $builder->delete();
                 return $db->affectedRows();
               
            }
            public function search_products($lang,$key){
      
    
                $db = \Config\Database::connect();
                $builder = $db->table('products');
                $builder->select('products.id,image_url,name,price,discount');
            $builder->join('products_translation','products.id=products_translation.product_id');
            $builder->like('name', $key);
          
            $builder->where('language',$lang);
             
           
           
            
                           $query   = $builder->get();
               
                           return $query->getResult();
        }
        public function add_key($data){
            $db      = \Config\Database::connect();
    
    
            $builder = $db->table('results_search');
    
    return  $builder->insert($data);
    
        }
        public function get_search_result($user_id,$lang){
            $db      = \Config\Database::connect();
    
    $builder = $db->table('results_search');
    $builder->select('name,image_url');
    $builder->join('products','results_search.product_id=products.id');
    $builder->join('products_translation','results_search.product_id=products_translation.product_id');
    $builder->where('user_id', $user_id);
  $builder->where('results_search.language',$lang);
    $builder->where('products_translation.language',$lang);
    $query   = $builder->get();
    return $query->getResult() ;
        }
                public function get_user_result($id,$product_id,$lang){
      
    
            $db = \Config\Database::connect();
            $builder = $db->table('results_search');
            $builder->select('id');
        
    
      
        $builder->where('user_id',$id);
         
        $builder->where('user_id',$id);
         $builder->where('product_id',$product_id);
       $builder->where('language',$lang);
        
                       $query   = $builder->get();
           
                       return $query->getRow();
    }
        public function add_color_image($data){
            $db      = \Config\Database::connect();
            $builder = $db->table('product_color_image');
             $builder->insert($data);
           return  $db->insertID();
             }
             public function get_colors_by_product($product_id){
      
    
                $db = \Config\Database::connect();
                $builder = $db->table('product_color_image');
                $builder->select('product_color_image.id,product_color_image.id,image_url,color,size,quantity');
            
        
          
            $builder->where('product_id',$product_id);
             
           
           
            
                           $query   = $builder->get();
               
                           return $query->getResult();
        }
        public function get_colors_by_id($id){
      
    
            $db = \Config\Database::connect();
            $builder = $db->table('product_color_image');
            $builder->select('product_color_image.id,product_color_image.id,image_url,color,quantity');
        
    
      
        $builder->where('id',$id);
         
       
       
        
                       $query   = $builder->get();
           
                       return $query->getRow();
    }
    public function update_color($id, $update){
         
       
        $db      = \Config\Database::connect();
        $builder = $db->table('product_color_image');
        $builder->where('id',$id);
       return  $builder->update($update);
    
   }

public function delete_color($id){
$db = \Config\Database::connect();
$builder = $db->table('product_color_image');
$builder->where('id',$id);
 $builder->delete();
 return $db->affectedRows();

}
public function get_product_size($product_id){
      
    
    $db = \Config\Database::connect();
    $builder = $db->table('products');
    $builder->select('sizes');



$builder->where('id',$product_id);
 



               $query   = $builder->get();
   
               return $query->getResult();
}
public function update_product_quantity($id, $update){
         
       
    $db      = \Config\Database::connect();
    $builder = $db->table('product_color_image');
    $builder->where('product_id',$id);
    $builder->where('quantity!=',0);
    $builder->where('size',$update['size']);
    $builder->where('color',$update['color']);
   return  $builder->update($update);

}
public function get_product_quantity($product_id,$size,$color){
      
    
    $db = \Config\Database::connect();
    $builder = $db->table('product_color_image');
    $builder->select('quantity');

$builder->where('product_id',$product_id);
$builder->where('size',$size);
 
               $query   = $builder->get();
   
               return $query->getResult();
}
public function get_product_color_by_size($product_id,$size){
      
    
     
    $db = \Config\Database::connect();
    $builder = $db->table('product_color_image');

    $builder->select('image_url');
   
 
$builder->where('product_id',$product_id);
$builder->where('quantity!=',0);
$builder->where('size',$size);

               $query   = $builder->get();
   
    return $query->getResult();
}
}