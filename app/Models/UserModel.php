<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class UserModel extends Model
{
    protected $table = 'users';
 
    protected $allowedFields = ['id', 'email','phone'];
    public function get_user_email($email){
        
        $db      = \Config\Database::connect();

$builder = $db->table('users');
$builder->select('email,id');
$builder->where('email', $email);

$query   = $builder->get();
return $query->getRow();
    }
    public function get_user_phone($phone){
        
        $db      = \Config\Database::connect();

$builder = $db->table('users');
$builder->select('phone,id');
$builder->where('phone', $phone);

$query   = $builder->get();
return $query->getRow();
    }
  
    public function add_user($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('users');
         $builder->insert($data);
       return  $db->insertID();
    }
    public function add_address($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('address');
         $builder->insert($data);
       return  $db->insertID();
    }
    public function login($email,$password){
        
        $db      = \Config\Database::connect();

$builder = $db->table('users');
$builder->select('email,id,first_name,last_name,profile_img');
$builder->where('email', $email);
$builder->where('password', md5($password));
$builder->where('active',1);
$query   = $builder->get();
return $query->getRow();
    }
    public function loginwithfacebook($email){
        
        $db      = \Config\Database::connect();

$builder = $db->table('users');
$builder->select('email,id,first_name,last_name');
$builder->where('email', $email);

$builder->where('active',1);
$query   = $builder->get();
return $query->getRow();
    }
    public function resetpassword($email,$password){
        $db      = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->where('email',$email);
         $builder->update($password);
       return  $db->affectedRows();;
    }
    public function get_user_account_status($user_id){
        
        $db      = \Config\Database::connect();

$builder = $db->table('users');
$builder->select('active');
$builder->where('id', $user_id);
$builder->where('active', 1);
$query   = $builder->get();
return $query->getRow();
    }
   
    public function update_user_account($user_id,$status){
        $db      = \Config\Database::connect();
 
        $builder = $db->table('users');
        $builder->where('id',$user_id);
         $builder->update($status);
       return  $db->affectedRows();
    }
    public function update_user_code($email,$status){
        $db      = \Config\Database::connect();
 
        $builder = $db->table('users');
        $builder->where('email',$email);
         $builder->update($status);
       return  $db->affectedRows();
    }
    public function get_user_code($user_id,$code){
        
        $db      = \Config\Database::connect();

$builder = $db->table('users');
$builder->select('email,id');
$builder->where('id', $user_id);
$builder->where('code', $code);
$query   = $builder->get();
return $query->getRow();
    }
    public function get_user_email_code($email,$code){
        
        $db      = \Config\Database::connect();

$builder = $db->table('users');
$builder->select('id');
$builder->where('email', $email);
$builder->where('code', $code);
$query   = $builder->get();
return $query->getRow();
    }
    public function get_favourite_products_by_user($lang,$user_id){
      
    
        $db = \Config\Database::connect();
        $builder = $db->table('products');
        $builder->select('favourite_products.id favourite_id,products.id,image_url,name,price,discount,is_new,sum(rate) sum,count(rate) count');
        $builder->join('favourite_products','products.id=favourite_products.product_id','left');
    $builder->join('products_translation','products.id=products_translation.product_id','left');
    $builder->join('product_reviews','products.id=product_reviews.product_id','left');
  
    $builder->where('language',$lang);
     
   
   
    $builder->where('favourite_products.user_id',$user_id);
    $builder->groupBy('products.id');
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function delete_from_favourite($id,$user_id){
        $db = \Config\Database::connect();
        $builder = $db->table('favourite_products');
        $builder->where('product_id',$id);
        $builder->where('user_id',$user_id);
         $builder->delete();
         return $db->affectedRows();
       
    }
    public function get_users(){
        $db = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->select('first_name,last_name,email,phone,users.id,mr');
        $builder->where('active',1);
        $builder->where('is_deleted',0);
        $query   = $builder->get();
       
        return $query->getResult();
  
        
    
        
    }
    public function get_users_token(){
        $db = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->select('first_name,last_name,token,users.id,mr');
        $builder->where('active',1);
        $builder->where('is_deleted',0);
        $builder->where('token is NOT NULL', NULL, FALSE);
        $query   = $builder->get();
       
        return $query->getResult();
  
        
    
        
    }
    public function get_users_token_by_id($users){
        $db = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->select('first_name,last_name,token,users.id,mr');
        $builder->where('active',1);
        $builder->where('is_deleted',0);
        $builder->where('token is NOT NULL', NULL, FALSE);
        $builder->wherein('id', $users, );
        $query   = $builder->get();
       
        return $query->getResult();
  
    }
    public function delete_user($id,$data){
        $db      = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->where('id',$id);
         $builder->update($data);
         
         return  $db->affectedRows();
         
     } 
     public function get_address($user_id,$lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('address');
        $builder->select('full_name,address.id,phone,country,city_code,city,street,home_number,note,is_deafult,address_name');
        
    
    $builder->where('user_id',$user_id);
    // $builder->where('language',$lang);
      
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_address_name($id,$lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('address_name_translation');
        $builder->select('name');
    
  
    $builder->where('language',$lang);
    $builder->where('address_id',$id);
                   $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_address_names($lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('address_name_translation');
        $builder->select('address_id,name');
    
  
    $builder->where('language',$lang);
    
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function update_user($user_id,$data){
        $db = \Config\Database::connect();
       
        $builder = $db->table('users');
        $builder->where('id',$user_id);
        
        return $builder->update($data);
        
       
    }
    public function update_address($address_id,$data){
        $db = \Config\Database::connect();
       
        $builder = $db->table('address');
        $builder->where('id',$address_id);
        
        return $builder->update($data);
        
       
    }
    public function get_profile($user_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->select('first_name,last_name,email,phone,mr,profile_img');
  
    $builder->where('id',$user_id);
  
      
                   $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_address_default($user_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('address');
        $builder->select('id,country,city_code,city,street,home_number,note,state,address_name');
  
    $builder->where('user_id',$user_id);
    $builder->where('is_deafult',1);
      
                   $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_favourite_user($user_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('favourite_products');
        $builder->select('product_id');
  
    $builder->where('user_id',$user_id);
    
      
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function delete_address($id){
        $db = \Config\Database::connect();
        $builder = $db->table('address');
        $builder->where('id',$id);
         $builder->delete();
         return $db->affectedRows();
       
    }
    public function get_user_token($id){
        $db = \Config\Database::connect();
        $builder = $db->table('users');
        $builder->select('first_name,last_name,token,users.id,mr');
        $builder->where('active',1);
        $builder->where('is_deleted',0);
        $builder->where('token is NOT NULL', NULL, FALSE);
        $builder->where('id', $id );
        $query   = $builder->get();
       
        return $query->getRow();
  
    }
}