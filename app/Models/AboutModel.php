<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class AboutModel extends Model
{
 
    public function get_Terms_of_Returns($lang){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('abouts_translation');
        $builder->select('terms_return_orders');

        $builder->where('language',$lang);
      
            $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_about_app($lang){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('abouts_translation');
        $builder->select('about,vission,mission');

        $builder->where('language',$lang);
      
            $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_terms_of_service($lang){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('abouts_translation');
        $builder->select('terms_of_service');

        $builder->where('language',$lang);
      
            $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_privacy_policy($lang){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('abouts_translation');
        $builder->select('privacy_policy');

        $builder->where('language',$lang);
      
            $query   = $builder->get();
       
        return $query->getRow();
    }
    public function get_all_about($id){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('abouts_translation');
        $builder->select('terms_return_orders,privacy_policy,about,terms_return_orders,terms_of_service,language');

        $builder->where('aabout_id',$id);
      
            $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_city_by_code($code,$lang){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('city');
        $builder->select('name');
        $builder->join('city_translation','city.id=city_translation.city_id');
        $builder->like('code', $code);
        $builder->where('language', $lang);
            $query   = $builder->get();
       
        return $query->getResult();
    }
    public function update_about($id, $update){
                     
                   
        $db      = \Config\Database::connect();
        $builder = $db->table('abouts_translation');
        $builder->where('aabout_id',$id);
        $builder->where('language',$update['language']);
       return  $builder->update($update);
     
   }

}