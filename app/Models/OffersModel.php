<?php
namespace App\Models;
use CodeIgniter\Database\ConnectionInterface;
use CodeIgniter\Model;
class OffersModel extends Model
{

    public function get_offers_tody($lang){
      
   
        date_default_timezone_set('Asia/Damascus'); 
        $date=date('Y-m-d h:i:s');

        $db = \Config\Database::connect();
        $builder = $db->table('offers');
        $builder->select('products.id,image_url,name,price,offers.discount,is_new,sum(rate) sum,count(rate) count,start_date,end_date,offers.id offer_id');
        // $builder->join('offers_products','offers.id=offers_products.offer_id');
        $builder->join('products','offers.product_id=products.id');
      
    $builder->join('products_translation','products.id=products_translation.product_id');
    $builder->join('product_reviews','products.id=product_reviews.product_id','left');
  
    $builder->where('language',$lang);
    $builder->where('start_date<=',$date);
    $builder->where('end_date>',$date);
    $builder->orderBy('offers.create_date', 'DESC');
   
   

                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_offers($lang){
      
       
     
        $db = \Config\Database::connect();
        $builder = $db->table('offers');
        $builder->select('name,start_date,end_date,offers.id');
        // $builder->join('offers_products','offers.id=offers_products.offer_id');
        $builder->join('products','offers.product_id=products.id');
        $builder->join('products_translation','products.id=products_translation.product_id');
        $builder->where('language',$lang);
        $builder->orderBy('offers.create_date', 'DESC');
      
            $query   = $builder->get();
       
        return $query->getResult();
    }
    public function add($data){
        $db      = \Config\Database::connect();
        $builder = $db->table('offers');
         $builder->insert($data);
       return  $db->insertID();
         }
         public function add_offers_products($data){
            $db      = \Config\Database::connect();
            $builder = $db->table('offers_products');
             $builder->insert($data);
           return  $db->insertID();
             }
             public function get_offer_by_id($id){
      
    
     
                $db = \Config\Database::connect();
                $builder = $db->table('offers');
                $builder->select('start_date,end_date,product_id,discount');
          
            $builder->where('id',$id);
            
              
                           $query   = $builder->get();
               
                return $query->getRow();
            }
            public function get_products_offer($id){
      
    
     
                $db = \Config\Database::connect();
                $builder = $db->table('offers_products');
                $builder->select('product_id');
          
            $builder->where('offer_id',$id);
            
              
                           $query   = $builder->get();
               
                return $query->getResult();
            }
            
            public function update_offer($id, $update){
         
       
                $db      = \Config\Database::connect();
                $builder = $db->table('offers');
                $builder->where('id',$id);
               return  $builder->update($update);
            
           }

       public function delete_offer($id){
        $db = \Config\Database::connect();
        $builder = $db->table('offers');
        $builder->where('id',$id);
         $builder->delete();
         return $db->affectedRows();
       
    }
    public function delete_products($product_id,$offer_id){
        $db = \Config\Database::connect();
        $builder = $db->table('offers_products');
        $builder->where('product_id',$product_id);
        $builder->where('offer_id',$offer_id);
         $builder->delete();
         return $db->affectedRows();
       
    }
    public function get_offer_discount_by_id($offer_id){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('offers');
        $builder->select('discount');
  
    $builder->where('id',$offer_id);
    
      
                   $query   = $builder->get();
       
        return $query->getRow();
    }
}