<?php namespace App\Models;

use CodeIgniter\Model;

class HomeModel extends Model
{
  
    public function get_slider(){
      
        $date=date('Y-m-d h:i');
     
        $db = \Config\Database::connect();
        $builder = $db->table('sliders');
        $builder->select('image_url');

        $builder->where('start_date<',$date);
        $builder->where('end_date>',$date);
        $builder->orderBy('sliders.create_date', 'DESC');
      
            $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_ads(){
      
        $date=date('Y-m-d');
     
        $db = \Config\Database::connect();
        $builder = $db->table('ads');
        $builder->select('image_url');

        $builder->where('start_date<=',$date);
        $builder->where('end_date>=',$date);
        $builder->orderBy('ads.create_date', 'DESC');
      
            $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_category($lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('category');
        $builder->select('category.id,image_url,name');
    $builder->join('category_translation','category.id=category_translation.category_id');
    $builder->where('language',$lang);
        $builder->orderBy('category.create_date', 'DESC');
      
                   $query   = $builder->get();
       
        return $query->getResult();
    }
    public function get_product_is_new_arrived($lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('products');
        $builder->select('products.id,image_url,name,price,discount,is_new,sum(rate) sum,count(rate) count');
    $builder->join('products_translation','products.id=products_translation.product_id');
    $builder->join('product_reviews','products.id=product_reviews.product_id','left');
  
    $builder->where('language',$lang);
    $builder->where('is_new_arrived',1);
        $builder->orderBy('products.create_date', 'DESC');
        $builder->groupBy("products.id");
            $query   = $builder->get(20);
       
        return $query->getResult();
    }
    public function get_product_is_choose($lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('products');
        $builder->select('products.id,image_url,name,price,discount,is_new,sum(rate) sum,count(rate) count');
    $builder->join('products_translation','products.id=products_translation.product_id');
    $builder->join('product_reviews','products.id=product_reviews.product_id','left');
    $builder->where('language',$lang);
    $builder->where('is_choose',1);
        $builder->orderBy('products.create_date', 'DESC');
        $builder->groupBy("products.id");
            $query   = $builder->get(20);
       
        return $query->getResult();
    }
    public function get_product_is_most_view($lang){
      
    
     
        $db = \Config\Database::connect();
        $builder = $db->table('products');
        $builder->select('products.id,image_url,name,price,discount,is_new,sum(rate) sum,count(rate) count');
    $builder->join('products_translation','products.id=products_translation.product_id');
    $builder->join('product_reviews','products.id=product_reviews.product_id','left');
    $builder->where('language',$lang);
    // $builder->where('is_choose',1);
        $builder->orderBy('products.views', 'DESC');
        $builder->groupBy("products.id");
            $query   = $builder->get(20);
       
        return $query->getResult();
    }
}